## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#

ad_library {
    
    Procedures for the messaging system
    
    @author <yourname> (<your email>)
    @creation-date 2012-01-06
    @cvs-id $Id$
}

ad_proc -public im_freelance_trans_send_request {
    -assignment_id
} {

    db_1row assignment_info "select assignment_name, assignee_id, project_id, fa.rate, fa.uom_id, fa.end_date as deadline, assignment_units, fa.freelance_package_id, assignment_type_id, assignment_status_id
        from im_freelance_assignments fa, im_freelance_packages fp
        where fa.freelance_package_id = fp.freelance_package_id
        and fa.assignment_id = :assignment_id"

    set salutation_pretty [im_invoice_salutation -person_id $assignee_id]
    if {$assignment_status_id eq 4220 || $assignment_status_id eq 4221} {
        db_1row project_info "
            select im_name_from_id(project_lead_id) as project_manager,
            project_nr, project_type_id, project_lead_id, subject_area_id, im_name_from_id(source_language_id) as source_language
            from im_projects
            where project_id=:project_id"

        set sql "
            select email
            from cc_users
            where user_id=:project_lead_id"
        set project_manager_email [db_string project_manager_email $sql]
        set company_name [im_name_from_id [im_company_freelance]]

        set location [util_current_location]
        set project_dir [im_filestorage_project_path $project_id]

        db_1row user_info "
        select first_names,last_name, email, company_id as provider_id, salutation_id
        from im_companies c, acs_rels r, persons pe, parties p
        where pe.person_id = p.party_id and pe.person_id = :assignee_id and r.object_id_one = c.company_id and r.object_id_two = pe.person_id limit 1"

        set salutation [im_invoice_salutation -person_id $assignee_id]
        set locale [lang::user::locale -user_id $assignee_id]

        # Get Briefing from NOTES / FORUM
        set briefing ""

        set deadline [lc_time_fmt $deadline "%q %X"]
        set task_ids [db_list tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]

        # ---------------------------------------------------------------
        # Prepare links to download
        # ---------------------------------------------------------------

        foreach task_id $task_ids {
            set pdf_filename [im_trans_task_source_file_pdf -task_id $task_id]
            if {$pdf_filename ne ""} {
                # Don't need the path
                set pdf_filename [file tail $pdf_filename]
                set ue_filename [ns_urlencode $pdf_filename]
                append links_to_file_downloads "<li><a href=\"${location}/sencha-freelance-translation/download/${task_id}/${ue_filename}\">${pdf_filename}</a></li>"

            } else {
                set filename [db_string filename "select coalesce(task_filename,task_name) from im_trans_tasks where task_id = :task_id" -default ""]
                if {$filename ne ""} {
                    append links_to_file_downloads "<li>$filename</li>"
                }
            }
        }

        # ---------------------------------------------------------------
        #  Prepare links to accept/decline
        # ---------------------------------------------------------------
        set total_number_of_task_units [lc_numeric $assignment_units "%.0f" $locale]
	set unit_of_measure [im_category_from_id -locale $locale $uom_id]
        if {$uom_id eq [im_uom_unit]} {
            set best_price_match "[lc_numeric $rate "%.2f" $locale] EUR (fixed minimum)"
            set total_amount "[lc_numeric $rate "%.2f" $locale] EUR"
        } else {
            set best_price_match "[lc_numeric $rate "%.2f" $locale] EUR"
            set total_amount "[lc_numeric [expr $assignment_units * $rate] "%.2s" $locale] EUR"
        }

        set token [ns_sha1 someSeCrEt-${assignee_id}-${project_id}]
        set assignments_link [export_vars -base "${location}/sencha-freelance-translation/project-assignments" -url {token project_id assignee_id}]
        set acceptance_link $assignments_link
        set deny_link $assignments_link
        
        # Get the messages by language of the user
        db_1row languages "select source_language_id, target_language_id from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $task_ids]) limit 1 "
        set source_language [im_category_from_id -locale $locale $source_language_id]
        set target_language [im_category_from_id -locale $locale $target_language_id]
	set language_combination "[string range $source_language 0 1]_[string range $target_language 0 1]"

        set project_type [im_category_from_id -locale $locale $project_type_id]
        set task_type_pretty [im_category_from_id -locale $locale $assignment_type_id]

	set subject "[lang::message::lookup $locale sencha-freelance-translation.lt_enquiry_message_subject]"
	set body "[lang::message::lookup $locale sencha-freelance-translation.lt_enquiry_message_body]"

        set from_addr $project_manager_email
        set signature [db_string signature "select signature from parties where email = :from_addr" -default ""]
        if {$signature ne ""} {
            append body [template::util::richtext::get_property html_value $signature]
        }
        intranet_chilkat::send_mail \
            -to_addr $email \
            -from_addr $from_addr \
    	    -cc_addr $from_addr \
            -subject $subject \
            -body $body \
            -object_id $project_id \
            -use_sender

        # Update status
        db_dml update "update im_freelance_assignments set assignment_status_id = 4221 where assignment_id = :assignment_id"
    }
}


ad_proc -public im_freelance_trans_send_assignment {
    -assignment_id
} {

    db_1row assignment_info "select assignment_name, assignee_id, project_id, fa.rate, fa.uom_id, fa.end_date as deadline, assignment_units, fa.freelance_package_id, assignment_type_id, assignment_status_id
        from im_freelance_assignments fa, im_freelance_packages fp
        where fa.freelance_package_id = fp.freelance_package_id
        and fa.assignment_id = :assignment_id"

    set salutation_pretty [im_invoice_salutation -person_id $assignee_id]

    db_1row project_info "
            select im_name_from_id(project_lead_id) as project_manager,
            project_nr, project_type_id, project_lead_id, subject_area_id, im_name_from_id(source_language_id) as source_language
            from im_projects
            where project_id=:project_id"

    set sql "
        select email
        from cc_users
        where user_id=:project_lead_id"
    set project_manager_email [db_string project_manager_email $sql]
    set company_name [im_name_from_id [im_company_freelance]]

    set location [util_current_location]
    set project_dir [im_filestorage_project_path $project_id]

    db_1row user_info "
        select first_names,last_name, email, company_id as provider_id, salutation_id
        from im_companies c, acs_rels r, persons pe, parties p
        where pe.person_id = p.party_id and pe.person_id = :assignee_id and r.object_id_one = c.company_id and r.object_id_two = pe.person_id limit 1"

    set salutation [im_invoice_salutation -person_id $assignee_id]
    set locale [lang::user::locale -user_id $assignee_id]

    
    set token [ns_sha1 someSeCrEt-${assignee_id}-${project_id}]
    set assignments_link [export_vars -base "${location}/sencha-freelance-translation/project-assignments" -url {token project_id assignee_id}]
    
    set base_url [parameter::get_from_package_key -package_key "sencha-portal" -parameter "FreelancersPortalUrl" -default ""]
    if {$base_url ne ""} {
         set assignments_link "$base_url/?project_id=$project_id#freelancerassignments"
    }


    # Get the messages by language of the user
    set task_ids [db_list tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]
    set total_number_of_task_units [lc_numeric $assignment_units "%.0f" $locale]

    db_1row languages "select source_language_id, target_language_id from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $task_ids]) limit 1 "
    set source_language [im_category_from_id -locale $locale $source_language_id]
    set target_language [im_category_from_id -locale $locale $target_language_id]
    set language_combination "[string range $source_language 0 1]_[string range $target_language 0 1]"

    set project_type [im_category_from_id -locale $locale $project_type_id]
    set task_type_pretty [im_category_from_id -locale $locale $assignment_type_id]

    set subject "[lang::message::lookup $locale sencha-freelance-translation.lt_assignment_message_subject]"
    set body "[lang::message::lookup $locale sencha-freelance-translation.lt_assignment_message_body]"

    set from_addr $project_manager_email
    set signature [db_string signature "select signature from parties where email = :from_addr" -default ""]
    if {$signature ne ""} {
        append body [template::util::richtext::get_property html_value $signature]
    }
    intranet_chilkat::send_mail \
        -to_addr $email \
        -from_addr $from_addr \
        -cc_addr $from_addr \
        -subject $subject \
        -body $body \
        -object_id $project_id \
        -use_sender 
}



ad_proc -public im_freelance_mail_project_component {
    -project_id
    -assignee_id
    {-return_url ""}
} {
    Return the mail component for a Projects Assignements of an assignee
} {
    set page [ns_queryget page]
    set params [list  [list project_id] [list assignee_id] [list page $page]]
    set result [ad_parse_template -params $params "/packages/sencha-freelance-translation/lib/messages"]
}

ad_proc -public im_freelance_notify {
    -object_id
    -recipient_ids
    {-message ""}
    {-severity "2"}
} {
    Notify the recipients and reset the view count
    
    @param severity Determine how important the note is. 0 = red, 1 = yellow, 2 = green
} {
    foreach recipient_id $recipient_ids {
        if {$message ne ""} {
            
            # Check if we already have an open notification for this object and recipient
            set notification_exists_p [db_string notif_exists "select 1 from im_freelance_notifications
                where object_id = :object_id and user_id = :recipient_id
                and (viewed_date is null or viewed_date > now() -interval '1 hour')
                and severity <= :severity limit 1" -default 0]
                
            if {!$notification_exists_p} {
                # Insert notifications
                db_dml insert_notification "insert into im_freelance_notifications
                    (user_id,object_id,creation_date,message,severity)
                    values
                    (:recipient_id,:object_id,now(),:message,:severity)"
            } else {
		# Reset the viewed date
		db_dml update_noficiation "update im_freelance_notifications set message = :message, viewed_date = NULL where user_id = :recipient_id and object_id = :object_id"
	    }
        }
            
        # Reset the view count for the object
        db_dml delete_view "delete from views_views where viewer_id = :recipient_id and object_id = :object_id"
    }
}

ad_proc -public im_freelance_notification_url {
    -object_id
} {
    Returns the URL for the object to pass through for the notification

    @param object_id Object which to visit afterwards
} {
    return [export_vars -base "/sencha-freelance-translation/notify-url" -url {object_id}]
}

ad_proc -public im_freelance_record_view {
    -object_id
    { -object_ids ""}
    -viewer_id
} {
    Record the viewing of the object. This might later tap into the
    specific notification system.
} {
    if {$object_ids eq ""} {

        db_dml update_notification "update im_freelance_notifications set viewed_date = now()
            where object_id = :object_id
            and user_id = :viewer_id
            and viewed_date is null"

        views::record_view -object_id $object_id -viewer_id $viewer_id

    } else {
        # Convert comma seperated string to list
        set object_ids_list [list] 
        foreach obj_id [split $object_ids ","] {
            lappend object_ids_list $obj_id
        }
        db_dml update_notification "update im_freelance_notifications set viewed_date = now()
            where object_id in ([template::util::tcl_to_sql_list $object_ids_list]))
            and user_id = :viewer_id
            and viewed_date is null"

        views::record_view -object_id $object_id -viewer_id $viewer_id

    }

}

ad_proc -public im_freelance_message_send {
    -sender_id
    -recipient_ids
    -subject
    -body
    -context_id
    -project_id
    {-file_ids ""}
    {-package_id ""}
} {
    Sends a message through the internal system
    
    @param file_ids Uploaded files
    @param package_id Package reference where to find the uploaded files
} {
    set to_addr [party::email -party_id [lindex $recipient_ids 0]]
    set from_addr [party::email -party_id $sender_id]
    if {$package_id eq ""} {
        set package_id [apm_package_id_from_key sencha-freelance-translation]
    }
    
    set log_id [intranet-mail::log_add \
        -recipient_ids $recipient_ids \
        -to_addr $to_addr \
        -sender_id $sender_id \
        -from_addr "$from_addr" \
        -subject "$subject" \
        -body "$body" \
        -package_id $package_id \
        -file_ids $file_ids \
        -context_id $context_id]

    db_1row project_info "select project_nr, project_lead_id from im_projects where project_id = :project_id"

    set is_context_an_assignment_p [db_string is_context_an_assignment "select 1 from im_freelance_assignments where assignee_id =:context_id" -default 0]
    
    foreach recipient_id $recipient_ids {
        # Only send that message if recipient is PM and context_id is an assignment
        if {[im_user_is_pm_p $recipient_id] && $is_context_an_assignment_p} {
            # Notify the user if not an employee (no callback)
            set assignee_id $recipient_id
            set salutation_pretty [im_invoice_salutation -person_id $assignee_id]
            set token [ns_sha1 someSeCrEt-${assignee_id}-${project_id}]
            set assignments_link [export_vars -base "[util_current_location]/sencha-freelance-translation/project-assignments" -url {token project_id assignee_id}]
    
            set subject "New message regarding $project_nr"
            set body "$salutation_pretty,
                <p>
                You have received a new message regarding project $project_nr
                </p><p>
                Log in <a href='$assignments_link'>here</a> to view this message.
                </p>
                Best regards,"
            set signature [db_string signature "select signature from parties where party_id = :project_lead_id" -default ""]
            if {$signature ne ""} {
                    append body [template::util::richtext::get_property html_value $signature]
            }
    
            intranet_chilkat::send_mail \
                -to_party_ids $assignee_id \
                -from_party_id $project_lead_id \
                -subject $subject \
                -body $body \
                -no_callback
        }
    }

    set log_link [export_vars -base "[util_current_location]/sencha-freelance-translation/one-message" -url {log_id}]
    
    im_freelance_notify \
        -object_id $log_id \
        -recipient_ids $recipient_ids \
        -message "New message for <a href='$log_link'>$project_nr</a>" \
        -severity 2

    return $log_id
}

ad_proc -public im_freelance_message_deadline_reminder {
    -project_id
    {-assignment_id ""}
    {-severity "1"}
} {
    Send a reminder about missed project deadlines / assignment deadlines


    @param project_id Project which we need to send the reminder for
    @param assignment_id If specified, send the reminder of the assignment to the assignee if overdue
} {

    db_1row project_info "select project_nr, project_lead_id from im_projects where project_id = :project_id"

    if {$assignment_id eq ""} {
        # Deadline for the project is overdue
        set project_url [export_vars -base "/intranet/projects/view" -url {project_id}]
        
        if {$severity eq "1"} {
            set message "Project <a href='$project_url'>$project_nr</a> is due soon"
        } elseif {$severity eq "0"} {
            set message "Project <a href='$project_url'>$project_nr</a> is overdue"
        } else {
            set message "Project <a href='$project_url'>$project_nr</a> is due sometime, not sure why I bother you"
        }

        im_freelance_notify \
            -object_id $project_id \
            -recipient_ids $project_lead_id \
            -message $message \
            -severity $severity
    } else {
        db_1row assignment_info "select assignee_id,purchase_order_id, freelance_package_name
            from im_freelance_assignments fa, im_freelance_packages fp
            where assignment_id = :assignment_id
            and fa.freelance_package_id = fp.freelance_package_id"
            
        set location [util_current_location]
        set assignments_link [export_vars -base "${location}/sencha-freelance-translation/project-assignments" -url {project_id assignee_id}]
    
        if {$severity eq "1"} {
            set message "Project package delivery in <a href='$assignments_link'>$project_nr</a> is due soon"
        } elseif {$severity eq "0"} {
            set message "Project package delivery in <a href='$assignments_link'>$project_nr</a> is overdue"
        }
        
        # Check if we already have an open notification for this object and recipient
        set notification_exists_p [db_string notif_exists "select 1 from im_freelance_notifications
            where object_id = :assignment_id and user_id = :assignee_id
            and (viewed_date is null or viewed_date > now() -interval '1 hour')
            and severity <= :severity limit 1" -default 0]
            
        im_freelance_notify \
            -object_id $assignment_id \
            -recipient_ids [list $project_lead_id $assignee_id] \
            -message $message \
            -severity $severity

        set notification_exists_p 1
        if {!$notification_exists_p} {
                    
            if {$severity eq 0} {
                
                if {$purchase_order_id ne ""} {
                    set purchase_order_nr [db_string invoice_nr "select invoice_nr from im_invoices
                        where invoice_id = :purchase_order_id" -default ""]
                    if {$purchase_order_nr ne ""} {
                        append project_nr " ($purchase_order_nr)"
                    }
                }
                set salutation_pretty [im_invoice_salutation -person_id $assignee_id]
    
                set subject "Missed deadline for $project_nr"
                set body "$salutation_pretty,
                    <p>
                    You have missed the deadline for the delivery of package $freelance_package_name
                    </p><p>
                    Please log in <a href='$assignments_link'>here</a> to upload the package now.
                    </p>
                    Best regards,"
                set signature [db_string signature "select signature from parties where party_id = :project_lead_id" -default ""]
                if {$signature ne ""} {
                    append body [template::util::richtext::get_property html_value $signature]
                }
                
                intranet_chilkat::send_mail \
                    -to_party_ids $assignee_id \
                    -from_party_id $project_lead_id \
                    -subject $subject \
                    -body $body
            }
        }
    }
}

ad_proc -public im_freelance_scan_overdue_projects {
    
} {
    Loop through all projects which are open and where the due date is in the past or upcoming
} {
    # ---------------------------------------------------------------
    # Due soon Projects
    # ---------------------------------------------------------------
    set open_status_ids [im_sub_categories [im_project_status_open]]
    set soon_project_ids [db_list projects_due_soon "select project_id from im_projects
        where end_date > now()
        and end_date <= now() + interval '1 hour'
        and project_status_id in ([template::util::tcl_to_sql_list $open_status_ids])"]
        
    foreach project_id $soon_project_ids {
        im_freelance_message_deadline_reminder -project_id $project_id -severity 1
    }
    
    # ---------------------------------------------------------------
    # Overdue Projects
    # ---------------------------------------------------------------

    set overdue_project_ids  [db_list projects_due_soon "select project_id from im_projects
     where end_date < now()
     and project_status_id in ([template::util::tcl_to_sql_list $open_status_ids])"]

    foreach project_id $overdue_project_ids {
        im_freelance_message_deadline_reminder -project_id $project_id -severity 0
    }
}

ad_proc -public im_freelance_scan_overdue_assignments {
    
} {
    Loop through all assignments which are open and where the due date is in the past or upcoming
} {
    # ---------------------------------------------------------------
    # Due soon Assignments
    # ---------------------------------------------------------------
    set open_status_ids [list 4222 4224 4229]
    db_foreach assignments_due_soon "select project_id,assignment_id
        from im_freelance_assignments fa, im_freelance_packages fp
        where fa.freelance_package_id = fp.freelance_package_id
        and end_date > now()
        and end_date <= now() + interval '1 hour'
        and assignment_status_id in ([template::util::tcl_to_sql_list $open_status_ids])" {
        im_freelance_message_deadline_reminder -project_id $project_id -assignment_id $assignment_id -severity 1
    }


    # ---------------------------------------------------------------
    # Overdue Assignments
    # ---------------------------------------------------------------

    db_foreach assignments_due_soon "select project_id,assignment_id
        from im_freelance_assignments fa, im_freelance_packages fp
        where fa.freelance_package_id = fp.freelance_package_id
        and end_date < now()
        and assignment_status_id in ([template::util::tcl_to_sql_list $open_status_ids])" {
        im_freelance_message_deadline_reminder -project_id $project_id -assignment_id $assignment_id -severity 0
    }
}



ad_proc -public im_rfq_reminder_all {
    -recipient_ids
    {-severity "1"}
} {
    Send a reminder about existing RFQ's

    @param user_id id to user to whom we want to send notification
} {

    set base_url "[im_rest_system_url]/sencha-tables"
    set rfq_url "$base_url/rfq"
    
    # Before execution we need to make sure that list has only available users (e.g not banned) - hence cc_users view
    set filtered_recipient_ids [db_list filtered_recipient_ids "select user_id from cc_users where user_id in ([template::util::tcl_to_sql_list $recipient_ids])"]
    
    # Temporarily we use package_id here. This should be changed later
    set package_id [apm_package_id_from_key "sencha-freelance-translation"]
    
    im_freelance_notify \
	-object_id $package_id \
	-recipient_ids $filtered_recipient_ids \
	-message  "RFQ not turned into a project are available: <a href='$rfq_url'>go to RFQ list</a>" \
	-severity $severity
}


ad_proc -public im_freelance_scan_available_rfq {

} {
    Loop thru all RFQ (table in postgresql: 'external_rfq') that were not turned into ]project-open[ project yet)
} {
    # Getting configured groups from package parameters
    set groups_to_be_notified_ids [list]

    # Building list of groups ids
    set groups_ids_from_parameters [parameter::get_from_package_key -package_key "sencha-freelance-translation" -parameter RfqNotificationGroups -default ""]
    foreach group_id_from_parameter [split $groups_ids_from_parameters " "] {
        lappend groups_to_be_notified_ids $group_id_from_parameter
    }

    # Setting empty receipents_ids
    set recipient_ids [list]
    
    # Checking db for available RFQs
    set available_rfqs_p [db_string available_rfqs_sql "select count(rfq_id) from external_rfq" -default 0]
   
    # If RFQs exists, we need to build receipt_ids list
    if {$available_rfqs_p > 0} {
        foreach group_id $groups_to_be_notified_ids {
            set group_memebers_ids [group::get_members -group_id $group_id]
            foreach group_member_id $group_memebers_ids {
                lappend recipient_ids $group_member_id
            }
        }
        # If list receipents_ids is bigger than one, we execute notifications
        if {[llength recipient_ids] > 0} {
            im_rfq_reminder_all -recipient_ids $recipient_ids -severity 1
        } 
    } 
}

ad_proc -public im_freelance_notify_project_leaders_of_inquiring_projects {

} {
    Loop thru all projects with status of id:72 (inquiring) and notify project leaders
} {

    set status_inquiring [im_project_status_inquiring]
    set update_sql "update im_freelance_notifications fn set viewed_date = NULL from im_projects p where p.project_id = fn.object_id and p.project_status_id = :status_inquiring and viewed_date is not null"
    db_dml update_notifications $update_sql
}


ad_proc -public im_accepted_assignments_without_po_reminder {
    {-severity "1"}
} {
    Send a reminder about existing assignments with status of 'Accepted', that still do not have Purchase Order

} {

    # Preparing variable for list of projects for which we want to notify PM's
    set notify_project_ids [list]
    set count 0
    # Get all tasks with status 'accepted' for give project_id 
    set status_accepted_id 4222
    set already_used_projects_ids [list]
    set project_accepted_assignments [db_list project_accepted_assignments "select assignment_id from im_freelance_assignments where assignment_status_id =:status_accepted_id and purchase_order_id is null"]

    foreach assignment_without_po $project_accepted_assignments {
        set any_package_id [db_string any_package_id "select distinct freelance_package_id from im_freelance_assignments where assignment_id =:assignment_without_po limit 1" -default 0]
        if {$any_package_id ne 0} {
           set project_id [db_string project_id "select distinct project_id from im_freelance_packages where freelance_package_id =:any_package_id limit 1" -default 0]
           
           db_1row assignment_info "select assignee_id, assignment_name from im_freelance_assignments where assignment_id =:assignment_without_po"
           set assignment_url [export_vars -base "/sencha-freelance-translation/project-assignments" -url {project_id assignee_id}]
           db_1row project_info "select project_nr, project_lead_id from im_projects where project_id = :project_id"
           
           # We must exclude all assignments ids within same project_id
           set possibly_notified_assignments_ids [db_list possibly_notified_assignments_ids "select assignment_id from im_freelance_assignments fa, im_freelance_packages fp where fa.freelance_package_id = fp.freelance_package_id and fa.assignee_id = :assignee_id and fp.project_id =:project_id"]
           if {[llength $possibly_notified_assignments_ids] > 0} {
              set assignment_exclude_sql "and object_id in ([template::util::tcl_to_sql_list $possibly_notified_assignments_ids])"
           } else {
               set assignment_exclude_sql ""
           }
           set notification_exists_db_p [db_string notification_exists_db_p "select count(object_id) from im_freelance_notifications where user_id =:project_lead_id $assignment_exclude_sql" -default 0]
           if {$notification_exists_db_p eq 0} {
               im_freelance_notify \
                   -object_id $assignment_without_po \
                   -recipient_ids $project_lead_id \
                   -message  "Project: <a href='$assignment_url'>$project_nr</a> has accepted assignments with missing PO</a>" \
                   -severity $severity
                   lappend already_used_projects_ids $project_id
                   incr count

            }
        }
    }

}


