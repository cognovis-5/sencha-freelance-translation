## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#

ad_library {
    
    Init and scheduling
}





if {[parameter::get_from_package_key -package_key "sencha-freelance-translation" -parameter ScanAndNotifyProjectLeadersOfInquiringStatuses] eq "t"} {
    ad_schedule_proc -thread t 304 im_freelance_notify_project_leaders_of_inquiring_projects
    ns_log Notice "Scanning for inquiring projects and notifying project leaders"
}

if {[parameter::get_from_package_key -package_key "sencha-freelance-translation" -parameter ScanForAcceptedAssignmentsWithoutPo] eq "t"} {
    ad_schedule_proc -thread t 303 im_accepted_assignments_without_po_reminder
    ns_log Notice "Scanning for accepted Assignments without PO"
}

if {[parameter::get_from_package_key -package_key "sencha-freelance-translation" -parameter ScanForRfq] eq "t"} {
    ad_schedule_proc -thread t 302 im_freelance_scan_available_rfq
    ns_log Notice "Scanning for RFQs"
}


if {[parameter::get_from_package_key -package_key "sencha-freelance-translation" -parameter ScanOverdueAssignments] eq "t"} {
    ad_schedule_proc -thread t 301 im_freelance_scan_overdue_assignments
    ns_log Notice "Scanning for overdue assignments"
}

if {[parameter::get_from_package_key -package_key "sencha-freelance-translation" -parameter ScanOverdueProjects] eq "t"} {
    ad_schedule_proc -thread t 300 im_freelance_scan_overdue_projects
    ns_log Notice "Scanning for overdue projects"
}
    

