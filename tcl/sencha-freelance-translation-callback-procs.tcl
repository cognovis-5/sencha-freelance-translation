## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#

ad_library {
    
    Procedures for the callbacks where we hook into
    
    @author <yourname> (<your email>)
    @creation-date 2012-01-06
    @cvs-id $Id$
}

ad_proc -public -callback project_assignment_before_view {
    {-assignee_id:required}
    {-project_id:required}
    {-user_id:required}
} {
    This callback allows to redirect to other locations (e.g. the portals) for users wanting to 
    see an assignment.
    
    @param assignee_id Assignee for which we are showing the assignments
    @param project_id Project for which we show the assignments
    @param user_id User who is currently viewing the page. Might be due to auto login, therefore we are not using require_login.
} -

ad_proc -public -callback im_invoice_before_nuke -impl sencha_freelance_assignment {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Remove the PO from the assignment
} {
    ns_log Notice "Updateing assignments for po $object_id"
    db_dml update_assignments "update im_freelance_assignments set purchase_order_id = null where purchase_order_id = :object_id"
    return
}

ad_proc -public -callback user_messages_before_render -impl sencha_freelance_notification {
    {-user_id:required}
} {
    This callback is executed on every single page, so be especially careful.
} {
    if {[parameter::get -package_id [apm_package_id_from_key sencha-freelance-translation] -parameter "EnableUserNotifications" -default 1]} {
	uplevel {
	    db_foreach messages "select distinct message,severity from im_freelance_notifications
            where viewed_date is null
            and user_id = [ad_conn user_id] order by severity desc, message" {
		template::multirow append user_messages $count $message $severity
    		incr count
	    }
	}
    }
}

ad_proc -public -callback im_project_after_update -impl sencha_freelance_assignments_close {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Set assignments to closed if still in an open status
} {

    set closed_ids [im_sub_categories 81]
    if {[lsearch $closed_ids $status_id]>-1} {
        # Check the open assignments
        set open_status_ids [list 4222 4224 4229]
        db_foreach assignments_due_soon "select assignment_id
            from im_freelance_assignments fa, im_freelance_packages fp
            where fa.freelance_package_id = fp.freelance_package_id
            and assignment_status_id in ([template::util::tcl_to_sql_list $open_status_ids])
            and fp.project_id = :object_id
        " {
            db_dml close_assignments "update im_freelance_assignments set assignment_status_id = 4231 where assignment_id = :assignment_id"

            # Mark notifications as viewed
            db_dml update_notification "update im_freelance_notifications set viewed_date = now()
                where object_id = :object_id
                and viewed_date is null"
        }
    }
}

ad_proc -public -callback im_project_after_update -impl sencha_freelance_remove_notifications {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Remove Notifications for projects which are closed
} {

    set closed_ids [im_sub_categories 81]
    if {[lsearch $closed_ids $status_id]>-1} {
	db_dml update_notifications "update im_freelance_notifications set viewed_date = now() where object_id = :object_id and viewed_date is null"
    }
}

ad_proc -public -callback im_project_view -impl sencha_freelance_remove_notifications {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Remove Notifications for projects which are being viewed
} {

    db_dml update_notifications "update im_freelance_notifications set viewed_date = now() where object_id = :object_id and viewed_date is null"
}
