# /packages/sencha-freelance-translation/tcl/sencha-freelance-translation-procs.tcl
#
# Copyright (c) 2018 cognovis Gmbh
#

ad_library {
    Common procedures to implement translation freelancer functions:
}

# ---------------------------------------------------------------
# Select Freelancers to projects
# ---------------------------------------------------------------

ad_proc -public im_rest_get_custom_trans_freelancers {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Handler for getting information on Translation freelancers

	Requires the project_id

	Returns a JSON with
	- Freelancer ID and name
	- Source & Target languages
	- Skills (to be defined which ones)
	- Rates (source word & hours)

	See /sencha-freelance-translation/lib/freelance-trans-member.tcl for details
} {
    set result ""
	array set query_hash $query_hash_pairs
	if {[info exists query_hash(project_id)]} {
		set project_id $query_hash(project_id)
	} else {
		# We can't work without a project!
		# im_rest_error -format "json" -http_status 403 -message "Missing a project_id"
		set project_id 0
	}
	
	# Anyone who can write on this project can query the freelancers for it
	im_project_permissions $rest_user_id $project_id view_p read_p write_p admin_p
	if {!$write_p} {return}

	set valid_vars [list freelancer_name no_times_worked_for_customer no_times_trans_for_customer no_times_edit_for_customer price_s_word price_hour selected_p potential_p active_project_member_p matched_target_lang_id sort_order source_language_exp_level target_language_exp_level matched_lang_exp_level existing_assignment_status_id matched_lang_exp_level_id]

	# Get the skill information for the skills to return
	set skill_type_sql ""

	db_foreach skill_types {select skill_type_id, object_type_id, display_mode
		from im_freelance_skill_type_map, im_projects
		where object_type_id = project_type_id and project_id = :project_id and display_mode != 'none'
	} {
			append skill_type_sql "\t\t(select '\[' || array_to_string(array_agg(skill_id), ',') || '\]' from im_freelance_skills where skill_type_id = $skill_type_id and user_id = m.member_id) as skill_$skill_type_id,\n"
#			append skill_type_sql "\t\tim_freelance_skill_list(m.member_id, $skill_type_id) as skill_$skill_type_id,\n"
			lappend valid_vars "skill_$skill_type_id"
	}

	#set target_language_exp_level "dummy"
	
	# Source & Target
	foreach skill_type_id [list 2000 2002] {
		if {[lsearch $valid_vars skill_$skill_type_id]<0} {
				append skill_type_sql "\t\t(select '\[' || array_to_string(array_agg(skill_id), ',') || '\]' from im_freelance_skills where skill_type_id = $skill_type_id and user_id = m.member_id) as skill_$skill_type_id,\n"
				lappend valid_vars "skill_$skill_type_id"
		}
	}

	# ---------------------------------------------------------------
	# Languages
	# ---------------------------------------------------------------

	# Project's Source & Target Languages
    if {[db_0or1row source_langs "select  source_language_id, ch.parent_id
				from    im_projects left outer join im_category_hierarchy ch on (child_id = source_language_id)
				where   project_id = :project_id"]} {

	set source_languages [list]

	if {[exists_and_not_null parent_id]} {
	    set source_language_ids [im_sub_categories $parent_id]
	} else {
	    set source_language_ids [im_sub_categories $source_language_id]
	}
    } else {
	# Seems to be missing something
	set source_language_ids ""
    }

	set target_language_ids [list]
	db_foreach target_langs "
			select  language_id as target_language_id, parent_id
			from	im_target_languages left outer join im_category_hierarchy ch on (child_id = language_id)
			where	project_id = :project_id
	" {
		if {[exists_and_not_null parent_id]} {
			set target_ids [im_sub_categories $parent_id]
		} else {
			set target_ids [im_sub_categories $target_language_id]
		}
		foreach target_id $target_ids {
			if {[lsearch $target_language_ids $target_id]<0} {
				lappend target_language_ids $target_id
			}
		}
	}



	if {$project_id eq 0} {


		set target_language_ids [list]
		set source_language_ids [list]
        set sql "select l.language_id as target_language
        from im_target_languages l
	    "
	    db_foreach select_target_languages $sql {
	       lappend target_language_ids $target_language
	       lappend source_language_ids $target_language
        }
	}

	if {$source_language_ids ne ""} {
		set source_lang_from ",(select user_id from im_freelance_skills where skill_type_id = 2000 and skill_id in ([template::util::tcl_to_sql_list $source_language_ids])) source_lang"
		set source_lang_where "AND m.member_id = source_lang.user_id"
	} else {
		set source_lang_from ""
		set source_lang_where ""
		set source_lang ""
	}


	# Override if we have the skill_2002
	if {[info exists query_hash(skill_2002)]} {
		set target_language_ids $query_hash(skill_2002)
	}
	if {$target_language_ids ne ""} {
		set target_lang_from ",(select user_id from im_freelance_skills where skill_type_id = 2002 and skill_id in ([template::util::tcl_to_sql_list $target_language_ids])) target_lang"
		set target_lang_where " AND m.member_id = target_lang.user_id"
	} else {
		set target_lang_from ""
		set target_lang_where ""
		set target_lang ""
	}

	set customer_id [db_string company "select coalesce(final_company_id,company_id) as company_id from im_projects where project_id = :project_id" -default "0"]

	set company_status_ids [im_sub_categories [im_company_status_active_or_potential]]

	# Get the project_type to find out the order of trans steps

	set trans_steps [split [db_string get_steps "select aux_string1 from im_categories c, im_projects p where p.project_id = :project_id and p.project_type_id = c.category_id" -default ""] " "]
	
    set project_type_id [db_string project_type_id "select project_type_id from im_projects where project_id =:project_id" -default 0]
    

	set freelance_sql "
		select distinct
			m.member_id as user_id
		from
			group_member_map m,
			(select object_id_two, company_status_id from acs_rels r, im_companies c where c.company_id = r.object_id_one and company_status_id in ([template::util::tcl_to_sql_list $company_status_ids])) f
			$source_lang_from
			$target_lang_from
		where m.group_id = [im_profile_freelancers] and m.member_id not in (
				-- Exclude banned or deleted users
				select	m.member_id
				from	group_member_map m,
					membership_rels mr
				where	m.rel_id = mr.rel_id and
					m.group_id = acs__magic_object_id('registered_users') and
					m.container_id = m.group_id and
					mr.member_state != 'approved') AND
			f.object_id_two = m.member_id
			$source_lang_where
			$target_lang_where
		"
			
	# ---------------------------------------------------------------
	# Build the price array for freelancers
	# ---------------------------------------------------------------
   	set price_sql "
		select
			f.user_id,
			p.uom_id,
			im_category_from_id(p.task_type_id) as task_type,
			im_category_from_id(p.source_language_id) as source_language,
			im_category_from_id(p.target_language_id) as target_language,
			im_category_from_id(p.subject_area_id) as subject_area,
			im_category_from_id(p.file_type_id) as file_type,
			min(p.price) as min_price,
			max(p.price) as max_price
		from
			($freelance_sql) f
			LEFT OUTER JOIN acs_rels uc_rel ON (f.user_id = uc_rel.object_id_two)
			LEFT OUTER JOIN im_trans_prices p ON (uc_rel.object_id_one = p.company_id)
		where
			(p.source_language_id in ([template::util::tcl_to_sql_list $source_language_ids]) or
				p.source_language_id is null)
			and (p.target_language_id in ([template::util::tcl_to_sql_list $target_language_ids]) or
				p.target_language_id is null)
		group by
			f.user_id,
			p.uom_id,
			p.task_type_id,
			p.source_language_id,
			p.target_language_id,
			p.subject_area_id,
			p.file_type_id
			order by task_type"

	db_foreach price_hash $price_sql {
		set key "$user_id-$uom_id"

		# Calculate the base cell value
		set price_append "$min_price - $max_price"
		if {$min_price == $max_price} { set price_append "$min_price" }
		# Add the list of parameters
		set param_list [list $price_append $source_language $target_language]
		if {"" == $source_language && "" == $target_language} { set param_list [list $price_append] }
		if {"" != $subject_area} { lappend param_list $subject_area }
		if {"" != $task_type} { lappend param_list $task_type }
		if {"" != $file_type} { lappend param_list $file_type }

		# Update the hash table cell
		set hash ""
		if {[info exists price_hash($key)]} { set hash $price_hash($key) }
		set hash [im_freelance_trans_member_select_component_join_prices $hash $param_list]
		set price_hash($key) $hash
	}

	foreach key [array names price_hash] {
		set values $price_hash($key)
		set result_list [list]
		foreach value $values {
			lappend result_list "\"[lindex $value 0] {[lrange $value 1 end]}\""
		}
		set price_hash($key) "\[[join $result_list ,]\]"
	}

	# ---------------------------------------------------------------
	# Generate the JSON for each freelancer
	# ---------------------------------------------------------------

	set obj_ctr 0
	
    # ---------------------------------------------------------------
    # Generate the from clauses for sorting
    # ---------------------------------------------------------------
    set from_sql ""

    # Get the confirmed assignees
    set confirmed_assignee_ids [db_list assignees "select assignee_id from im_freelance_assignments fa, im_freelance_packages fp 
         where fp.freelance_package_id = fa.freelance_package_id and fp.project_id = :project_id and assignment_status_id in (4222,4225,5225,4226,4227,4229)"]

    if {$confirmed_assignee_ids ne ""} {
	append from_sql "case when m.member_id in ([template::util::tcl_to_sql_list $confirmed_assignee_ids]) then '1' else '0' end as confirmed_assignee_p,"
    } else {
	append from_sql "0 as confirmed_assignee_p,"
    }


    # Get the requested assignees
    set requested_assignee_ids [db_list assignees "select assignee_id from im_freelance_assignments fa, im_freelance_packages fp 
         where fp.freelance_package_id = fa.freelance_package_id and fp.project_id = :project_id and assignment_status_id in (4221,4220)"]

    if {$requested_assignee_ids ne ""} {
	append from_sql "case when m.member_id in ([template::util::tcl_to_sql_list $requested_assignee_ids]) then '1' else '0' end as requested_assignee_p,"
    } else {
	append from_sql "0 as requested_assignee_p,"
    }
    

    # Get the project members
    set project_member_ids [im_biz_object_member_ids $project_id]


    # Assignees are also project members
    set project_member_ids [concat $project_member_ids $confirmed_assignee_ids $requested_assignee_ids]	

    if {$project_member_ids ne ""} {
	append from_sql "case when m.member_id in ([template::util::tcl_to_sql_list $project_member_ids]) then '1' else '0' end as active_project_member_p,"
    } else {
	append from_sql "0 as active_project_member_p,"
    }
    
    # Get the main translators
    set sql "select distinct freelancer_id from im_trans_main_freelancer where customer_id = :customer_id and source_language_id in ([template::util::tcl_to_sql_list $source_language_ids]) and target_language_id in ([template::util::tcl_to_sql_list $target_language_ids])"
    set main_translator_ids [db_list freelancer "$sql"]

    if {$main_translator_ids ne ""} {
	append from_sql "case when m.member_id in ([template::util::tcl_to_sql_list $main_translator_ids]) then '1' else '0' end as main_trans_p,"
    } else {
	append from_sql "0 as main_trans_p,"
    }

    # Potential Members should be shown afterwards
    set potential_status_ids [im_sub_categories [im_company_status_potential]]


    set source_language_exp_level_sql "(select max(im_categories.aux_int1) AS experience from im_freelance_skills inner join im_categories ON (im_categories.category_id = im_freelance_skills.confirmed_experience_id) where im_categories.category_type = 'Intranet Experience Level' AND im_freelance_skills.user_id = m.member_id AND im_freelance_skills.skill_id in ([template::util::tcl_to_sql_list $source_language_ids]) AND im_freelance_skills.skill_type_id = 2000) as source_lang_experience,"
	

	#set add_language_exp_level_sql "(select im_categories.aux_int1 AS experience from im_freelance_skills inner join im_categories ON (im_categories.category_id = im_freelance_skills.confirmed_experience_id) where im_categories.category_type = 'Intranet Experience Level' AND im_freelance_skills.user_id = m.member_id AND im_freelance_skills.skill_id in ([template::util::tcl_to_sql_list $target_language_ids]) AND im_freelance_skills.skill_type_id = 2002) as target_experien,"

	set sort_order 0
	set max_quality_projects 0

    set freelancer_ids 	[db_list freelancers "select distinct
				m.member_id as user_id
			from
				group_member_map m,
				(select object_id_two, company_status_id from acs_rels r, im_companies c where c.company_id = r.object_id_one and company_status_id in ([template::util::tcl_to_sql_list $company_status_ids])) f
				$source_lang_from
				$target_lang_from
			where m.group_id in ([im_profile_freelancers],[im_profile_employees]) and m.member_id not in (
					-- Exclude banned or deleted users
					select	m.member_id
					from	group_member_map m,
						membership_rels mr
					where	m.rel_id = mr.rel_id and
						m.group_id = acs__magic_object_id('registered_users') and
						m.container_id = m.group_id and
						mr.member_state != 'approved') AND
				f.object_id_two = m.member_id
				$source_lang_where
				$target_lang_where"]

    if {$freelancer_ids ne ""} {
	foreach task_type $trans_steps {
	    db_foreach customers "select count(*) as worked_with, freelancer_id from 
				(select distinct tree_root_key(p.tree_sortkey), ${task_type}_id as freelancer_id
				 	from im_trans_tasks t, im_projects p
				 	where (p.company_id = :customer_id or p.final_company_id = :customer_id)
				 	and p.project_id = t.project_id
                                        and ${task_type}_id in ([template::util::tcl_to_sql_list $freelancer_ids])) t group by freelancer_id" {
					    set worked_with_${task_type}_customer($freelancer_id) $worked_with
					}
	    db_foreach worked_with "select count(distinct t.project_id) as worked_with, ${task_type}_id as freelancer_id from im_trans_tasks t where ${task_type}_id in ([template::util::tcl_to_sql_list $freelancer_ids]) group by freelancer_id" {
		set worked_with_${task_type}($freelancer_id) $worked_with
	    }
	}
    }
	set freelance_sql "
			select distinct
				m.member_id as user_id,
				$source_language_exp_level_sql
				im_name_from_user_id(m.member_id) as object_name,
				$skill_type_sql
				$from_sql
				case when company_status_id in ([template::util::tcl_to_sql_list $potential_status_ids]) then '1' else '0' end as potential_p
			from
				group_member_map m,
				(select object_id_two, company_status_id from acs_rels r, im_companies c where c.company_id = r.object_id_one and company_status_id in ([template::util::tcl_to_sql_list $company_status_ids])) f
				$source_lang_from
				$target_lang_from
			where m.group_id in ([im_profile_freelancers],[im_profile_employees]) and m.member_id not in (
					-- Exclude banned or deleted users
					select	m.member_id
					from	group_member_map m,
						membership_rels mr
					where	m.rel_id = mr.rel_id and
						m.group_id = acs__magic_object_id('registered_users') and
						m.container_id = m.group_id and
						mr.member_state != 'approved') AND
				f.object_id_two = m.member_id
				$source_lang_where
				$target_lang_where
			order by confirmed_assignee_p desc, requested_assignee_p desc, active_project_member_p desc, main_trans_p desc, potential_p desc, object_name desc
		" 

    db_foreach select_freelancers $freelance_sql {
		incr sort_order
		set freelancer_name $object_name
		set source_language_exp_level $source_lang_experience

		# Append the prices
		set uom_listlist "{324 price_s_word} {320 price_hour}"
		foreach uom_tuple $uom_listlist {
			set uom_id [lindex $uom_tuple 0]
			set var_name [lindex $uom_tuple 1]
			set key "${user_id}-${uom_id}"
			set $var_name ""
			if { [info exists price_hash($key)] } {
				set $var_name $price_hash($key)
			}
		}

		# ---------------------------------------------------------------
		# Find out the number worked with
		# The frontend assumes "trans + edit" as steps
		# We therefore have to ammend this to the real steps
		# Eventually have to change frontend :-)
		# ---------------------------------------------------------------
		    if {[info exists worked_with_[lindex $trans_steps 0]_customer($user_id)]} {
			set no_times_trans_for_customer [set worked_with_[lindex $trans_steps 0]_customer($user_id)]
		    } else {
			set no_times_trans_for_customer 0
		    }
		    if {[llength $trans_steps]>1 && [info exists worked_with_[lindex $trans_steps 1]_customer($user_id)]} {
			set no_times_edit_for_customer [set worked_with_[lindex $trans_steps 1]_customer($user_id)]
		} else {
			set no_times_edit_for_customer 0
		}

		set no_times_worked_for_customer [expr $no_times_trans_for_customer + $no_times_edit_for_customer]
		    if {$no_times_worked_for_customer <2 && [info exists worked_with_[lindex $trans_steps 0]($user_id)]} {
			set no_times_trans_for_customer [set worked_with_[lindex $trans_steps 0]($user_id)]
			if {[llength $trans_steps]>1 && [info exists worked_with_[lindex $trans_steps 1]($user_id)]} {
			    set no_times_edit_for_customer [set worked_with_[lindex $trans_steps 1]($user_id)]
			} else {
				set no_times_edit_for_customer 0
			}
		}
		
		# ---------------------------------------------------------------
		# Matched Target Language
		# ---------------------------------------------------------------
		set matched_target_lang_id [list]
		
		regsub -all {\[} $skill_2002 {} target_skill_ids
		regsub -all {\]} $target_skill_ids {} target_skill_ids
		foreach match_target_lang_id [split $target_skill_ids ","] {
			if {[lsearch $target_language_ids $match_target_lang_id]>-1} {
				lappend matched_target_lang_id $match_target_lang_id
			}
		}

        db_0or1row target_language_exp_level_sql "select im_categories.aux_int1 AS target_language_exp_level from im_freelance_skills inner join im_categories ON (im_categories.category_id = im_freelance_skills.confirmed_experience_id) where im_categories.category_type = 'Intranet Experience Level' AND im_freelance_skills.user_id = :user_id AND im_freelance_skills.skill_id in ([template::util::tcl_to_sql_list $match_target_lang_id]) AND im_freelance_skills.skill_type_id = 2002"
		set $target_language_exp_level target_language_exp_level

		db_0or1row matched_lang_experience_level "select im_categories.category_id AS matched_lang_exp_level_id, im_categories.aux_int1 AS target_lang_experience_from_sql_int, im_categories.category AS target_lang_experience_from_sql_string  from im_freelance_skills inner join im_categories ON (im_categories.category_id = im_freelance_skills.confirmed_experience_id) where im_categories.category_type = 'Intranet Experience Level' AND im_freelance_skills.user_id = :user_id AND im_freelance_skills.skill_id in([template::util::tcl_to_sql_list $match_target_lang_id]) AND im_freelance_skills.skill_type_id = 2002"
        set matched_lang_exp_level $target_lang_experience_from_sql_string
        set matched_lang_exp_level_id $matched_lang_exp_level_id

		# ---------------------------------------------------------------
		# Active Project Members have the relationship_id set
		# ---------------------------------------------------------------
		set object_id $user_id
		if {[lsearch $project_member_ids $user_id]>-1} {
			set selected_p 1
		} else {
		    set selected_p 0
		}
		
		# ---------------------------------------------------------------
		# Quality JSON
		# ---------------------------------------------------------------
		set quality_list [im_trans_freelancer_quality_json -freelancer_id $user_id]
		set quality [lindex $quality_list 1]
		
		# Find the maximum number of projects to display in the quality bar
		set num_projects [lindex $quality_list 0]
		if {$max_quality_projects < $num_projects} {
			set max_quality_projects $num_projects
		}

        set existing_assignment_status_id [db_string check_existing_assignment_status "select assignment_status_id from im_freelance_assignments fa, im_freelance_packages fp where assignee_id =:user_id and project_id =:project_id and fa.freelance_package_id = fp.freelance_package_id order by assignment_id desc limit 1" -default 0]
	    
		switch $format {
			html {
			append result "<tr>
				<td>$rest_oid</td>
				<td><a href=\"$url?format=html\">$project_name</a></td>
				<td>$project_manager</td>
			</tr>\n"
			}
		json {
			set komma ",\n"
			if {0 == $obj_ctr} { set komma "" }
			set dereferenced_result ""
			foreach v $valid_vars {
				eval "set a $$v"
				regsub -all {\n} $a {\n} a
				regsub -all {\r} $a {} a
				append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
			}
			
			append dereferenced_result ", \"quality\": $quality"

            set force_not_showing_fl_p 0
            set project_type_cat_name [im_category_from_id $project_type_id]
            set searching_for_word "Cert"
            if {$project_type_id == 2505 || [string first $project_type_cat_name $searching_for_word] > -1} {
            	if {$matched_lang_exp_level_id ne 2204} {
            		set force_not_showing_fl_p 1
            	}
            }

            if {$force_not_showing_fl_p ne 1} {
			    append result "$komma{\"id\": \"$object_id\", \"object_name\": \"[im_quotejson $object_name]\"$dereferenced_result}"
			}
		}
		default {}
	}
		incr obj_ctr
	}

	set project_status_id [db_string project_status_id "select project_status_id from im_projects where project_id=:project_id" -default 0]
    

	switch $format {
		html {
			set page_title "object_type: $rest_otype"
			im_rest_doc_return 200 "text/html" "
			[im_header $page_title [im_rest_header_extra_stuff]][im_navbar]<table>
			<tr class=rowtitle><td class=rowtitle>object_id</td><td class=rowtitle>Link</td></tr>$result
			</table>[im_footer]
			"
		}
		json {
			set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"project_type_id\": $project_type_id,\n\"project_status_id\": $project_status_id,\n\"message\": \"im_rest_get_custom_trans_freelancers: Data loaded..\",\n\"max_quality_projects\": $max_quality_projects,\n\"data\": \[\n$result\n\]\n}"
			im_rest_doc_return 200 "application/json" $result
			return
		}
	}
	return
}

ad_proc -public freelancer_project_add_permissions {
	rest_user_id
	rest_oid
	view_p
	read_p
	write_p
	admin_p
} {
	Pseudo permissions for freelance_main_translators
} {
	upvar $write_p write_p
	im_project_permissions $rest_user_id $project_id view read write admin
}

ad_proc -public im_rest_get_custom_freelance_main_translators {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
	{ -customer_id ""}
	{ -freelancer_id ""}
} {
	Get the list of main translators for a company
	or the companies a translator is working for
	
	@param customer_id	ID of the customer for whom we display the list of main translators
	@param freelancer_id ID of the freelancer (User) for whom we display the list of companies she is a main translator for
	
	@return customer_id ID of the customer
	@return customer_name Name of the customer
	@return freelancer_id ID of the main translator
	@return freelancer_name Name of the main translator 
	@return source_language_id im_categories.category_id of the source language the freelancer is a main translator for this company
	@return source_language category_name (non translated) of the language the freelancer is a main translator for this customer
	@return target_language_id im_categories.category_id of the target language the freelancer is a main translator for this company
	@return target_language category_name (non translated) of the target language the freelancer is a main translator for this customer
} {

	if {$customer_id ne ""} {
		set where_clause "where customer_id = $customer_id"
	} elseif {$freelancer_id ne ""} {
		set where_clause "where freelancer_id = $freelancer_id"
	} else {
		set where_clause ""
	}

	set sql "select mapping_id as object_id, customer_id, im_name_from_id(customer_id) as customer_name, freelancer_id,
		im_name_from_id(freelancer_id) as freelancer_name, source_language_id, im_name_from_id(source_language_id) as source_language, target_language_id, im_name_from_id(target_language_id) as target_language
		from im_trans_main_freelancer
		$where_clause
		order by customer_name, freelancer_name, source_language, target_language"
		
	set valid_vars [list customer_id customer_name freelancer_id freelancer_name source_language_id source_language target_language_id target_language]
	set obj_ctr 0
	set result ""
	db_foreach objects $sql {
		
		set komma ",\n"
		if {0 == $obj_ctr} { set komma "" }
		set dereferenced_result ""
		foreach v $valid_vars {
			eval "set a $$v"
			regsub -all {\n} $a {\n} a
			regsub -all {\r} $a {} a
			append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
		}
	
		set object_name "$customer_name -> $freelancer_name ($obj_ctr)"
		append result "$komma{\"id\": \"$object_id\", \"object_name\": \"$object_name\"$dereferenced_result}"
		incr obj_ctr
	}
	
	set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_sencha_biz_object_member: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
	im_rest_doc_return 200 "application/json" $result
	return
}

ad_proc -public freelance_main_translators_permissions {
	rest_user_id
	rest_oid
	view_p
	read_p
	write_p
	admin_p
} {
	Pseudo permissions for freelance_main_translators
} {
	upvar $write_p local_write_p
	set local_write_p 1
	upvar $admin_p local_admin_p
	set local_admin_p 1
    ns_log Notice "Set admin and write to 1"
}
	
ad_proc -public im_rest_post_object_type_freelance_main_translators {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for POST calls on the main translators.
} {
    ns_log Notice "im_rest_post_freelance_main_translators: rest_oid=$rest_oid"

    # Permissions
    # ToDo

    # Extract a key-value list of variables from JSON POST request
    array set hash_array [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]

	if {$hash_array(customer_id) eq 0} {
		set hash_array(rest_oid) "0"
    	return [array get hash_array]
	}
    # Check that all required variables are there
    set required_vars [list customer_id freelancer_id source_language_id target_language_id]
    foreach var $required_vars {
		if {![info exists hash_array($var)]} {
		    return [im_rest_error -format $format -http_status 406 -message "Variable '$var' not specified. The following variables are required: $required_vars"]
		} else {
		    if {$hash_array($var) eq 0} {
			return [im_rest_error -format $format -http_status 406 -message "Variable '$var' set to 0 which is not filled out. The following variables are required: $required_vars"]
		    } else {
			set $var $hash_array($var)
		    }
		}
    }
    
    set mapping_id [db_string exists_freelance "select mapping_id from im_trans_main_freelancer where customer_id = :customer_id
    	and freelancer_id = :freelancer_id
	and source_language_id = :source_language_id
	and target_language_id = :target_language_id" -default 0]

    if {$mapping_id eq 0} {
		db_dml insert "insert into im_trans_main_freelancer (customer_id, freelancer_id, source_language_id, target_language_id) values (:customer_id, :freelancer_id, :source_language_id, :target_language_id)"
		set object_id  [db_string mapping_id "select mapping_id from im_trans_main_freelancer where customer_id = :customer_id
	    	and freelancer_id = :freelancer_id
			and source_language_id = :source_language_id
			and target_language_id = :target_language_id" -default 0]
	    ns_log Notice "FREELANCE INSERT:: $object_id ... $customer_id ... $freelancer_id ... $source_language_id  .... 	$target_language_id"
    }

    set hash_array(rest_oid) "$mapping_id"
    return [array get hash_array]
}

ad_proc -public freelance_main_translators_nuke {
    {-current_user_id ""}
    mapping_id
} {
    Delete the mapping_id from the database
} {
    db_dml main_translator_delete "delete from im_trans_main_freelancer where mapping_id = :mapping_id"
}



ad_proc -public im_trans_freelancer_quality {
	{-freelancer_id}
	{-limit "50"}
} {
	Return a list of of lists with the freelancer quality and now often it was achieved
} {
	set freelancer_quality_list [list]
	db_foreach freelancer_quality "select count(project_id) as num_projects, quality_id, aux_int1
		from (select p.project_id, coalesce(quality_id,0) as quality_id,
			coalesce(aux_int1,0) as aux_int1,
			coalesce(rating_date, end_date) as rating_date
			from acs_rels r, im_projects p left outer join
				(select project_id, rating_date, quality_id, aux_int1
					from im_freelancer_quality fq, im_categories c
					where fq.quality_id = c.category_id and fq.user_id = :freelancer_id) fq_outer on
				fq_outer.project_id = p.project_id
			where r.object_id_one = p.project_id and r.object_id_two = :freelancer_id
			order by rating_date desc limit :limit) f
		group by quality_id, aux_int1" {
		lappend freelancer_quality_list [list $quality_id $aux_int1 $num_projects]
	}
	return $freelancer_quality_list
}

ad_proc -public im_trans_freelancer_quality_json {
	{-freelancer_id}
	{-limit "50"}
} {
	Return a JSON Array
} {
	set freelancer_json_list [list]
		
	# Set to the num_projects to count down correctly
	set total_num_projects 0
	foreach freelancer_quality [im_trans_freelancer_quality -freelancer_id $freelancer_id -limit $limit] {
		set quality [lindex $freelancer_quality 1]
		set num_projects [lindex $freelancer_quality 2]
		set total_num_projects [expr $total_num_projects + $num_projects]
		lappend freelancer_json_list "\{\"quality\": $quality, \"num_projects\": $num_projects\}"
	}
	
	set return_json "\[[join $freelancer_json_list ","]\]"
	return [list $total_num_projects $return_json]
}

ad_proc -public im_rest_get_custom_trans_freelancer_projects {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Return a list of projects of the currently logged in freelancer
} {
	
	set valid_vars [list project_nr project_name total_units deadline]

	set obj_ctr 0
	set result ""

	db_foreach assignment { select p.project_id,p.project_nr, to_char(max(fa.end_date),'YYYY-MM-DD HH24:MI') as deadline, sum(assignment_units) as total_units, p.project_name
		from im_freelance_assignments fa, im_projects p, im_freelance_packages fp
		where fa.freelance_package_id = fp.freelance_package_id
		and fp.project_id = p.project_id
		and fa.assignee_id = :rest_user_id
		and fa.assignment_status_id in (4222,4224,4227)
		group by p.project_id,p.project_nr, p.project_name
		order by deadline asc
	} {
		set komma ",\n"
		if {0 == $obj_ctr} { set komma "" }
		set dereferenced_result ""
		foreach v $valid_vars {
			eval "set a $$v"
			regsub -all {\n} $a {\n} a
			regsub -all {\r} $a {} a
			append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
		}
		append result "$komma{\"id\": \"${project_id}\", \"object_name\": \"[im_quotejson "$project_nr"]\"$dereferenced_result}"
		incr obj_ctr
	}
	set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_custom_trans_freelancer_projects: Data loaded..\",\n\"data\": \[\n$result\n\]\n}"
	im_rest_doc_return 200 "application/json" $result
	return
}

ad_proc -public im_rest_get_custom_trans_freelancer_participation_requests {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Return a list of participation of the currently logged in freelancer
} {

	set valid_vars [list project_nr task_type_pretty task_units task_uom_pretty deadline participation_id]

	set obj_ctr 0
	set result ""
	db_foreach participation_request "select participation_id, task_ids, task_type_id, uom_id, project_nr,deadline from im_projects p, im_trans_participation tp where project_status_id = [im_project_status_open] and p.project_id = tp.project_id and tp.user_id = :rest_user_id and reply_date is null" {
		set komma ",\n"
		if {0 == $obj_ctr} { set komma "" }
		set dereferenced_result ""

		set task_units [db_string sum "select sum(task_units) from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $task_ids])" -default 0]
		set task_type_pretty [im_category_from_id $task_type_id]
		set task_uom_pretty [im_category_from_id $uom_id]
		set dereferenced_result ""
		foreach v $valid_vars {
			eval "set a $$v"
			regsub -all {\n} $a {\n} a
			regsub -all {\r} $a {} a
			append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
		}
		append result "$komma{\"id\": \"$participation_id\", \"object_name\": \"[im_quotejson "$project_nr $task_type_pretty $task_uom_pretty"]\"$dereferenced_result}"
		incr obj_ctr

	}

	set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_custom_trans_freelancer_projects: Data loaded..\",\n\"data\": \[\n$result\n\]\n}"
	im_rest_doc_return 200 "application/json" $result
	return
}

ad_proc -public im_rest_get_custom_trans_freelancer_participation_request {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Return the data of a single participation request.
} {
	
	array set query_hash $query_hash_pairs
	
	if {![info exists query_hash(participation_id)]} {
		return [im_rest_error -format $format -http_status 406 -message "Variable 'participation_id' not specified."]
	} else {
		set participation_id $query_hash(participation_id)
	}
	
	set single_vars [list participation_id price_per_unit rate_uom_pretty total_amount source_language project_name project_nr deadline briefing subject_area project_lead_name project_lead_id num_tasks accepted_p email work_phone]

	db_1row participation_result "select task_ids,
			task_type_id, im_name_from_id(task_type_id) as task_type,
			p.project_id, project_name, project_nr,
			rate as price_per_unit, uom_id, total_amount,
			source_language_id, deadline, briefing,
			im_name_from_id(subject_area_id) as subject_area,
			im_name_from_id(project_lead_id) as project_lead_name,
			email, work_phone, project_lead_id
		from im_trans_participation tp, im_projects p, users_contact u, parties pa
		where tp.project_id = p.project_id
			and pa.party_id = p.project_lead_id
			and u.user_id = p.project_lead_id
			and tp.participation_id = :participation_id"

	set task_type_pretty [im_category_from_id $task_type_id]
	set rate_uom_pretty [im_category_from_id $uom_id]
	set source_language [im_category_from_id $source_language_id]

	# Find out if any of the tasks are already assigned for the task_type
	# If yes, the user can't accept this anymore.
	
	set accepted_p [db_string task_accepted "select 1 from im_trans_tasks where ${task_type}_id is not null and task_id in ([template::util::tcl_to_sql_list $task_ids]) limit 1" -default 0]

	# Get the required skills
	db_foreach required_project_skill {
		select skill_type_id, skill_id, aux_string2 from im_object_freelance_skill_map, im_categories where object_id = :project_id and skill_type_id not in (2000,2002) and skill_type_id = category_id
	} {
		if {$aux_string2 ne ""} {
			set $aux_string2 $skill_id
			lappend single_vars $aux_string2
		} else {
			set skill_$skill_type_id $skill_id
			lappend single_vars skill_$skill_type_id
		}
	}

	set num_tasks [llength $task_ids]
	
	set obj_ctr 0
	set data_result ""

	
	# Typically we would only have one line here, but who knows
	# therefore we provide mulitple lines just in case.
	set data_vars [list participation_id price_per_unit rate_uom_pretty total_amount source_language project_name project_nr deadline briefing subject_area project_lead_name project_lead_id num_tasks accepted_p email work_phone task_units target_language_id task_uom_pretty]
	
	db_foreach task_info "select sum(task_units) as task_units, target_language_id from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $task_ids]) group by target_language_id" {
		set komma ",\n"
		if {0 == $obj_ctr} { set komma "" }
		set dereferenced_result ""
		
		set task_uom_pretty [im_category_from_id $uom_id]
		set dereferenced_result ""
		foreach v $data_vars {
			eval "set a $$v"
			regsub -all {\n} $a {\n} a
			regsub -all {\r} $a {} a
			append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
		}
		append data_result "$komma{\"id\": \"$participation_id\", \"object_name\": \"[im_quotejson "$project_nr $task_type_pretty"]\"$dereferenced_result}"
		incr obj_ctr
		

	}
	
	set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_custom_trans_freelancer_participation_request: Data loaded..\""
	
	
#	foreach var $single_vars {
#		set value [set $var]
#		append result ",\n\"$var\": \"[im_quotejson $value]\""
#	}
	
	append result ",\n\"data\": \[\n$data_result\n\]\n}"
	im_rest_doc_return 200 "application/json" $result
	return
}


ad_proc -public im_rest_get_custom_trans_freelancer {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Handler for getting information on a single Translation freelancer

	Requires the freelancer_id

	Returns a JSON with
	- Freelancer ID and name
	- Source & Target languages
	- Skills (to be defined which ones)
	- Rates (source word & hours)

	See /sencha-freelance-translation/lib/freelance-trans-member.tcl for details
} {

	array set query_hash $query_hash_pairs
	if {[info exists query_hash(freelancer_id)]} {
		set freelancer_id $query_hash(freelancer_id)
	} else {
		# We can't work without a project!
		im_rest_error -format "json" -http_status 403 -message "Missing a freelancer_id"
	}

	# Anyone who can write on this project can query the freelancers for it
	im_project_permissions $rest_user_id $freelancer_id view_p read_p write_p admin_p
	if {!$view_p} {return}

	set valid_vars [list company_id office_id portrait_url]

	# Get the skill information for the skills to return
	set skill_type_sql ""

	db_foreach skill_types {select distinct skill_type_id
		from im_freelance_skills
	} {
		append skill_type_sql "\t\t(select '\[' || array_to_string(array_agg(skill_id), ',') || '\]' from im_freelance_skills where skill_type_id = $skill_type_id and user_id = :freelancer_id) as skill_$skill_type_id,\n"
		lappend valid_vars "skill_$skill_type_id"
	}

	db_1row company "
	select
		$skill_type_sql
		im_name_from_id(object_id_two) as object_name,
		c.company_id,
		c.main_office_id as office_id
	from
		im_companies c,
		acs_rels r
	where
		r.object_id_two = :freelancer_id
		and r.object_id_one = c.company_id
	order by c.company_name desc
	limit 1"
	
	set user_fs_url "/intranet/download/user/$freelancer_id"
	set portrait_file [im_portrait_user_file $freelancer_id]
	if {"" != $portrait_file} {
		set portrait_url "$user_fs_url/$portrait_file"
	} else {
		set portrait_url ""
	}
	
	set obj_ctr 0
	set result ""
	set komma ",\n"
	if {0 == $obj_ctr} { set komma "" }
	set dereferenced_result ""
	foreach v $valid_vars {
		eval "set a $$v"
		regsub -all {\n} $a {\n} a
		regsub -all {\r} $a {} a
		append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
	}

	append result "$komma{\"id\": \"$freelancer_id\", \"object_name\": \"[im_quotejson $object_name]\"$dereferenced_result}"
	incr obj_ctr

	switch $format {
		json {
			set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_rest_get_custom_sencha_project: Data loaded..\",\n\"data\": \[\n$result\n\]\n}"
			im_rest_doc_return 200 "application/json" $result
			return
		}
	}
	return
}


ad_proc -public im_rest_get_custom_rate_freelancer {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
	Handler for rating a freelancer
	Later this should be conerted to POST request

} {

    array set query_hash $query_hash_pairs
    
    if {[info exists query_hash(freelancer_id)]} {
        set freelancer_id $query_hash(freelancer_id)
    } 

    if {[info exists query_hash(project_id)]} {
        set project_id $query_hash(project_id)
    } 

    if {[info exists query_hash(quality_id)]} {
        set quality_id $query_hash(quality_id)
    } 
    
    set permission [im_user_is_employee_p $rest_user_id]

    if {$permission} {
        set count [db_string sql "select count(mapping_id) as count from im_freelancer_quality where project_id =:project_id and user_id = :freelancer_id"]
        if {$count > 0} {
            db_dml update_freelancer_rating "update im_freelancer_quality set quality_id =:quality_id where project_id =:project_id and user_id = :freelancer_id"
            set result_message "freelancer rating updated"
        } else {
    	    set result_message "freelancer inserted"
            db_dml insert_freelancer_rating "insert into im_freelancer_quality (project_id, user_id, quality_id) values (:project_id, :freelancer_id, :quality_id)"
        }
        set final_result true

    } else {
    	set final_result false
    	set result_message "You do not have permission"
    }
    
	set result "{\"success\": $final_result,\n\"message\": \"$result_message\"}"
	im_rest_doc_return 200 "application/json" $result
	return


}



ad_proc -public im_rest_get_custom_user_portrait_url {
	{ -format "json" }
	{ -rest_user_id 0 }
	{ -rest_otype "" }
	{ -rest_oid "" }
	{ -query_hash_pairs {} }
	{ -debug 0 }
} {
    Return the user portrait url
} {
	array set query_hash $query_hash_pairs
	if {[info exists query_hash(freelancer_id)]} {
		set freelancer_id $query_hash(freelancer_id)
	} else {
		# We can't work without a project!
		im_rest_error -format "json" -http_status 403 -message "Missing a freelancer_id  to start with"
	}
    set user_fs_url "/intranet/download/user/$freelancer_id"
    set portrait_file [im_portrait_user_file $freelancer_id]
    set portrait_url "${user_fs_url}/$portrait_file"
    set result "{\"success\": true,\n\"total\": 1,\n\"message\": \"user_portrait_url: Data loaded\",\n\"data\": \[ \n \{ \"url\": \"[im_quotejson $portrait_url]\" \} \n\]\n}"
	im_rest_doc_return 200 "application/json" $result
	return
}
