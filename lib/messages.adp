<property name="title">@page_title;noquote@</property>
<property name="context">@context;noquote@</property>

<listtemplate name="messages"></listtemplate>
<br />
<if @mail_url@ ne "">
<a href="@mail_url;noquote@" class="button">#intranet-mail.Send_Mail#</a>
</if>