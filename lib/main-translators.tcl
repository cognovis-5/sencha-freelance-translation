if {[exists_and_not_null customer_id]} {
	set maintain_url "[export_vars -base "/sencha-freelance-translation/main-translators" -url {customer_id}]"
	set where_clause "where customer_id = :customer_id"
} elseif {[exists_and_not_null freelancer_id]} {
	set maintain_url "[export_vars -base "/sencha-freelance-translation/main-translators" -url {freelancer_id}]"
	set where_clause "where freelancer_id = :freelancer_id"
}

template::list::create \
-name "main_translators" \
-multirow multirow \
-key mapping_id \
-actions [list	"Maintain" $maintain_url] \
-elements {
	company_name {
		label "Company"
		display_template {
			<a href='@multirow.company_url;noquote@'>@multirow.company_name;noquote@</a>
		}
	}
	freelancer_name {
		label "Freelancer"
		display_template {
			<a href='@multirow.freelancer_url;noquote@'>@multirow.freelancer_name;noquote@</a>
		}
	}
	source_language {
		label "Source"
	}
	target_language {
		label "Target"
	}
}

db_multirow -extend {company_url freelancer_url} multirow multirow "
	select mapping_id, im_name_from_id(customer_id) as company_name, customer_id, freelancer_id,im_name_from_id(freelancer_id) as freelancer_name, im_name_from_id(source_language_id) as source_language, im_name_from_id(target_language_id) as target_language from im_trans_main_freelancer $where_clause order by freelancer_name, company_name" {
		set company_url [export_vars -base "/intranet/companies/view" -url {{company_id $customer_id}}]
		set freelancer_url [export_vars -base "/intranet/users/view" -url {{user_id $freelancer_id}}]
	}

	