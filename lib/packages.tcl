if {$display_only_p ne 1} {
    # Apparently we want to select freelancers, so hide this
    set list_html ""
} else {
    if {[apm_package_installed_p "intranet-trans-trados"] && [im_trans_trados_project_p -project_id $project_id]} {
        set trados_p 1
    } else {
        set trados_p 0
    }
    
    # ---------------------------------------------------------------
    # Redirect if no tasks are created
    # ---------------------------------------------------------------
    set task_exists_p [db_string tasks "select 1 from im_trans_tasks where project_id = :project_id limit 1" -default 0]
    
    if {!$task_exists_p} {
        ad_returnredirect [export_vars -base "/intranet-translation/trans-tasks/task-list" -url {project_id}]
    }
    
    set bgcolor(0) "class=roweven"
    set bgcolor(1) "class=rowodd"
    
    # ---------------------------------------------------------------
    # Get defaults
    # ---------------------------------------------------------------
    
    db_1row project_info "select to_char(start_date,'YYYY-MM-DD HH24:MI') as project_start_date, project_name, project_nr, project_lead_id, project_status_id,company_id, subject_area_id,source_language_id from im_projects where project_id = :project_id"
    
    set project_dir [im_filestorage_project_path $project_id]
    
    if {[lsearch [im_sub_categories [im_project_status_open]] $project_status_id]<0} {
        set open_p 0
    } else {
        set open_p 1
    }
    
    set ctr 0
    set return_url [ad_return_url]
    # ---------------------------------------------------------------
    # Prepare task data in a local array
    # ---------------------------------------------------------------
    set task_ids [list]
    set task_type_ids [list]
    array set task_lang_arr [list]
    set target_language_ids [list]
    
    db_foreach task_names {
        select t.*, c.aux_string1
        from im_trans_tasks t, im_categories c
        where t.project_id = :project_id
        and c.category_id = t.task_type_id
    } {
        lappend task_ids $task_id
        lappend task_lang_arr($target_language_id) $task_id
        if {[lsearch $target_language_ids $target_language_id]<0} {lappend target_language_ids $target_language_id}
        
        if {$task_units eq ""} {set task_units 0}
        set task_name_arr($task_id) "$task_name ($task_units [im_category_from_id $task_uom_id])"
        set task_type_arr($task_id) ""
    
        # Add the task type ids
        foreach task_type $aux_string1 {
            set task_type_id [db_string type_id "select category_id from im_categories where lower(category) = :task_type and category_type = 'Intranet Trans Task Type'" -default ""]
            if {$task_type_id ne ""} {
                lappend task_type_arr($task_id) $task_type_id
                if {[lsearch $task_type_ids $task_type_id]<0} {lappend task_type_ids $task_type_id}
            }
            
            # Get an array for each task_type so we can find the tasks for this type
            if {[info exists ${task_type_id}_tasks($target_language_id)]} {
                lappend ${task_type_id}_tasks($target_language_id) $task_id
            } else {
                set ${task_type_id}_tasks($target_language_id) $task_id
            }
        }
    }
    
    # ---------------------------------------------------------------
    # Prepare the list html header
    # ---------------------------------------------------------------
    
    set list_html {
        <link href="/intranet-jquery/jquery_datetimepicker/jquery.datetimepicker.css" media="screen" rel="stylesheet" type="text/css">
        <script src="/intranet-jquery/jquery_datetimepicker/jquery.js" type="text/javascript"></script>
        <script src="/intranet-jquery/jquery_datetimepicker/build/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
    }
    append list_html "<table width=100%>"
    append list_html "<tr>"
    append list_html "<td class=rowtitle align=center>[_ intranet-translation.Target_Language]</td>"
    foreach task_type_id $task_type_ids {
        set task_type_name [im_category_from_id $task_type_id]
        append list_html "<td class=rowtitle align=center colspan=2>${task_type_name}</td>"
    }
    
    
    # Append Finalising
    append list_html "<td class=rowtitle align=center>[_ sencha-freelance-translation.Finalize]</td>"
    append list_html "</tr><tr>"
    
    append list_html "<td></td>"
    foreach task_type_id $task_type_ids {
        append list_html "<th align=center>[_ sencha-freelance-translation.Package_name]</th>"
        append list_html "<th align=center>[_ sencha-freelance-translation.Files]</th>"
    }
    append list_html "<td></td>"
    append list_html "</tr>"
    
    
    # ---------------------------------------------------------------
    # Prepare the packages
    # ---------------------------------------------------------------
    foreach target_language_id $target_language_ids {
        set num_lines($target_language_id) 0
        foreach task_type_id $task_type_ids {
            set num_type_lines 0
            set ${task_type_id}_lines($target_language_id) [list]
	    if {[info exists ${task_type_id}_tasks($target_language_id)]} {
		set task_ids [set ${task_type_id}_tasks($target_language_id)]
		
		set task_type [im_category_from_id -translate_p 0 $task_type_id]
		# Keep track of tasks in packages
		set packaged_task_ids [list]
    
		# ---------------------------------------------------------------
		#  Find the packages for this type and language
		# ---------------------------------------------------------------
    
		db_foreach package "select distinct sum(task_units) as total_units, task_uom_id, fp.freelance_package_id, fp.freelance_package_name
                from im_freelance_packages fp, im_freelance_packages_trans_tasks fptt, im_trans_tasks tt
                where fp.freelance_package_id = fptt.freelance_package_id
                    and fptt.trans_task_id in ([template::util::tcl_to_sql_list $task_ids])
                    and fptt.trans_task_id = tt.task_id
                    and fp.package_type_id = :task_type_id
                    group by task_uom_id, fp.freelance_package_id, fp.freelance_package_name" {
    
			set package_task_ids [db_list tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]
    
			set freelance_package_name_html "$freelance_package_name <br />($total_units [im_category_from_id $task_uom_id])"
                
    
			# ---------------------------------------------------------------
			# Check if we have a package file
			# ---------------------------------------------------------------
			
			set file_present_p 0
			
			set out_file_types [im_sub_categories 600]
			# Check if we already have a file for this package
			db_foreach freelance_package_file "select freelance_package_file_id, freelance_package_file_name, file_path, package_file_type_id from im_freelance_package_files where freelance_package_id = :freelance_package_id and package_file_type_id in ([template::util::tcl_to_sql_list $out_file_types])" {
			    set file_present_p 1
			}
			
			
			# This routine should go into a CALLBACK
			# so we can differentiate between customers what should happen
			if {!$file_present_p} {
			    
			    # ---------------------------------------------------------------
			    # Auto Create Tasks if we have trados packages
			    # ---------------------------------------------------------------
			    if {$trados_p} {
				set package_file_type_id 606
				
				# Check if we have the package
				foreach package_file [im_trans_trados_packages -project_id $project_id -target_language_id $target_language_id -task_type $task_type] {
                            
				    set file_path [lindex $package_file 0]
				    set file_task_ids [lindex $package_file 1]
				    # Check if it contains all the tasks for the package
				    set file_match_p 1
				    foreach task_id $task_ids {
					if {[lsearch $file_task_ids $task_id]<0} {
					    set file_match_p 0
					    break
					}
				    }
				    
				    if {$file_match_p} {
					set extension [file extension $file_path]
					set folder [im_trans_task_folder -project_id $project_id -target_language [im_category_from_id -translate_p 0 $target_language_id] -folder_type $task_type]
					set freelance_package_file_name [file tail $file_path]
					set new_path "${project_dir}/${folder}/${freelance_package_name}$extension"
					file rename -force $file_path $new_path
					set freelance_package_file_id [im_freelance_package_file_create -freelance_package_id $freelance_package_id \
									   -file_path $new_path \
									   -freelance_package_file_name $freelance_package_file_name \
									   -package_file_type_id $package_file_type_id]
					
					set file_present_p 1
				    }
				}
			    } else {
				# expect a zip file
				set package_file_type_id 608
			    }
			}
			# Provide links to upload package or replace it if it is present
			if {!$file_present_p} {
			    set upload_url [export_vars -base "/sencha-freelance-translation/package-upload" -url {freelance_package_id package_file_type_id return_url {return_file_p 0}}]
			    append freelance_package_name_html "<br /> <a href='$upload_url'>Upload Package</A>"
			    set freelance_package_name_html "<div align=center style='font-weight:bold;'>$freelance_package_name_html</div>"
			} else {
			    set upload_url [export_vars -base "/sencha-freelance-translation/package-upload" -url {freelance_package_id package_file_type_id return_url {return_file_p 0}}]
			    append freelance_package_name_html "<br /><a href='$upload_url'>[lang::message::lookup "" sencha-freelance-translation.Replace_Package "Replace Package"]</A>"
			    append freelance_package_name_html " &nbsp; <img src=/intranet/images/zip-download.gif width=16 height=16 title='$freelance_package_file_name' alt='$freelance_package_file_name'>"
			    set freelance_package_name_html "<div align=center>$freelance_package_name_html</div>"
			    
			}
			
			# ---------------------------------------------------------------
			# Assignee Info if present
			# ---------------------------------------------------------------
			set assignment_status_id ""
			db_0or1row assignment_info "select im_name_from_id(assignee_id) as assignee, assignment_status_id,assignee_id, end_date
                    from im_freelance_assignments
                    where freelance_package_id = :freelance_package_id
                    and assignment_status_id in (4222,4224,4225,4226,4229) limit 1
                    "
			
			
			if {$assignment_status_id eq ""} {
			    set select_url [export_vars -base "/sencha-freelance-translation/freelancers-select" -url {target_language_id project_id}]
			    set package_html "<div align=center style='font-weight:bold;'>[lang::message::lookup "" sencha-freelance-translation.Missing_Assignee "No Assignee for this package."] <br /><a href='$select_url'>[lang::message::lookup "" sencha-freelance-translation.Select_Freelancer "Select freelancer"]</a>"
			} else {
			    set assignments_url [export_vars -base "/sencha-freelance-translation/project-assignments" -url {project_id assignee_id}]
			    
			    set package_html "<div align=center><a href='$assignments_url'>$assignee</a> ([im_category_from_id $assignment_status_id])<br />[lc_time_fmt $end_date "%x %X" [lang::user::locale]]</div><br />"
			}
			
			lappend ${task_type_id}_lines($target_language_id) [list $freelance_package_name_html $package_html]
			incr num_type_lines
			set packaged_task_ids [concat $packaged_task_ids $package_task_ids]
		    }
		
	    }

	    # ---------------------------------------------------------------
	    #  Now deal with no package existing
	    # ---------------------------------------------------------------
		
            set packaged_task_ids [lsort -unique $packaged_task_ids]
            
            # Add one line for adding additional packages
            if {[llength $task_ids] ne [llength $packaged_task_ids]} {
                set freelance_package_name "${project_name}_[im_category_from_id -translate_p 0 $target_language_id]_[im_category_from_id -translate_p 0 $task_type_id]"
                set existing_count [db_string count "select count(freelance_package_name) from im_freelance_packages where freelance_package_name like '${freelance_package_name}%'" -default 0]
                if {$existing_count >0} {
                    # In case we already have the name, append an _1 or higher behind it
                    set freelance_package_name "${freelance_package_name}_$existing_count"
                }
    
                set package_name_html "<input type=text id=\"package_name_${target_language_id}_${task_type_id}\" name=\"${target_language_id}_${task_type_id}_package_name\" value=\"$freelance_package_name\">"
                set tasks_html ""
                foreach task_id [lsort $task_ids] {
                    if {[lsearch $packaged_task_ids $task_id]<0} {
                        append tasks_html "<input type=checkbox name='${target_language_id}_${task_type_id}_bulk_file_ids' id='bulk_file_ids,$task_id' value='$task_id' checked>$task_name_arr(${task_id})<br />"
                    }
                }
                            
                lappend ${task_type_id}_lines($target_language_id) [list $package_name_html $tasks_html]
                incr num_type_lines
            }
            if {$num_type_lines > $num_lines($target_language_id)} {
                set num_lines($target_language_id) $num_type_lines
            }
            
        }
    }
    
    # ---------------------------------------------------------------
    # Build the HTML TAble
    # ---------------------------------------------------------------
    
    set ctr 0
    foreach target_language_id $target_language_ids {
        # Loop through each line
        set line_ctr 0
        set show_finalized_p 1
        for {set i 0} {$i < $num_lines($target_language_id)} {incr i} {
            set line_html "<tr $bgcolor([expr $ctr % 2])>"
            if {$line_ctr eq 0} {
                append line_html "<td $bgcolor([expr $ctr % 2])>[im_category_from_id $target_language_id]</td>"
            } else {
                append line_html "<td $bgcolor([expr $ctr % 2])>&nbsp;</td>"
            }
    
            foreach task_type_id $task_type_ids {
                # Find if we have something to add for this line
                set package_line [lindex [set ${task_type_id}_lines($target_language_id)] $line_ctr]
                if {$package_line ne ""} {
                    append line_html "<td $bgcolor([expr $ctr % 2]) valign='top'><nobr>[lindex $package_line 0]<nobr></td>"
                    if {[lindex $package_line 1] ne ""} {
                        append line_html "<td align=center $bgcolor([expr $ctr % 2])>[lindex $package_line 1]</td>"
                    } else {
                        append line_html "<td $bgcolor([expr $ctr % 2])>&nbsp;</td>"
                    }
                } else {
                    append line_html "<td $bgcolor([expr $ctr % 2])>&nbsp;</td><td $bgcolor([expr $ctr % 2])>&nbsp;</td>"
                }
            }
            
            # ---------------------------------------------------------------
            # Finalizing
            # ---------------------------------------------------------------
    	if {$show_finalized_p} {
    	    set show_finalized_p 0
            set task_nr "finalize_${project_nr}_$target_language_id"
            set task_id [db_string task_exists "select project_id from im_projects where project_nr = :task_nr and parent_id = :project_id limit 1" -default ""]
            if {$task_id eq ""} {
                set finalizing_hours 0
                set finalizer_id $project_lead_id
                set deadline [db_string deadline "select max(end_date) - interval '1 hour' from im_trans_tasks where project_id = :project_id and target_language_id = :target_language_id" -default ""]
                if {$deadline eq ""} {
                    set deadline [db_string deadline_from_project "select end_date - interval '1 hour' from im_projects where project_id = :project_id" -default ""]
                }
            } else {
                db_1row task_info "select task_assignee_id as finalizer_id, planned_units as finalizing_hours, deadline_date as deadline from im_timesheet_tasks where task_id = :task_id"
            }
            # Show Empty line if not finding a task
            append line_html "<td $bgcolor([expr $ctr % 2])><table>"
            append line_html "<tr><td $bgcolor([expr $ctr % 2])>[_ sencha-freelance-translation.Finalizer]</td><td $bgcolor([expr $ctr % 2])>[im_employee_select_multiple ${target_language_id}_finalizer_id $finalizer_id 1 0]</td></tr>"
            append line_html "<tr><td $bgcolor([expr $ctr % 2])>[_ intranet-timesheet2.Hours_1]</td><td $bgcolor([expr $ctr % 2])><input size=6 type=text name=\"${target_language_id}_finalizing_hours\" value=\"$finalizing_hours\"></td></tr>"
            append line_html "<tr><td $bgcolor([expr $ctr % 2])>[_ sencha-freelance-translation.Deadline]</td><td $bgcolor([expr $ctr % 2])><input type=text size=16 id=\"deadline_${target_language_id}\" name=\"${target_language_id}_deadline\" value=\"$deadline\">
                        <script>
            \$(document).ready(function() {
                \$('#deadline_${target_language_id}').datetimepicker({
                  format:'Y-m-d H:i'
                });
            });
                        </script></td></tr>"
            append line_html "</tr></table></td>"
    	} else {
    	    append line_html "<td $bgcolor([expr $ctr % 2])>&nbsp;</td"
    	}
            append list_html "$line_html</tr>"
            incr ctr
            incr line_ctr
        }
    }
    
    
    append list_html "</table>"
    
    set left_navbar_html ""
    
    # Setup the subnavbar
    set bind_vars [ns_set create]
    ns_set put $bind_vars project_id $project_id
    set parent_menu_id [im_menu_id_from_label "project"]
    set menu_label "project_assignments"
    
    set sub_navbar [im_sub_navbar \
        -components \
        -base_url "/intranet/projects/view?project_id=$project_id" \
        $parent_menu_id \
        $bind_vars "" "pagedesriptionbar" "freelance_assignments"]
}
