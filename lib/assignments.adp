<if @display_p@ eq 1>
	<form action=assignments-2>
	<input type="hidden" name="project_id" value="@project_id@">
	<list name="task_type_ids">
		<input type="hidden" name="task_type_ids" value="@task_type_ids:item@">
	</list>
	@list_html;noquote@
	
	<if @display_only_p@ eq 0>
		<button name=action value=save type=submit>#sencha-freelance-translation.Prepare_Assignments#</button>
		<if @open_p@ eq 1>
			<button name=action value=request type=submit>#sencha-freelance-translation.lt_Request_Participation#</button>
			<button name=action value=assign type=submit>#sencha-freelance-translation.Assign_and_create_PO#</button>
		</if>
		<button name=action value=back type=submit>#sencha-freelance-translation.Back_to_Project#</button>
	</if>
	</form>
</if>
<else>#sencha-freelance-translation.lt_No_freelancer_selecte# <a href='freelancers-select?project_id=@project_id@'>#sencha-freelance-translation.Select_freelancers#</a></else>
