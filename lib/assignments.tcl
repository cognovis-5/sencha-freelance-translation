if {[apm_package_installed_p "intranet-trans-trados"] && [im_trans_trados_project_p -project_id $project_id]} {
    set trados_p 1
} else {
    set trados_p 0
}

# ---------------------------------------------------------------
# Redirect if no tasks are created
# ---------------------------------------------------------------
set task_exists_p [db_string tasks "select 1 from im_trans_tasks where project_id = :project_id limit 1" -default 0]

if {!$task_exists_p} {
    ad_returnredirect [export_vars -base "/intranet-translation/trans-tasks/task-list" -url {project_id}]
}

set bgcolor(0) "class=roweven"
set bgcolor(1) "class=rowodd"

set return_url [ad_return_url]
# ---------------------------------------------------------------
# Decide on the freelancers to display
# ---------------------------------------------------------------

set currently_assigned_freelancers [list]
# Append already assigned freelancers
if {[llength $freelancer_ids]>0} {
    set freelancer_sql "and assignee_id not in ([template::util::tcl_to_sql_list $freelancer_ids])"
    db_foreach assignee "select distinct assignee_id from im_freelance_assignments fa, im_freelance_packages fp where project_id = :project_id and fp.freelance_package_id = fa.freelance_package_id $freelancer_sql" {
        lappend currently_assigned_freelancers $assignee_id
    }
} else {
    db_foreach assignee "select distinct assignee_id from im_freelance_assignments fa, im_freelance_packages fp where project_id = :project_id and fp.freelance_package_id = fa.freelance_package_id" {
        lappend freelancer_ids $assignee_id
        lappend currently_assigned_freelancers $assignee_id
    }
}

set display_p 1
if {[llength $freelancer_ids] <1} {
    set display_p 0
} else {

# ---------------------------------------------------------------
# Get basic information for multiple languages
# ---------------------------------------------------------------
set target_language_ids [list]

db_foreach language {
    select  language_id as target_language_id, parent_id
        from	im_target_languages left outer join im_category_hierarchy ch on (child_id = language_id)
        where	project_id = :project_id
} {
    if {[exists_and_not_null parent_id]} {
        set target_ids [im_sub_categories $parent_id]
    } else {
        set target_ids [im_sub_categories $target_language_id]
    }

    set target_lang_freelancers($target_language_id) [db_list freelancers "select user_id from im_freelance_skills where skill_id in ([template::util::tcl_to_sql_list $target_ids]) and skill_type_id=2002 and user_id in ([template::util::tcl_to_sql_list $freelancer_ids])"]
    
    lappend target_language_ids $target_language_id
}


# Turn it upside down to the get the production language

foreach target_language_id $target_language_ids {
    foreach freelancer_id $freelancer_ids {
        if {[lsearch $target_lang_freelancers($target_language_id) $freelancer_id]>-1} {
            lappend freelancer_target_lang_id($freelancer_id) $target_language_id
        }
    }
}


# ---------------------------------------------------------------
# Find out who is a better translator for automated selection
# ---------------------------------------------------------------

set trans_freelancer_ids [list]
set proof_freelancer_ids [list]
set company_id [db_string company "select coalesce(final_company_id,company_id) as company_id from im_projects where project_id = :project_id" -default ""]
foreach freelancer_id $freelancer_ids {
    set main_task_type [im_freelance_trans_main_task_type -freelancer_id $freelancer_id -company_id $company_id]
    switch $main_task_type {
        trans {
            lappend trans_freelancer_ids $freelancer_id
        }
        proof {
            lappend proof_freelancer_ids $freelancer_id
        }
        default {
        }
    }
}



# Check if we have assignments for this project
set assignments_p [db_string assignments "select 1 from im_freelance_assignments fa, im_freelance_packages fp where project_id = :project_id and fa.freelance_package_id = fp.freelance_package_id limit 1" -default 0]
if {$assignments_p eq 0} {
    migrate_task_assignments -project_id $project_id
}


# ---------------------------------------------------------------
# Basic project info
# ---------------------------------------------------------------
db_1row project_info "select to_char(start_date,'YYYY-MM-DD HH24:MI') as project_start_date, project_status_id,company_id, subject_area_id,source_language_id from im_projects where project_id = :project_id"

set project_dir [im_filestorage_project_path $project_id]

if {[lsearch [im_sub_categories [im_project_status_open]] $project_status_id]<0} {
    set open_p 0
} else {
    set open_p 1
}

# ---------------------------------------------------------------
# Prepare task data in a local array
# ---------------------------------------------------------------
array set file_exists [list]
array set task_name_arr [list]
set task_ids [list]
set task_type_ids [list]
array set task_lang_arr [list]
set target_language_ids [list]
array set assigned_tasks [list]
array set task_uom_arr [list]
# Check if we have more task uoms in this project.
# We need to be careful that we don't mix assignments
set task_uom_ids [list]

db_foreach task_names {
    select t.*, c.aux_string1
    from im_trans_tasks t, im_categories c
    where t.project_id = :project_id
    and c.category_id = t.task_type_id
} {
    lappend task_ids $task_id
    lappend task_lang_arr($target_language_id) $task_id
    lappend target_language_ids $target_language_id
    set pdf_file [im_trans_task_source_file_pdf -task_id $task_id]
    set file_exists($task_id) [file exists $pdf_file]
    set task_name_arr($task_id) "$task_name"
    set task_type_arr($task_id) ""
    
    if {[lsearch $task_uom_ids $task_uom_id]<0} {lappend task_uom_ids $task_uom_id}
    if {$task_units eq ""} {set task_units 0}
    set task_units_arr($task_id) $task_units
    set task_uom_arr($task_id) "($task_units [im_category_from_id $task_uom_id])"
    
    # Add the task type ids
    foreach task_type $aux_string1 {
        set task_type_id [db_string type_id "select category_id from im_categories where lower(category) = :task_type and category_type = 'Intranet Trans Task Type'" -default ""]
        if {$task_type_id ne ""} {
            lappend task_type_arr($task_id) $task_type_id
            if {[lsearch $task_type_ids $task_type_id]<0} {lappend task_type_ids $task_type_id}
            
            if {[set ${task_type}_id] ne ""} {
                set assigned_freelancer_id  [set ${task_type}_id]
                # Who is assigned to a task (as we can have multiple for this task
                set ${task_type}_assigned_arr($task_id) $assigned_freelancer_id
                
                # What tasks is the freelancer already assigned, so he can't be assigned for it anywhere else
                if {[info exists assigned_tasks($assigned_freelancer_id)]} {
                    lappend assigned_tasks($assigned_freelancer_id) $task_id
                } else {
                    set assigned_tasks($assigned_freelancer_id) $task_id
                }
            }
        }
    }
}

# Show the task_uom_ids
if {[llength $task_uom_ids] >1} {
    foreach task_id $task_ids {
        append task_name_arr($task_id) " $task_uom_arr($task_id)"
    }
}

foreach task_type_id $task_type_ids {
    set rate_task_type_arr($task_type_id) [db_string rate "select aux_int2 from im_categories where category_id = :task_type_id"]
}

# ---------------------------------------------------------------
# Prepare the list html header
# ---------------------------------------------------------------

set list_html {
    <link href="/intranet-jquery/jquery_datetimepicker/jquery.datetimepicker.css" media="screen" rel="stylesheet" type="text/css">
    <script src="/intranet-jquery/jquery_datetimepicker/jquery.js" type="text/javascript"></script>
    <script src="/intranet-jquery/jquery_datetimepicker/build/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
}
append list_html "<table width=100%>"
append list_html "<tr>"
append list_html "<td class=rowtitle align=center>[_ sencha-freelance-translation.Freelancer]</th>"
append list_html "<td class=rowtitle align=center>[_ intranet-translation.Target_Language]</th>"
foreach task_type_id $task_type_ids {
    set task_type_name [im_category_from_id $task_type_id]
    append list_html "<td class=rowtitle align=center colspan=3>${task_type_name}</th>"
}
append list_html "</tr><tr>"

append list_html "<td colspan=2></td>"
foreach task_type_id $task_type_ids {
    append list_html "<td align=center>[_ sencha-freelance-translation.Deadline]</td>"
    append list_html "<td align=center>[_ sencha-freelance-translation.Price_per_Source_Word]</td>"
    append list_html "<td align=center>[_ sencha-freelance-translation.Files]</td>"
}
append list_html "<td></td>"
append list_html "</tr>"

set sql "
    select
        user_id,
        im_name_from_user_id(user_id) as user_name
    from cc_users
    where user_id in ([template::util::tcl_to_sql_list $freelancer_ids])
"


# ---------------------------------------------------------------
# Loop through the freelancers
# ---------------------------------------------------------------

set ctr 0
foreach freelancer_id $freelancer_ids {
    incr ctr
    # Ignore the rates for employees
    set employee_p [im_user_is_employee_p $freelancer_id]

    # Necessary defaults
    if {![info exists assigned_tasks($freelancer_id)]} {
        set assigned_tasks($freelancer_id) [list]
    }

    foreach type [db_list trans_types "select category from im_categories where category_type = 'Intranet Trans Task Type'"] {
	set ${type}_days 0
    }
    

    # finds company id (=provider_id) for each of the freelancers
    if {[im_user_is_employee_p $freelancer_id]} {
        set provider_id [im_company_internal]
    } else {
        db_1row freelancer_info "
            select
                c.company_id as provider_id,
                im_name_from_user_id(object_id_two) as user_name
            from            acs_rels r,
                    im_companies c
            where       r.object_id_one = c.company_id
            and     r.object_id_two =:freelancer_id
            and rel_type = 'im_company_employee_rel' limit 1"
    
    }
    
    # A freelancer can only produce in one target language in a
    # project
    if {![info exists freelancer_target_lang_id($freelancer_id)]} {
	   break
    } else {
        foreach target_lang_id $freelancer_target_lang_id($freelancer_id) {
	    if {[lsearch $target_language_ids $target_lang_id]>-1} {
		set target_language_id $target_lang_id
	    }
	}
    }
    set num_lines($freelancer_id) 0

    # Append the rows for each task type
    foreach task_type_id $task_type_ids {
        set ${task_type_id}_lines($freelancer_id) [list]
        set rate ""
        set uom_id ""
        set task_type [im_category_from_id -translate_p 0 $task_type_id]
        set user_task_ids ""
        set deadline ""
        
        set freelancer_task_html ""
        
        # ---------------------------------------------------------------
        # Check if the freelancer would be selected
        # ---------------------------------------------------------------
        set checked_p 0
        # Check with the default freelancers
        switch $task_type_id {
            4210 {
                if {[lsearch $trans_freelancer_ids $freelancer_id]>-1} {
                    set checked_p 1
                }
            }
            4211 {
                if {[lsearch $proof_freelancer_ids $freelancer_id]>-1} {
                    set checked_p 1
                }
            }
        }
        
        if {$checked_p eq 1} {set checked_html "checked"} else {set checked_html ""}
        






        # ---------------------------------------------------------------
        # Display the package assignments
        # which the freelancer is already assigned to
        # ---------------------------------------------------------------

        set task_sum_units 0
        set package_task_ids [list]
	set exclude_status_ids [list 4223 4228 4230 4231]
	
        db_foreach package_assigned "select fp.freelance_package_id, freelance_package_name, fa.assignment_status_id,
                end_date, assignment_id, rate as assignment_rate, uom_id,
                coalesce(assignment_units,0) as assignment_units, purchase_order_id
            from im_freelance_assignments fa, im_freelance_packages fp
            where assignee_id = :freelancer_id
            and assignment_type_id = :task_type_id
            and fa.freelance_package_id = fp.freelance_package_id
            and project_id = :project_id
            and fa.assignment_status_id not in ([template::util::tcl_to_sql_list $exclude_status_ids])
        " {
            incr num_rows(task_type_id)
            set one_line [list]
            
            # Not sure if this is the best way, but hide any tasks not assignable
            set show_line_p 1
            # Check if the freelancer is already assigned

            # Allow for editing while assignment is only saved/created
            if {$assignment_status_id eq 4220 && $open_p && !$display_only_p} {
                lappend one_line "<input type=text id=\"deadline_${freelancer_id}_${task_type_id}\" name=\"${freelancer_id}_${task_type_id}_deadline\" value=\"$end_date\">
                            <script>
                \$(document).ready(function() {
                    \$('#deadline_${freelancer_id}_${task_type_id}').datetimepicker({
                      format:'Y-m-d H:i'
                    });
                });
                            </script>"
                if {$employee_p} {
                    lappend one_line "&nbsp;"
                } else {
                    lappend one_line "<table>
                        <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.Rate Rate]</td><td $bgcolor([expr $ctr % 2])><input size=6 type=text name=\"${freelancer_id}_${task_type_id}_rate\" value=\"$assignment_rate\"></td></tr>
                        <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.Units Units]</td><td $bgcolor([expr $ctr % 2])><input size=6 type=text name=\"${freelancer_id}_${task_type_id}_units\" value=\"$assignment_units\"></td></tr>
                        <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.UOM UOM]</td><td $bgcolor([expr $ctr % 2])>[im_category_select "Intranet UoM" "${freelancer_id}_${task_type_id}_uom_id" $uom_id]</td></tr>
                        </table>"
		}
                set package_html "<input type=checkbox name='${freelancer_id}_${task_type_id}_bulk_freelance_package_ids' id='bulk_freelance_packackage_ids,$freelance_package_id' value='$freelance_package_id' $checked_html>$freelance_package_name<ul>"

            } else {
		set assignment_change_url [export_vars -base "/sencha-freelance-translation/assignment-change" -url {assignment_id return_url}]
                lappend one_line "<a href='$assignment_change_url'>[lc_time_fmt $end_date "%x %X"]</a>"
		
		#                lappend one_line "[lc_time_fmt $end_date "%x %X"]"
                if {$employee_p} {
                    if {$assignment_status_id eq 4222 || $assignment_status_id eq 4220} {
                        set delete_assignment_url [export_vars -base "assignment-delete" -url {assignment_id project_id return_url}]
                        lappend one_line "<a href='$delete_assignment_url'>[lang::message::lookup "" sencha-freelance-translation.Delete_Assignment "Delete Assignment"]</a>"
                    } else {
                        lappend one_line "&nbsp;"
                    }
                } else {
                    
                    # Check if we have a purchase order
                    set po_created_p [db_0or1row po_created "select c.amount,i.invoice_id,i.invoice_nr, sum(item_units) as item_units, price_per_unit, company_contact_id, c.currency from im_invoice_items ii, im_invoices i, im_costs c, im_trans_tasks t where i.invoice_id = ii.invoice_id and t.task_id in (select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id) and t.task_id = ii.task_id and company_contact_id = :freelancer_id and c.cost_id = i.invoice_id group by c.amount, i.invoice_id, invoice_nr, price_per_unit, company_contact_id, c.currency limit 1"]
                    if {$po_created_p} {
                        set invoice_url [export_vars -base "/intranet-invoices/view" -url {invoice_id return_url}]
                        set total_price $amount
                        lappend one_line "<table>
                            <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.PO PO]</td><td $bgcolor([expr $ctr % 2])><center><a href='$invoice_url'>$invoice_nr</a>:<br />$total_price $currency</center></td></tr>
                            <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.Rate Rate]</td><td $bgcolor([expr $ctr % 2])>$price_per_unit</td></tr>
                            <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.Units Units]</td><td $bgcolor([expr $ctr % 2])>$assignment_units</tr>
                            <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.UOM UOM]</td><td $bgcolor([expr $ctr % 2])>[im_category_from_id $uom_id]</td></tr>
                            </table>"
                        # Update assignment just in case
                        if {$purchase_order_id eq ""} {
                            db_dml update_assignment_po "update im_freelance_assignments set purchase_order_id = :invoice_id where assignment_id = :assignment_id"
                        }
                    } else {
                        
                        # Check if the user is assigned but missing a purchase order
                        if {$assignment_status_id eq 4222 || $assignment_status_id eq 4220} {
                            set create_po_url [export_vars -base "assignment-create-purchase-order" -url {assignment_id return_url}]
                            set delete_assignment_url [export_vars -base "assignment-delete" -url {assignment_id project_id return_url}]
                            set one_field "<table>
                            <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.Assignment Assignment]</td><td $bgcolor([expr $ctr % 2])><center><a href='$delete_assignment_url'>[lang::message::lookup "" sencha-freelance-translation.Delete_Assignment "Delete Assignment"]</a></td></tr>
                            <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.PO PO]</td><td $bgcolor([expr $ctr % 2])><center><a href='$create_po_url'>[lang::message::lookup "" sencha-freelance-translation.Create_PO "Create PO"]</a></td></tr>
                            "
                            
                        } else {
                            set one_field "<table>"
                        }
                        
                        append one_field "
                            <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.Rate Rate]</td><td $bgcolor([expr $ctr % 2])>$assignment_rate</td></tr>
                            <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.Units Units]</td><td $bgcolor([expr $ctr % 2])>$assignment_units</tr>
                            <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.UOM UOM]</td><td $bgcolor([expr $ctr % 2])>[im_category_from_id $uom_id]</td></tr>
                            </table>"
                        
                        lappend one_line $one_field
                    }
                }
                set package_html "$freelance_package_name <ul>"
            }

            set task_ids [db_list tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]
            foreach task_id $task_ids {
                if { [info exists ${task_type}_assigned_arr($task_id)]} {
                    # Someone is assigned already to this task
                    # Figure out who
                    if {[set ${task_type}_assigned_arr($task_id)] eq $freelancer_id} {
                        append package_html "<li><font color=green>$task_name_arr(${task_id})</font></li>"
                    }
                } else {
                        append package_html "<li><font color=blue>$task_name_arr(${task_id})</font></li>"
#                        set show_line_p 0
                }
                lappend package_task_ids $task_id
            }
            
            # ---------------------------------------------------------------
            # Check if the freelance package is already there
            # ---------------------------------------------------------------
            set assignment_line ""
 
            lappend one_line "$assignment_line<br />$package_html </ul>"
            set task_sum_units [expr $task_sum_units + $assignment_units]
            if {$show_line_p} {
                lappend ${task_type_id}_lines($freelancer_id) $one_line
            }
        }
        
        
        # ---------------------------------------------------------------
        # Task (only if applicable)
        # ---------------------------------------------------------------
            
        set tasks_html ""
        set new_tasks 0

        set assignable_task_ids [list]
	if {![info exists task_lang_arr($target_language_id)]} {
	    set task_lang_arr($target_language_id) [list]
	}

        foreach task_id $task_lang_arr($target_language_id) {
        
            if {[lsearch $package_task_ids $task_id]<0 && ![info exists ${task_type}_assigned_arr($task_id)]} {
                # The user isn't assigned himself and nobody else is so far.
               incr new_tasks
               lappend assignable_task_ids $task_id
            }
        }

        if {$new_tasks >0} {
            # Apparently there are tasks not yet assigned to the user, so offer to do so.
            
            # ---------------------------------------------------------------
            # Deadline Calculation
            # We might have to change this to actually look at the
            # Previous processing steps to find the new deadline
            # ---------------------------------------------------------------
	    
	    set previous_task_type_id [im_trans_task_previous_type -project_id $project_id -task_type_id $task_type_id]
            if {$open_p} {
		set task_type [db_string task_type "select lower(category) from im_categories where category_id = :task_type_id"]
		set days [im_translation_task_processing_time -task_type $task_type -task_ids $assignable_task_ids]
		if {$previous_task_type_id  ne ""} {
		    set previous_task_type [db_string task_type "select lower(category) from im_categories where category_id = :previous_task_type_id"]
		    set previous_deadline [db_string previous "select to_char(max(${previous_task_type}_end_date),'YYYY-MM-DD HH24:MI') from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $assignable_task_ids])" -default ""]
		    if {$previous_deadline eq ""} {
			# No previous task assigned, calculate from start_date
			set previous_deadline $project_start_date
			set previous_days  [im_translation_task_processing_time -task_type "$previous_task_type" -task_ids $assignable_task_ids]
			set deadline_days [expr $previous_days + $days]
		    } else {
			set deadline_days $days
		    }
		    set deadline "[im_translation_processing_deadline -start_timestamp $previous_deadline -days $deadline_days]"
		}
            } else {
                set deadline ""
            }
                    
            # ---------------------------------------------------------------
            # Check for packages for the tasks
            # You can only assign the package in this case
            # ---------------------------------------------------------------
            set package_task_ids [list]

            db_foreach package "select distinct fp.freelance_package_id, freelance_package_name from im_freelance_packages fp, im_freelance_packages_trans_tasks fpt where package_type_id = :task_type_id and fpt.freelance_package_id = fp.freelance_package_id and trans_task_id in ([template::util::tcl_to_sql_list $assignable_task_ids])" {
                
                incr num_rows(task_type_id)

                # ---------------------------------------------------------------
                # Prepare the package check
                # ---------------------------------------------------------------

                set task_sum_units 0
                set task_ids [db_list tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]

                # find out if the package contains tasks the user is already assigned to. then don't offer this package
                set ignore_package_p 0
                foreach task_id $task_ids {
                    if {[lsearch $assigned_tasks($freelancer_id) $task_id]>-1} {
                        set ignore_package_p 1
                    }
                }
            
                if {$ignore_package_p} {
                    continue
                }
                
                if {$display_only_p} {
                    set package_html "$freelance_package_name<ul>"
                } else {
                    set package_html "<input type=checkbox name='${freelancer_id}_${task_type_id}_bulk_freelance_package_ids' id='bulk_freelance_packackage_ids,$freelance_package_id' value='$freelance_package_id' $checked_html>$freelance_package_name<ul>"
                }

                foreach task_id $task_ids {
                    if { [info exists ${task_type}_assigned_arr($task_id)]} {
                        # Someone is assigned already to this task, ignore
                    } else {
                        set task_sum_units [expr $task_sum_units + $task_units_arr($task_id)]

#                        if { $file_exists($task_id) } {
                            append package_html "<li>$task_name_arr(${task_id})</li>"
#                       } else {
#                            append package_html "<li><font color=red>$task_name_arr(${task_id})</font></li>"
#                        }
                    }
                    lappend package_task_ids $task_id
                }

                set one_line [list]
                    
                # Deadline
                lappend one_line "<input type=text id=\"deadline_${freelancer_id}_${task_type_id}\" name=\"${freelancer_id}_${task_type_id}_deadline\" value=\"$deadline\">
                            <script>
                \$(document).ready(function() {
                    \$('#deadline_${freelancer_id}_${task_type_id}').datetimepicker({
                      format:'Y-m-d H:i'
                    });
                });
                            </script>"

                # ---------------------------------------------------------------
                # Freelancer Rate
                # ---------------------------------------------------------------

                if {$rate eq ""} {
                    set rate_list [im_translation_best_rate \
                        -provider_id $provider_id \
                        -task_type_id $rate_task_type_arr($task_type_id) \
                        -task_uom_id [im_uom_s_word] \
                        -source_language_id $source_language_id \
                        -target_language_id $target_language_id \
                        -subject_area_id $subject_area_id \
                        -task_sum $task_sum_units \
                        -currency "EUR"]
                    set uom_id [lindex $rate_list 1]
                    set rate [string trim [lindex $rate_list 2]]
                }
                
                
                # Rate
                if {$employee_p} {
                    lappend one_line "&nbsp;"
                } else {
                    lappend one_line "<table>
                        <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.Rate Rate]</td><td $bgcolor([expr $ctr % 2])><input size=6 type=text name=\"${freelancer_id}_${task_type_id}_rate\" value=\"$rate\"></td></tr>
                        <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.Units Units]</td><td $bgcolor([expr $ctr % 2])><input size=6 type=text name=\"${freelancer_id}_${task_type_id}_units\"></td></tr>
                        <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.UOM UOM]</td><td $bgcolor([expr $ctr % 2])>[im_category_select "Intranet UoM" "${freelancer_id}_${task_type_id}_uom_id" $uom_id]</td></tr>
                        </table>"
                }
                if {$display_only_p} {
                    set one_line [list]
                    lappend one_line "[lc_time_fmt $deadline "%x %X"]"
                    if {$employee_p} {
                        lappend one_line "&nbsp;"
                    } else {
                        lappend one_line "<table>
                            <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.Rate Rate]</td><td $bgcolor([expr $ctr % 2])>$rate</td></tr>
                            <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.Units Units]</td><td $bgcolor([expr $ctr % 2])></td></tr>
                            <tr><td $bgcolor([expr $ctr % 2])>[lang::message::lookup "" sencha-freelance-translation.UOM UOM]</td><td $bgcolor([expr $ctr % 2])>[im_category_from_id $uom_id]</td></tr>
                            </table>"
                    }
                } else {
                    lappend one_line "$package_html </ul>"
                }
                lappend ${task_type_id}_lines($freelancer_id) $one_line
            }



    
        # ---------------------------------------------------------------
        # Rows for all the tasks not in a package
        # ---------------------------------------------------------------


            set still_assigneable_task_ids [list]
            set task_sum_units 0
            set task_html ""
            
            # Ugly trick.. but we don't assign tasks if we have display only
            if {$display_only_p} {set assignable_task_ids [list]}
            foreach task_id $assignable_task_ids {
                if {[lsearch $package_task_ids $task_id]<0 && [lsearch $assigned_tasks($freelancer_id) $task_id]<0} {
                    # TAsk not included in the package
                    lappend still_assigneable_task_ids $task_id
                    set task_sum_units [expr $task_sum_units + $task_units_arr($task_id)]
                    append task_html "<input type=checkbox name='${freelancer_id}_${task_type_id}_bulk_file_ids' id='bulk_file_ids,$task_id' value='$task_id' $checked_html>$task_name_arr(${task_id})<br />"
                }
            }
            
            set one_line [list]
            if {[llength $still_assigneable_task_ids]>0} {
                if {$rate eq ""} {
                    set rate_list [im_translation_best_rate \
                        -provider_id $provider_id \
                        -task_type_id $rate_task_type_arr($task_type_id) \
                        -task_uom_id $task_uom_id \
                        -source_language_id $source_language_id \
                        -target_language_id $target_language_id \
                        -subject_area_id $subject_area_id \
                        -task_sum $task_sum_units \
                        -currency "EUR"]
                    set uom_id [lindex $rate_list 1]
                    set rate [string trim [lindex $rate_list 2]]
                }

                lappend one_line "<input type=text size=16 id=\"deadline_${freelancer_id}_$task_type_id\" name=\"${freelancer_id}_${task_type_id}_deadline\" value=\"$deadline\">
                            <script>
                \$(document).ready(function() {
                    \$('#deadline_${freelancer_id}_${task_type_id}').datetimepicker({
                      format:'Y-m-d H:i'
                    });
                });
                            </script>"

                # Rate
                if {$employee_p} {
                    lappend one_line "&nbsp;"
                } else {
                    
                    lappend one_line "<table>
                        <tr><td $bgcolor([expr $ctr % 2])>Rate</td><td $bgcolor([expr $ctr % 2])><input size=6 type=text name=\"${freelancer_id}_${task_type_id}_rate\" value=\"$rate\"></td></tr>
                        <tr><td $bgcolor([expr $ctr % 2])>Units</td><td $bgcolor([expr $ctr % 2])><input size=6 type=text name=\"${freelancer_id}_${task_type_id}_units\"></td></tr>
                        <tr><td $bgcolor([expr $ctr % 2])>UOM</td><td $bgcolor([expr $ctr % 2])>[im_category_select "Intranet UoM" "${freelancer_id}_${task_type_id}_uom_id" $uom_id]</td></tr>
                        </table>"
                }
    
                lappend one_line "$task_html </ul>"

                lappend ${task_type_id}_lines($freelancer_id) $one_line
                
            }
        }
        set num_freelancer_lines [llength [set ${task_type_id}_lines($freelancer_id)]]
        if {$num_freelancer_lines > $num_lines($freelancer_id)} {
            set num_lines($freelancer_id) $num_freelancer_lines
        }
    }
    append list_html "</tr>"

}

# ---------------------------------------------------------------
# Build the HTML TAble
# ---------------------------------------------------------------
set ctr 0
foreach freelancer_id $freelancer_ids {
incr ctr
    set target_language_id $freelancer_target_lang_id($freelancer_id)
    set target_language [im_category_from_id $target_language_id]
    
    if {[im_user_is_employee_p $freelancer_id]} {
        set provider_id [im_company_internal]
        set user_name [im_name_from_id $freelancer_id]
    } else {

        # finds company id (=provider_id) for each of the freelancers
        db_1row freelancer_info "
            select
                c.company_id as provider_id,
                im_name_from_user_id(object_id_two) as user_name
            from            acs_rels r,
                    im_companies c
            where       r.object_id_one = c.company_id
            and     r.object_id_two =:freelancer_id
            and rel_type = 'im_company_employee_rel' limit 1"
    }
        
    # Fill the first line. It must exist
    set line_ctr 0
    set next_line_p 1
    for {set i 0} {$i < $num_lines($freelancer_id)} {incr i} {

        set show_line_p 0
        set line_html ""
        if {$line_ctr eq 0} {
	    append line_html "<tr $bgcolor([expr $ctr % 2])>"
            append line_html "<td $bgcolor([expr $ctr % 2])>
                    <input type=hidden name=\"user_ids\" value=\"${freelancer_id}\">
                    <a href=\"/intranet/users/view?user_id=${freelancer_id}\">${user_name}</a>"

            if {[lsearch $currently_assigned_freelancers $freelancer_id]>-1} {
                set assignments_url [export_vars -base "/sencha-freelance-translation/project-assignments" -url {project_id {assignee_id $freelancer_id}}]
                append line_html "<ul><li><a href='$assignments_url'>[lang::message::lookup "" sencha-freelance-translation.Assignment Assignment]</a></li></ul>"
            }
            append line_html "</td><td $bgcolor([expr $ctr % 2])>$target_language</td>"
        } else {
	    append line_html "<tr $bgcolor([expr $ctr % 2])><td $bgcolor([expr $ctr % 2])></td><td $bgcolor([expr $ctr % 2])></td>"
        }
        foreach task_type_id $task_type_ids {
            set first_line [lindex [set ${task_type_id}_lines($freelancer_id)] $line_ctr]
            if {$first_line ne ""} {
                append line_html "<td $bgcolor([expr $ctr % 2])><nobr>[lindex $first_line 0]<nobr></td>"
                append line_html "<td align=center $bgcolor([expr $ctr % 2])>[lindex $first_line 1]</td>"
                if {[lindex $first_line 2] ne ""} {
                    append line_html "<td $bgcolor([expr $ctr % 2]) valign='top'>[lindex $first_line 2]</td>"
                    set show_line_p 1
                } else {
                    append line_html "<td $bgcolor([expr $ctr % 2])>&nbsp;</td>"
                }
            } else {
                append line_html "<td $bgcolor([expr $ctr % 2])>&nbsp;</td><td $bgcolor([expr $ctr % 2])>&nbsp;</td><td $bgcolor([expr $ctr % 2])>&nbsp;</td>"
            }
        }
        if {$show_line_p} {
            append list_html $line_html
        }
        incr line_ctr
        append list_html "</tr>"
    }
}
append list_html "</table>"

set left_navbar_html ""

# Setup the subnavbar
set bind_vars [ns_set create]
ns_set put $bind_vars project_id $project_id
set parent_menu_id [im_menu_id_from_label "project"]
set menu_label "project_assignments"

set sub_navbar [im_sub_navbar \
    -components \
    -base_url "/intranet/projects/view?project_id=$project_id" \
    $parent_menu_id \
    $bind_vars "" "pagedesriptionbar" "freelance_assignments"]
}
