
set element_list [list]
lappend element_list "assignee_name"
lappend element_list {
	label "Assignee"
	display_template {
		@multirow.assignee_name_pretty;noquote@
	}
}

lappend element_list target_language
lappend element_list {
	label "Target"
}

# ---------------------------------------------------------------
# Get the tasks supported in the project
# ---------------------------------------------------------------
set task_types [list]
set task_types_aux [db_list aux "
	select aux_string1
	from im_trans_tasks, im_categories
	where project_id = :project_id
	and category_id = task_type_id"]

set extend_list [list]

foreach task_aux $task_types_aux {
	foreach task_type $task_aux {
		if {[lsearch $task_types $task_type]<0} {
			lappend task_types $task_type
			lappend extend_list ${task_type}
			lappend element_list ${task_type}
			lappend element_list [list label $task_type]
		}
	}
}


# APpend the assignment status
lappend element_list status
lappend element_list [list label [_ intranet-core.Status]]

lappend element_list package_file
lappend element_list [list label [_ sencha-freelance-translation.Package_name]]


if {$user_id eq ""} {
    set user_id [im_require_login]
}

template::list::create \
-name "project_assignees" \
-multirow multirow \
-key assignee_id \
-elements $element_list


# Record the viewing of the project
im_freelance_record_view -object_id $project_id -viewer_id $user_id


lappend extend_list target_language assignee_name_pretty package_file

db_multirow -extend $extend_list multirow multirow "select distinct im_name_from_id(assignee_id) as assignee_name,
		assignee_id, fp.freelance_package_id, im_name_from_id(assignment_status_id) as status
	from im_freelance_assignments fa, im_freelance_packages fp
	where fp.freelance_package_id = fa.freelance_package_id
	and fp.project_id = :project_id" {
	set assignments_url [export_vars -base "/sencha-freelance-translation/project-assignments" -url {assignee_id project_id}]
	set assignee_name_pretty "<a href='$assignments_url'>$assignee_name</a>"
	set target_language [db_string target_lang "select im_name_from_id(target_language_id)
		from im_trans_tasks tt, im_freelance_packages_trans_tasks fptt
		where fptt.freelance_package_id = :freelance_package_id
		and tt.task_id = fptt.trans_task_id
		limit 1" -default ""]
		
	foreach task_type $task_types {
		set assignment_ids [db_list assignment_ids "select assignment_id
			from im_freelance_assignments fa, im_categories c, im_freelance_packages fp
			where c.category_id = fa.assignment_type_id
			and c.category = :task_type
			and fa.assignee_id = :assignee_id
			and fa.freelance_package_id = fp.freelance_package_id
			and fp.project_id = :project_id
			and c.category_type = 'Intranet Trans Task Type'"]
		

		# Mark the entry bold if we have not seen it yet
		set viewed_p 1
	    set package_file "Missing"
		foreach assignment_id $assignment_ids {
			if {![views::viewed_p -object_id $assignment_id -user_id $user_id]} {
				set viewed_p 0
			}
		    
		    # Check for the package files
		    set package_file_list [list]
		    db_foreach freelance_package_file "select freelance_package_file_id, freelance_package_file_name, file_path, package_file_type_id from im_freelance_package_files fpf, im_freelance_assignments fa where fpf.freelance_package_id = fa.freelance_package_id and fa.assignment_id = :assignment_id" {
			lappend package_file_list "$freelance_package_file_name ([im_name_from_id $package_file_type_id])"
		    }
		    if {[llength $package_file_list]>0} {
			set package_file [join $package_file_list "<br />"]
		    }
		}
		set $task_type [llength $assignment_ids]

		if {!$viewed_p} {
			set assignee_name_pretty "<b>$assignee_name_pretty</b>"
		}
	}
}
template::multirow sort multirow target_language assignee_name

	
