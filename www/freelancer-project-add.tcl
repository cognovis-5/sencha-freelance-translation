
ad_page_contract {
    @author malte.sussdorff@cognovis.de
} {
    {project_id:integer ""}
    {freelancer_ids ""}
    {user_id ""}
    {auto_login ""}
    {format "json"}
    {user_id_from_search:integer,multiple ""}
    {object_id:integer ""}
}

if {$user_id ne ""} {
    set expected_auto_login [im_generate_auto_login -user_id $user_id]
    if {![string equal $auto_login $expected_auto_login]} {
        im_rest_error -format $format -http_status 406 -message "Not authorized with $user_id and $auto_login"
    }
} else {
    set user_id [auth::require_login]
}

if {$project_id eq ""} {set project_id $object_id}

# Check if project_id exists
if {![db_string project_exists_p "select project_id from im_projects where project_id = :project_id" -default 0]} {
    im_rest_error -format $format -http_status 406 -message "The Project $project_id does not exist"
}

# ---------------------------------------------------------------
# Add the business relationship
# ---------------------------------------------------------------
set role_id [im_biz_object_role_full_member]
set touched_p 0

set project_member_ids [im_biz_object_member_ids $project_id]
if {$freelancer_ids eq ""} {
    set freelancer_ids $user_id_from_search
} else {
    set freelancer_ids [split $freelancer_ids ","]
}

foreach freelancer_id $freelancer_ids {
    if {[lsearch $project_member_ids $freelancer_id]<0} {
        callback im_before_member_add -user_id $freelancer_id -object_id $project_id
        im_biz_object_add_role $freelancer_id $project_id $role_id
        set touched_p 1
        ns_log Notice "Adding $freelancer_id to $project_id"
    }
}

if {$touched_p} {
    
    # Get an array of freelancers for each of the target languages of the project
    db_foreach language {select language_id from im_target_languages where project_id = :project_id} {
        set target_lang_freelancers($language_id) [db_list freelancers "select user_id from im_freelance_skills where skill_id = :language_id"]
    }
    
    set trans_freelancer_ids [list]
    set edit_freelancer_ids [list]
    set proof_freelancer_ids [list]
    set company_id [db_string company "select coalesce(final_company_id,company_id) as company_id from im_projects where project_id = :project_id" -default ""]
    foreach freelancer_id $freelancer_ids {
        set main_task_type [im_freelance_trans_main_task_type -freelancer_id $freelancer_id -company_id $company_id]
	lappend ${main_task_type}_freelancer_ids $freelancer_id
    }
    
    # ---------------------------------------------------------------
    # Automatically add to tasks in the language
    # ---------------------------------------------------------------
    
    # Assign not yet assigned trans tasks
    db_foreach task {select task_id, target_language_id from im_trans_tasks where project_id = :project_id and trans_id is null} {
        foreach freelancer_id $trans_freelancer_ids {
            # Check if the freelancer knows the language
            if {[lsearch $target_lang_freelancers($target_language_id) $freelancer_id]>-1} {
                db_dml update "update im_trans_tasks set trans_id = :freelancer_id where task_id = :task_id"
                break
            }
        }
    }
    
    # Assign not yet assigned edit tasks
    db_foreach task {select task_id, target_language_id from im_trans_tasks where project_id = :project_id and edit_id is null} {
        foreach freelancer_id $edit_freelancer_ids {
            # Check if the freelancer knows the language
            if {[lsearch $target_lang_freelancers($target_language_id) $freelancer_id]>-1} {
                db_dml update "update im_trans_tasks set edit_id = :freelancer_id where task_id = :task_id"
                break
            }
        }
    }
    # Assign not yet assigned proof tasks
    db_foreach task {select task_id, target_language_id from im_trans_tasks where project_id = :project_id and proof_id is null} {
        foreach freelancer_id $proof_freelancer_ids {
            # Check if the freelancer knows the language
            if {[lsearch $target_lang_freelancers($target_language_id) $freelancer_id]>-1} {
                db_dml update "update im_trans_tasks set proof_id = :freelancer_id where task_id = :task_id"
                break
            }
        }
    }
}

if {$touched_p} {
    # record that the object has changed
    db_dml update_object "
        update acs_objects set
                last_modified = now(),
                modifying_user = :user_id,
                modifying_ip = '[ad_conn peeraddr]'
        where object_id = :project_id
    "

    # Audit the object
    im_audit -object_id $project_id -action "after_update" -comment "After adding members"
}

if {$object_id ne ""} {
    ad_returnredirect [export_vars -base "/intranet-translation/trans-tasks/task-assignments" -url {project_id}]
}
set result "{\"success\": true,\n\"message\": \"[im_quotejson "Added members $freelancer_ids to project $project_id"]\"\n}"
doc_return 200 "application/json" $result
