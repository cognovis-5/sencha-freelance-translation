# packages/intranet-invoices/www/pdf.tcl
#
# Copyright (c) 2015, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
ad_page_contract {
	@author ND

	Show the user the details of a single assignment.
} {
	task_id:integer
	{return_url ""}
}
im_freelance_record_view -object_id $task_id -viewer_id [ad_conn user_id]

