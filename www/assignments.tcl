ad_page_contract {
    Show the list of current assignments and allow the pm to create new assignments

    @author malte.sussdorff@cognovis.de
} {
    {project_id:integer ""}
    {object_id:integer ""}
    {freelancer_ids ""}
    {user_id_from_search:integer,multiple ""}
    {display_only_p 1}
}


if {$project_id eq ""} {set project_id $object_id}
if {$freelancer_ids eq ""} {
    set freelancer_ids $user_id_from_search
} else {
    set freelancer_ids [split $freelancer_ids ","]
    set display_only_p 0
}

set user_id [ad_maybe_redirect_for_registration]
set return_url [im_url_with_query]

set project_nr [db_string project_nr "select project_nr from im_projects where project_id = :project_id" -default ""]
set page_title "$project_nr - Assignments"

set context_bar [im_context_bar [list /intranet/projects/ "[_ intranet-core.Projects]"] [list "/intranet/projects/view?project_id=$project_id" "[_ intranet-translation.One_project]"] $page_title]
set company_view_page "/intranet/companies/view"

# Setup the subnavbar
set bind_vars [ns_set create]
ns_set put $bind_vars project_id $project_id
set parent_menu_id [db_string parent_menu "select menu_id from im_menus where label='project'" -default 0]

set menu_label "project_assignments"
set sub_navbar [im_sub_navbar \
    -components \
    -base_url "/intranet/projects/view?project_id=$project_id" \
    $parent_menu_id \
    $bind_vars "" "pagedesriptionbar" $menu_label]
