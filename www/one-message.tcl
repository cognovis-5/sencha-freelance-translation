# /packages/intranet-mail/lib/one-message.tcl
ad_page_contract {
    Displays one message that was send to a user
    
    @author malte.sussdorff@cognovis.de
    @creation-date 2017-08-24
} -query {
    log_id:notnull
    {return_url ""}
} -validate {
    message_exists  -requires {log_id} {
        if { ![db_0or1row message_exists_p "select 1
        	from acs_mail_log
        	where log_id = :log_id"]} {
            ad_complain "<b>[_ intranet-mail.The_specified_message_does_not_exist]</b>"
        }
    }
}

# We need to figure out a way to detect which contacts package a party_id belongs to

set page_title "[_ intranet-mail.One_message]"

if { [empty_string_p $return_url] } {
    set return_url [get_referrer]
}
set user_id [im_require_login]

# Forward and reply email
set forward_url [export_vars -base "forward" -url {log_id return_url}]
set reply_url [export_vars -base "reply" -url {log_id return_url}]

# Get the information of the message
db_1row get_message_info "select log_id, sent_date, sender_id, from_addr,
    subject, body, context_id
    from acs_mail_log
    where log_id = :log_id"

set sent_date_pretty [lc_time_fmt $sent_date "%q %X"]

if {[exists_and_not_null sender_id]} {
	set sender [party::name -party_id $sender_id]
} else {
    set sender "$from_addr"
}

# We get the related files
set tracking_url [apm_package_url_from_key "intranet-mail"]
set download_files [list]
db_foreach files "select cr.title, cr.revision_id as version_id
    from acs_mail_log_attachment_map lam, cr_revisions cr
    where log_id = :log_id
    and cr.revision_id = lam.file_id" {
    append download_files "<a href=\"[export_vars -base "${tracking_url}download/$title" -url {version_id}]\">$title</a><br>"
}

if {![ad_looks_like_html_p $body]} {
    set body "<pre>$body</pre>"
}

# Record the view of the message
im_freelance_record_view -object_id $log_id -viewer_id $user_id
