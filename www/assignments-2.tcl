ad_page_contract {
    @author ND
} {
    project_id:integer
    user_ids:integer,multiple
    task_type_ids:integer,multiple
    action
}

set form [ns_conn form]

set html ""

if {$action eq "back"} {
	# Return to project
	ad_returnredirect "/intranet/projects/view?project_id=$project_id"
}

if {$action eq "assign"} {
    # Check that we don't have assigned the same file twice for a task_id
    foreach task_type_id $task_type_ids {
        set file_ids [list]
        foreach user_id $user_ids {
            foreach file_id [ns_querygetall ${user_id}_${task_type_id}_bulk_file_ids] {
                if {[lsearch $file_ids $file_id]<0} {
                    lappend file_ids $file_id
                } else {
                    ad_return_error "File Task assigned twice" "You have assigned the file $file_id for task type $task_type_id twice."
                }
            }
        }
    }
}

set project_member_ids [im_biz_object_member_ids $project_id]
set role_id [im_biz_object_role_full_member]
set previous_task_type_id ""
set previous_task_type ""
db_1row project_info "select to_char(start_date,'YYYY-MM-DD HH24:MI') as project_start_date, project_status_id,company_id, subject_area_id,source_language_id from im_projects where project_id = :project_id"

if {[lsearch [im_sub_categories [im_project_status_open]] $project_status_id]<0} {
  	set open_p 0
} else {
  	set open_p 1
}

foreach task_type_id $task_type_ids {
    set task_type [db_string task_type "select lower(category) from im_categories where category_id = :task_type_id"]
    foreach user_id $user_ids {
    #    set locale_arr(${user_id}) [lang::user::locale -user_id $user_id]
        set locale_arr(${user_id}) "en_US"
        
##		set briefing_arr(${task_type_id}) [ns_queryget ${task_type_id}_briefing]
##		set briefing $briefing_arr($task_type_id)
		set deadline [ns_queryget ${user_id}_${task_type_id}_deadline]
		set rate [ns_queryget ${user_id}_${task_type_id}_rate]
		set uom_id [ns_queryget ${user_id}_${task_type_id}_uom_id]
		set units [ns_queryget ${user_id}_${task_type_id}_units]

		set assignment_ids [list]
		
		switch $action {
			save {
				set assignment_status_id 4220
			}
			request {
				# The resetting will be done upon sendin
				# We need the status to created for duplicate
				# Mail sending check
				set assignment_status_id 4220
			}
			assign {
				set assignment_status_id 4222
			}
		    default {
				set assignment_status_id 4220
		    }
		}

		# ---------------------------------------------------------------
		# Work with task_ids
		# We have those in case we did not assign all tasks in packages
		# yet. Typically if we do split assignments.
		# ---------------------------------------------------------------
		
		set bulk_file_ids(${user_id},${task_type_id}) [ns_querygetall ${user_id}_${task_type_id}_bulk_file_ids]
		set task_ids $bulk_file_ids(${user_id},${task_type_id})
		

        if {$task_ids ne ""} {
	        # Get the list of target_languages
        	db_1row target_languages "select distinct source_language_id, target_language_id from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $task_ids]) limit 1"
	        set bulk_file_names(${user_id},${task_type_id}) [list]
	        
			# ---------------------------------------------------------------
			# Check if we have multiple uoms in the tasks
			# If yes, we need to have provided units to override the
			# Default total_number_of_task_units
			# ---------------------------------------------------------------
        	if {$units eq ""} {
	        	set task_uom_ids [db_list task_uom "select distinct task_uom_id from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $task_ids])"]
	    	    set uom_id [lindex $task_uom_ids 0]
    	    	if {[llength $task_uom_ids]>1} {
		    set total_number_of_task_units ""
#	    	    	ad_return_error "Multiple Units of Measure" "Can't assign different task UomIDS ($task_uom_ids) for [im_name_from_id $user_id]"
    	    	} else {

	    	    set total_number_of_task_units [db_string task_units "select sum(task_units) from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $bulk_file_ids(${user_id},${task_type_id})])"]
		}
			} else {
				set total_number_of_task_units $units
			}
			
	        # Do not allow the assignment of tasks which all have no task_units.
	        #if { $total_number_of_task_units == 0 } {
	        #    continue
	        #}

	        # ---------------------------------------------------------------
	        # Deadline Calculation
	        # We assume that the task types are in order. So we
	        # set the deadline of the task_id and the task_type_id
	        # Find the deadline for the previous task_type_id
	        # ---------------------------------------------------------------
	        set previous_deadlines [list]
	        foreach task_id $task_ids {
	            set deadline_arr($task_type_id,$task_id) $deadline
	            if {$previous_task_type_id ne ""} {
	                # Get a list of previous deadlines
	                if {[info exists deadline_arr($previous_task_type_id,$task_id)]} {
	                    lappend previous_deadlines $deadline_arr($previous_task_type_id,$task_id)
	                } else {
	                    set prev_deadline [db_string deadline_from_db "select ${previous_task_type}_end_date from im_trans_tasks where task_id = :task_id" -default ""]
	                    if {$deadline ne ""} {
	                        lappend previous_deadlines $prev_deadline
	                    }
	                }
	            }
	        }
        
	        # Find the maximum as the start_date
	        set start_date [lindex [lsort -dictionary -decreasing $previous_deadlines] 0]

			# Start date is now if the project is open and we don't have a previous deadline.
	        if {$start_date eq "" && $open_p} {
		        set start_date [db_string now "select to_char(now(),'YYYY-MM-DD HH24:MI') from dual"]
	        }
	        
	        # MIN PRICE!!!!!!
	        if {$rate ne ""} {
		        if {$uom_id eq [im_uom_unit]} {
		            set total_amount $rate
		        } else {
		            set total_amount "[expr {double(round(100* $total_number_of_task_units * $rate))/100}]"
		        }
		    } else {
			    set total_amount ""
		    }
	
	        set task_ids $bulk_file_ids(${user_id},${task_type_id})
	        lappend assignment_ids [im_freelance_assignment_create \
			    -task_ids $task_ids \
			    -assignee_id $user_id \
			    -assignment_type_id $task_type_id \
			    -assignment_status_id $assignment_status_id \
			    -assignment_units $total_number_of_task_units \
			    -end_date $deadline \
			    -start_date $start_date \
			    -rate $rate \
			    -uom_id $uom_id]
		}
		
		
		# ---------------------------------------------------------------
		#  Assign packages
		# ---------------------------------------------------------------

		foreach freelance_package_id [ns_querygetall ${user_id}_${task_type_id}_bulk_freelance_package_ids] {
			
			if {$units eq ""} {
				set total_number_of_task_units [db_string calc_total "select sum(task_units) from im_trans_tasks tt, im_freelance_packages_trans_tasks fptt where fptt.trans_task_id = tt.task_id and fptt.freelance_package_id = :freelance_package_id" -default 0]
			} else {
				set total_number_of_task_units $units
			}
			
			
			# ---------------------------------------------------------------
			# Deadline Calculation
			# We assume that the task types are in order. So we
			# set the deadline of the task_id and the task_type_id
			# Find the deadline for the previous task_type_id
			# ---------------------------------------------------------------
			set previous_deadlines [list]
			set package_task_ids [db_list tasks "select trans_task_id
				from im_freelance_packages_trans_tasks
				where freelance_package_id = :freelance_package_id"]
				
			foreach task_id $task_ids {
				set deadline_arr($task_type_id,$task_id) $deadline
				if {$previous_task_type_id ne ""} {
					# Get a list of previous deadlines
					if {[info exists deadline_arr($previous_task_type_id,$task_id)]} {
						lappend previous_deadlines $deadline_arr($previous_task_type_id,$task_id)
					} else {
						set prev_deadline [db_string deadline_from_db "select ${previous_task_type}_end_date from im_trans_tasks where task_id = :task_id" -default ""]
					    # We might have to calculate the previous deadline based onthe assignments. Take a look at project-assignments.tcl how it is done there.
						if {$deadline ne ""} {
							lappend previous_deadlines $prev_deadline
						}
					}
				}
			}
				   
			# Find the maximum as the start_date
			set start_date [lindex [lsort -dictionary -decreasing $previous_deadlines] 0]
			
			# Start date is now if the project is open and we don't have a previous deadline.
			if {$start_date eq "" && $open_p} {
				set start_date [db_string now "select to_char(now(),'YYYY-MM-DD HH24:MI') from dual"]
			}
			
			ds_comment "Deadline...$start_date $deadline "
			

			
			set assignment_id [im_freelance_assignment_create \
				-freelance_package_id $freelance_package_id \
				-assignee_id $user_id \
			    -assignment_type_id $task_type_id \
			    -assignment_status_id $assignment_status_id \
			    -assignment_units $total_number_of_task_units \
			    -start_date $start_date \
			   	-end_date $deadline \
			    -rate $rate \
			    -uom_id $uom_id
			]
			
			lappend assignment_ids $assignment_id
		}
		foreach assignment_id $assignment_ids {
			# ---------------------------------------------------------------
		    # Deal with actions
		    # Possible actions: Save (for later); Request (send mail);
		    # Assign (directly assign and create PO)
		    # ---------------------------------------------------------------
	        switch $action {
		        "save" {
			        # Do Nothing (probably)
		        }
	            "request" {
	                # Sending will set the status to requested upon sending
	                im_freelance_trans_send_request -assignment_id $assignment_id
	            }
	            "assign" {
	                # Update the status to assigned
	                db_dml update "update im_freelance_assignments set assignment_status_id = 4222 where assignment_id = :assignment_id"
				    if {[lsearch $project_member_ids $user_id]<0} {
				        callback im_before_member_add -user_id $user_id -object_id $project_id
				        im_biz_object_add_role $user_id $project_id $role_id
				        set touched_p 1
				        ns_log Notice "Adding $user_id to $project_id"
				    }

					set task_ids [db_list tasks_from_assignment "select trans_task_id from im_freelance_packages_trans_tasks fptt, im_freelance_assignments fa where fa.freelance_package_id = fptt.freelance_package_id and fa.assignment_id = :assignment_id and assignment_type_id = :task_type_id"]
	                # Update the tasks
	                foreach task_id $task_ids {
	                    db_dml update "update im_trans_tasks set ${task_type}_id = :user_id , ${task_type}_end_date = to_date(:deadline,'YYYY-MM-DD HH24:MI') where task_id = $task_id"
	                }
	                
	                # Create the purchase order
	                set invoice_id [im_freelance_create_purchase_orders -assignment_id $assignment_id]
	    	    }
		    }
	    }
    }
    set previous_task_type_id $task_type_id
    set previous_task_type $task_type
}

switch $action {
	"save" {
		ad_returnredirect [export_vars -base "assignments" -url {project_id}]
	}
    "assign" {
        # Redirect to the assignment page
        ad_returnredirect [export_vars -base "assignments" -url {project_id {display_only_p 1}}]
    }
    default {
        ad_returnredirect "/intranet/projects/view?project_id=$project_id"
    }
}


