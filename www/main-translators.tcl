ad_page_contract {
    @author ND
} {
    {customer_id:integer ""}
    {freelancer_id:integer ""}
}

set user_id [ad_maybe_redirect_for_registration]


template::head::add_meta -http_equiv "X-UA-Compatible" -content "IE=edge"
template::head::add_meta -name "viewport" -content "width=device-width, initial-scale=1, maximum-scale=10, user-scalable=yes"

template::head::add_javascript -src "[apm_package_url_from_key "sencha-freelance-translation"]/main-translators.js"
# im_company_permissions $user_id $company_id view read write admin

template::head::add_javascript -script "
	   var serverParams = {
       	serverUrl: '[ad_url]'
       };
       serverParams.restUrl = serverParams.serverUrl + '/intranet-rest/';"


set filter_js_list [list]
set counter 0
foreach v [list freelancer_id customer_id] {
	if {[exists_and_not_null $v]} {
		incr counter
		lappend filter_js_list "
			\{
			   	name: '$v',
			   	values: \[ [set $v] \]
		   \}
		"
	}
}


set mainTranslatorsFilter_js "
   var mainTranslatorsFilter = \{
	   total: $counter,
	   data: \[
	   		[join $filter_js_list ", \n"]
	   \]
   \};"

if {$freelancer_id ne "" || $customer_id ne ""} {
    append mainTranslatorsFilter_js "var mainTranslatorsHideColumn = mainTranslatorsFilter.data\[0\].name;"
} else {
    append mainTranslatorsFilter_js "var mainTranslatorsHideColumn = '';"
}

template::head::add_javascript -script $mainTranslatorsFilter_js
