# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Template for email inclusion
# @author Malte Sussdorff (sussdorff@sussdorff.de)
# @creation-date 2005-06-14
ad_page_contract {
	@author malte.sussdorff@cognovis.de

	Send an E-mail for the assignee in the project
	
	@param assignee_id for which this communication is stored. if he is the sender, the recipient is the project lead
	@param project_id project
} {
	assignee_id:integer,notnull
	project_id:integer,notnull
}

set mime_type "text/html"
set return_url [export_vars -base "/sencha-freelance-translation/project-assignments" -url {assignee_id project_id}]
set user_id [im_require_login]

# ---------------------------------------------------------------
# Get E-Mails
# ---------------------------------------------------------------

db_1row project_info "select project_nr, project_lead_id from im_projects p where p.project_id = :project_id"
		
set assignment_id [db_string assignment "select min(assignment_id)
		from im_freelance_assignments fa, im_freelance_packages fp
		where fa.freelance_package_id = fp.freelance_package_id
		and fp.project_id = :project_id
		and fa.assignee_id = :assignee_id
	" -default ""]
		
if {$assignment_id eq ""} {
	util_user_message -html -message "Missing Assignment for [im_name_from_id $assignee_id] in $project_nr"
	ad_returnredirect $return_url
}
	
set edit_buttons [list [list [_ acs-mail-lite.Send] send]]

ad_form -action [ad_conn url] \
    -html {enctype multipart/form-data} \
    -name email \
    -cancel_label "[_ acs-kernel.common_Cancel]" \
    -cancel_url $return_url \
    -edit_buttons $edit_buttons \
    -export {project_id assignee_id assignment_id return_url} \
    -form {
	    {message_id:key}
		{sender_id:text(hidden)}
    	{recipient_ids:text(hidden)}
    	{-section "message" {legendtext "[_ acs-mail-lite.Message]"}}
    	{subject:text(text),optional
    		{label "[_ acs-mail-lite.Subject]"}
    			{html {size 55}}
    	}
    	{content_body:text(richtext),optional
    		{label "[_ acs-mail-lite.Message]"}
    		{html {cols 55 rows 18}}
    	}
    	{upload_file:file(file),optional
    		{label "[_ acs-mail-lite.Upload_file]"}
    	}
	} -on_request {

		set sender_id $user_id
		if {$user_id eq $assignee_id} {
			# Message from the assignee to the pm
			set recipient_ids $project_lead_id
		} else {
			if {$user_id eq $project_lead_id} {
				set recipient_ids $assignee_id
			} else {
				set recipient_ids [list $assignee_id $project_lead_id]
			}
		}
	} -on_submit {
        # Insert the uploaded file linked under the package_id
        set package_id [ad_conn package_id]
        
        if {![empty_string_p $upload_file] } {
            set revision_id [content::item::upload_file \
                                 -package_id $package_id \
                                 -upload_file $upload_file \
                                 -parent_id $user_id]
        }
        
        if {[exists_and_not_null revision_id]} {
			set file_ids $revision_id
		} else {
			set file_ids [list]
		}
		
		set log_id [im_freelance_message_send \
			-sender_id $sender_id \
			-recipient_ids $recipient_ids \
			-subject "$subject" \
			-body "$content_body" \
			-context_id $assignment_id \
			-file_ids $file_ids \
			-package_id $package_id \
			-project_id $project_id]

		im_freelance_record_view -object_id $log_id -viewer_id $user_id

        util_user_message -html -message "Message recorded"
    } -after_submit {
        ad_returnredirect $return_url
    }

