<master>
<property name="title">@page_title;noquote@</property>
<div style="background-color: #eee; padding: .5em;">
<table>
<tr><td>
#intranet-mail.Sent_Date#:</td><td>@sent_date_pretty;noquote@</tr><td>
#intranet-mail.Sender#:</td><td>@sender;noquote@</tr><td>
#intranet-mail.Subject#:</td><td>@subject;noquote@</tr><td>

</table>
</div>
<p>
@body;noquote@
<p>
	@download_files;noquote@
</p>
<!-- <a href="@return_url;noquote@">#intranet-mail.Go_Back#</a> | <a href="@reply_url;noquote@">#intranet-mail.Reply_To#</a> -->
<a href="@return_url;noquote@">#intranet-mail.Go_Back#</a>
