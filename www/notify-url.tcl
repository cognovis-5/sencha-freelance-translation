# /packages/sencha-freelance-translation/www/notify-url
ad_page_contract {
    Redirect to the object while recording the view
    
    @author malte.sussdorff@cognovis.de
    @creation-date 2017-08-24
} -query {
    object_id:notnull
}

# Record the view
im_freelance_record_view -object_id $object_id -viewer_id [ad_conn user_id]

# Redriect

ad_returnredirect [im_biz_object_url $object_id]
