ad_page_contract {
    @author malte.sussdorff@cognovis.de
} {
    assignee_id:integer,notnull
    assignment_id:integer,notnull
    project_id:integer,notnull
}

set user_id [im_require_login]
# Check that the assignment is still available
# And no other user has been assigned for tasks in this assignment

db_1row assignment_info "select freelance_package_id, assignment_type_id, rate, uom_id,end_date as deadline from im_freelance_assignments where assignment_id = :assignment_id"
db_1row project_info "select project_nr,project_lead_id from im_projects where project_id = :project_id"

set existing_assignee_id [db_string assigned_p "select assignee_id from im_freelance_assignments where freelance_package_id = :freelance_package_id
    and assignment_type_id = :assignment_type_id
    and assignment_status_id in (4222,4224,4225,4226) limit 1" -default 0]


set task_type [string tolower [im_category_from_id -translate_p 0 $assignment_type_id]]
set task_id [db_list tasks_from_assignment "select min(trans_task_id) from im_freelance_packages_trans_tasks fptt, im_freelance_assignments fa where fa.freelance_package_id = fptt.freelance_package_id and fa.assignment_id = :assignment_id and assignment_type_id = :assignment_type_id"]
set target_language_id [db_string target "select target_language_id from im_trans_tasks where task_id = :task_id"]
set assignments_url [export_vars -base "/sencha-freelance-translation/project-assignments" -url {project_id assignee_id}]

if {$existing_assignee_id eq $user_id} {
    # Notify the project_lead
    im_freelance_notify -object_id $assignment_id -recipient_ids $project_lead_id \
	-message "DENY AFTER ACCEPT - <a href='$assignments_url'>$project_nr</a> :: $task_type tried to be denied by [im_name_from_id $assignee_id] for [im_category_from_id $target_language_id]" \
	-severity 1

    ad_complain "task already accepted, cannot deny now"
} else {

    # Update Status to denied
    db_dml update "update im_freelance_assignments set assignment_status_id = 4223 where assignment_id = :assignment_id"
    
    im_freelance_notify -object_id $assignment_id -recipient_ids $project_lead_id \
	-message "<a href='$assignments_url'>$project_nr</a> :: $task_type denied by [im_name_from_id $assignee_id] for [im_category_from_id $target_language_id]" \
	-severity 1
}

