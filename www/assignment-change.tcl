# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Template for email inclusion
# @author Malte Sussdorff (sussdorff@sussdorff.de)
# @creation-date 2005-06-14

ad_page_contract {
    See if this person is authorized to read the task file,
    guess the MIME type from the original client filename and
    write the binary file to the connection

    @author malte.sussdorff@cognovis.de
    @creation-date 2017-07-08
} {
    assignment_id:integer
    return_url
}
set current_user_id [ad_maybe_redirect_for_registration]

set page_title "Change assignment"


ad_form -action [ad_conn url] \
    -html {enctype multipart/form-data} \
    -name assignment_change \
    -export [list return_url] \
    -cancel_label "[_ acs-kernel.common_Cancel]" \
    -cancel_url $return_url \
    -form {
	assignment_id:key
	{start_date:text(text) {label "[lang::message::lookup "" sencha-freelance-translation.Available_from "Available From"]"} {after_html {
	    <link href="/intranet-jquery/jquery_datetimepicker/jquery.datetimepicker.css" media="screen" rel="stylesheet" type="text/css">
	    <script src="/intranet-jquery/jquery_datetimepicker/jquery.js" type="text/javascript"></script>
	    <script src="/intranet-jquery/jquery_datetimepicker/build/jquery.datetimepicker.full.min.js" type="text/javascript"></script>        
	    <script> \$(document).ready(function() {
		\$('#start_date').datetimepicker({
      format:'Y-m-d H:i'
		});
	    });
	    </script>}}}
	{end_date:text(text) {label "[_ intranet-timesheet2.End_Date]"} {after_html {
	    <link href="/intranet-jquery/jquery_datetimepicker/jquery.datetimepicker.css" media="screen" rel="stylesheet" type="text/css">
	    <script src="/intranet-jquery/jquery_datetimepicker/jquery.js" type="text/javascript"></script>
	    <script src="/intranet-jquery/jquery_datetimepicker/build/jquery.datetimepicker.full.min.js" type="text/javascript"></script>        
	    <script> \$(document).ready(function() {
		\$('#end_date').datetimepicker({
      format:'Y-m-d H:i'
		});
	    });
	    </script>}}}
    } -select_query {
select start_date, end_date from im_freelance_packages fp, im_freelance_assignments fa
 where fp.freelance_package_id = fa.freelance_package_id
 and fa.assignment_id = :assignment_id

    } -edit_data {
	db_dml update_assignment "update im_freelance_assignments set start_date = :start_date, end_date = :end_date where assignment_id = :assignment_id"
    } -after_submit {
        ad_returnredirect $return_url
    }

