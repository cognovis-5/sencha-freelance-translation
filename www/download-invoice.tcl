# packages/intranet-invoices/www/pdf.tcl
#
# Copyright (c) 2015, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
ad_page_contract {
	@author ND

	Show the user the details of a single assignment.
} {
	token:trim,notnull
	assignee_id:integer,notnull
	invoice_id:integer,notnull
} -validate {
	check_token {
		set validate_token [ns_sha1 someSeCrEt-${assignee_id}-${invoice_id}]
		if { $validate_token ne $token } {
			ad_complain "invalid token"
		}
	}
}

set page_title "[_ intranet-invoices.Invoice_Mail]"
db_1row invoice_info "select invoice_nr,last_modified from im_invoices,acs_objects where invoice_id = :invoice_id and invoice_id = object_id"

set invoice_revision_id [intranet_openoffice::invoice_pdf -invoice_id $invoice_id -preview_p 1]

set outputheaders [ns_conn outputheaders]
ns_set cput $outputheaders "Content-Disposition" "attachment; filename=${invoice_nr}.pdf"
ns_returnfile 200 application/pdf [content::revision::get_cr_file_path -revision_id $invoice_revision_id]
