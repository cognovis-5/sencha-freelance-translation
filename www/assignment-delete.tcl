
ad_page_contract {
    @author malte.sussdorff@cognovis.de
} {
    assignment_id:integer
    project_id:integer
    {return_url ""}
}


db_1row assignment_info "select im_name_from_id(assignment_type_id) as task_type, freelance_package_id
    from im_freelance_assignments
    where assignment_id = :assignment_id"




# Remove from the tasks
set task_ids [db_list tasks "select trans_task_id from im_freelance_packages_trans_tasks
    where freelance_package_id = :freelance_package_id"]

foreach task_id $task_ids {
    db_dml update_task "update im_trans_tasks set ${task_type}_id = null where task_id = :task_id"
}

db_dml update "update im_freelance_assignments set assignment_status_id = 4230 where assignment_id = :assignment_id"

# commenting usage of $return_url as it usually redirected user to NON existing assignment
#if {$return_url eq ""} {
    #set return_url [export_vars -base "/sencha-freelance-translation/assignments" -url {project_id}]
#}
set return_url [export_vars -base "/intranet/projects/view" -url {project_id}]

ad_returnredirect $return_url
