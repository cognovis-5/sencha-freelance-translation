
ad_page_contract {
    @author malte.sussdorff@cognovis.de
} {
    {project_id:integer ""}
    {freelancer_ids ""}
    {user_id ""}
    {auto_login ""}
    {format "json"}
    {user_id_from_search:integer,multiple ""}
    {object_id:integer ""}  
}

if {$user_id ne ""} {
    set expected_auto_login [im_generate_auto_login -user_id $user_id]
    if {![string equal $auto_login $expected_auto_login]} {
        im_rest_error -format $format -http_status 406 -message "Not authorized with $user_id and $auto_login"
    }
} else {
    set user_id [auth::require_login]
}

if {$project_id eq ""} {set project_id $object_id}

# Check if project_id exists
if {![db_string project_exists_p "select project_id from im_projects where project_id = :project_id" -default 0]} {
    im_rest_error -format $format -http_status 406 -message "The Project $project_id does not exist"    
}

# ---------------------------------------------------------------
# Add the business relationship
# ---------------------------------------------------------------
set role_id [im_biz_object_role_full_member]
set touched_p 0

set project_member_ids [im_biz_object_member_ids $project_id]
if {$freelancer_ids eq ""} {
    set freelancer_ids $user_id_from_search
} else {
    set freelancer_ids [split $freelancer_ids ","]
}

if {$project_id ne ""} {
    foreach project_member_id $project_member_ids {
        if {[lsearch $freelancer_ids $project_member_id]>-1 && ![group::member_p -user_id $project_member_id -group_id [im_profile_employees]]} {
            # Check if we have a purchase order for the member
            set cost_present_p [db_string cost_p "select 1 from im_costs where project_id = :project_id and provider_id = (select company_id from im_companies where primary_contact_id = :project_member_id) limit 1" -default 0]
            if {!$cost_present_p} {
                # Remove him/her from all trans_tasks in the project
                db_dml remove_from_tasks "update im_trans_tasks set trans_id = null where trans_id = :project_member_id and project_id = :project_id"
                db_dml remove_from_tasks "update im_trans_tasks set edit_id = null where edit_id = :project_member_id and project_id = :project_id"
    
                # Need to remove the member
                im_exec_dml delete_user "im_biz_object_member__delete($project_id, $project_member_id)"
                callback im_biz_object_member_after_delete -object_type im_project -object_id $project_id -user_id $project_member_id
                set touched_p 1
            }
        }
    }
}

if {$touched_p} {
    # record that the object has changed
    db_dml update_object "
        update acs_objects set
                last_modified = now(),
                modifying_user = :user_id,
                modifying_ip = '[ad_conn peeraddr]'
        where object_id = :project_id
    "

    # Audit the object
    im_audit -object_id $project_id -action "after_update" -comment "After adding members"
}

if {$object_id ne ""} {
    ad_returnredirect [export_vars -base "/intranet-translation/trans-tasks/task-assignments" -url {project_id}]
}
set result "{\"success\": true,\n\"message\": \"[im_quotejson "Removed members $freelancer_ids from project $project_id"]\"\n}"
doc_return 200 "application/json" $result