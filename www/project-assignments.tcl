ad_page_contract {
    @author ND
    
    Show the user the details of a single assignment.
} {
    token:trim,optional
    assignee_id:integer,notnull
    project_id:integer,notnull
} -validate {
    check_token {
    }
}

if {[apm_package_installed_p "intranet-trans-trados"] && [im_trans_trados_project_p -project_id $project_id]} {
    set trados_p 1
} else {
    set trados_p 0
}

set show_create_missing_po_button_p 0

db_1row project_info "select aux_string1, project_nr, project_name, project_lead_id, im_name_from_id(project_lead_id) as project_lead, end_date from im_projects, im_categories where project_id = :project_id and project_type_id = category_id"

set task_type_ids [list]
foreach task_type $aux_string1 {
    set task_type_id [db_string type_id "select category_id from im_categories where lower(category) = :task_type and category_type = 'Intranet Trans Task Type'" -default ""]
    lappend task_type_ids $task_type_id
}

if {[exists_and_not_null token]} {
    set validate_token [ns_sha1 someSeCrEt-${assignee_id}-${project_id}]
    if { $validate_token ne $token } {
        ad_complain "invalid token"
    } else {
        set user_id $assignee_id
        ad_user_login $assignee_id
    }
} else {
    set user_id [im_require_login]
}

callback project_assignment_before_view -assignee_id $assignee_id -user_id $user_id -project_id $project_id

# Get possible assignments ids
set possibly_notified_assignments_ids [db_list possibly_notified_assignments_ids "select assignment_id from im_freelance_assignments fa, im_freelance_packages fp where fa.freelance_package_id = fp.freelance_package_id and fa.assignee_id = :assignee_id and fp.project_id =:project_id"]
if {[llength $possibly_notified_assignments_ids] > 0} {
    db_dml update_notifications "update im_freelance_notifications set viewed_date = now() where object_id in ([template::util::tcl_to_sql_list $possibly_notified_assignments_ids]) and viewed_date is null and user_id =:user_id"
}

# Make sure we have the skills set
set skill_2006 ""
set skill_2024 ""
set skill_2014 ""
set skill_2000 ""
set skill_2002 ""

db_foreach skills_for_object "select skill_type_id, skill_id from im_object_freelance_skill_map, im_categories where object_id = :project_id and category_id = skill_type_id order by sort_order " {
    set skill_$skill_type_id [im_category_from_id $skill_id]
}


# ---------------------------------------------------------------
# Assignments table
# ---------------------------------------------------------------

#check permission for freelancer rating
set employee_p [im_user_is_employee_p $user_id]
#select current rating
set current_rating [db_string sql "select quality_id from im_freelancer_quality where project_id =:project_id and user_id = :assignee_id" -default 0]
#set url for custom JS code in project-assignments.adp
set rate_url [ad_url]/sencha-freelance-translation/freelancer-rate?
set select_quality_html "<select onchange='javascript:rateFreelancer($project_id, $assignee_id, this.value)'>"
if {$current_rating == 0} {
    append select_quality_html "<option selected value=0>Not rated yet</option>"
}
set quality_sql "select * from im_categories where category_type = 'Intranet Quality' and enabled_p = 't' " ;
db_foreach currency $quality_sql {
    if {$current_rating == $category_id} {
        append select_quality_html "<option selected value='$category_id'>$category</option>"
    } else {
        append select_quality_html "<option value='$category_id'>$category</option>"
    }
    
}

set elements {
        count {
            label {[_ sencha-freelance-translation.assignment_count]}
            display_template {
                @assignments.creation_date;noquote@
            }
        }
        freelance_package_name {
            label {[_ sencha-freelance-translation.package_name]}
            display_template {
                <a href="@assignments.package_url@">@assignments.freelance_package_name;noquote@</a>
		<ul><li>@assignments.task_names_html;noquote@</li>
            }
        }
        amount {
            label {[_ sencha-freelance-translation.amount]}
            display_template {
                @assignments.assignment_units@ @assignments.uom@
            }
        }
        po_amount {
            label {[_ sencha-freelance-translation.salary]}
            display_template {
                @assignments.po_amount;noquote@
            }
        }
        assignment_type {
            label {[_ sencha-freelance-translation.assignment_type]}
        }
        start_date {
            label {[_ sencha-freelance-translation.available_after]}
	    display_template {
		<font color=blue><b>@assignments.start_date@</b></font>
	    }
        }
        deadline {
            label {[_ sencha-freelance-translation.deadline]}
	    display_template {
		<font color=red><b>@assignments.deadline@</b></font>
	    }
        }
        assignment_status {
            label {[_ sencha-freelance-translation.assignment_status]}
	    display_template {
		@assignments.assignment_status;noquote@
	    }
        }
        assignment_comment {
            label Comment
            display_template {
                @assignments.assignment_comment;noquote@
            }
        }
        action {
            label {[_ sencha-freelance-translation.assignment_action]}
            display_template {
                @assignments.action;noquote@
            }
        }
}

set return_url [util_get_current_url]


template::list::create \
        -name assignments \
        -multirow assignments \
        -key assignment_id \
        -elements $elements

set num_assignments 0
set total_amount 0
set total_units() [list]
set counter 0
set location [util_current_location]
set currency [im_cost_default_currency]

switch $currency {
    "EUR" { set currency "€" }
    "USD" { set currency "\$" }
}



db_multirow -extend {count package_url po_amount deadline action assignment_status} assignments assignments_sql {
    select distinct freelance_package_name, assignment_id, coalesce(assignment_units,0) as assignment_units, uom_id, im_name_from_id(uom_id) as uom, coalesce(rate,0) as rate, assignment_status_id, im_name_from_id(assignee_id) as assignee_name,
        im_name_from_id(assignment_type_id) as assignment_type, start_date, end_date,
        purchase_order_id, fp.freelance_package_id, assignment_type_id, assignment_comment, 
    source_language_id, target_language_id, o.creation_date,
    (select array_to_string(array_agg(task_name), '</li><li>') from im_trans_tasks where task_id in (select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = fp.freelance_package_id)) as task_names_html
    from im_freelance_assignments fa, im_freelance_packages fp, im_materials m, acs_objects o
    where project_id = :project_id and fp.freelance_package_id = fa.freelance_package_id and assignee_id = :assignee_id
    and fa.assignment_status_id <> 4230
    and fa.material_id = m.material_id
    and fa.assignment_id = o.object_id
    order by assignment_status_id, creation_date desc
} {
    set creation_date [lc_time_fmt $creation_date %x]
    incr counter
    set count $counter
    set po_sum [db_string total "select amount from im_costs where cost_id = :purchase_order_id" -default ""]
    if {$po_sum ne ""} {
	set total_amount  [expr $total_amount + $po_sum]
	set po_sum [format "%.2f" $po_sum]
    #   set po_sum [expr round(100 * $po_sum) / 100]
	set po_amount "$po_sum $currency"
    } else {
        if {$uom_id eq [im_uom_unit]} {
            set total_amount [expr $total_amount + $rate]
        } else {
            set total_amount [expr $total_amount + ($assignment_units * $rate)]
        }
        set formatted_rate [im_report_format_number $rate]
        set formatted_total_units_rate [im_report_format_number [expr $assignment_units * $rate]]
        set po_amount "<center>$assignment_units $uom <br />$formatted_rate $currency / $uom <br />($formatted_total_units_rate $currency)</center>"
    }

    # If Po is not existing "at last once", we need to show Create Purchase Order button
    if {$purchase_order_id eq ""} {
        set show_create_missing_po_button_p 1
    }

    set total_amount_pretty "[format "%.2f" $total_amount] $currency"

    incr num_assignments
    if {[info exists total_units($uom_id)]} {
        set total_units($uom_id) [expr $total_units($uom_id) + $assignment_units]
    } else {
        set total_units($uom_id) $assignment_units
    }

    # Check if we have uom of "unit" and a translation assigment
    # then we can check for the source words
    if { [im_uom_unit] eq $uom_id } {
	set task_uom_ids [list]
	set source_words 0
	db_foreach task_ids "select task_uom_id, task_units from im_trans_tasks tt, im_freelance_packages_trans_tasks fptt where freelance_package_id = :freelance_package_id and fptt.trans_task_id = tt.task_id" {
	    # Check that we have only task_uom_id in the end
	    lappend task_uom_ids $task_uom_id
	    set source_words [expr $source_words + $task_units]
	}
	
	if {[llength $task_uom_ids] eq 1 && $task_uom_ids eq [im_uom_s_word]} {
	    set assignment_units $source_words
	    set uom [im_category_from_id [im_uom_s_word]]
	}
    }
		      

    set assignment_status [im_category_from_id $assignment_status_id]
    set package_url ""
    set accept_url [export_vars -base "${location}/sencha-freelance-translation/accept" -url {project_id assignment_id assignee_id}]
    set deny_url [export_vars -base "${location}/sencha-freelance-translation/deny" -url {project_id assignment_id assignee_id}]
    set deadline [lc_time_fmt $end_date "%q %X"]

    set task_position [lsearch $task_type_ids $assignment_type_id]
    if {$task_position >0} {
    # Find the previous 
    set previous_task_type_id [lindex $task_type_ids [expr $task_position -1]]
    set previous_end_date [db_string end_date "select min(end_date) + interval '1 hour' from im_freelance_assignments fa, im_freelance_packages_trans_tasks fptt where fptt.trans_task_id in (select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id)
and fptt.freelance_package_id = fa.freelance_package_id and fa.assignment_type_id = :previous_task_type_id" -default ""]
    if {$previous_end_date eq ""} {
        set start_date [lc_time_fmt $start_date "%q %X"]
    } else {
        set start_date [lc_time_fmt $previous_end_date "%q %X"]
    }
    } else {
    set start_date [lc_time_fmt $start_date "%q %X"]
    }
    

    switch $assignment_status_id {
        4221 {
            # Requested
        
            # Check if the package is already assigned to someone else
            set existing_assignee_id [db_string assigned_p "select assignee_id from im_freelance_assignments
                where freelance_package_id = :freelance_package_id
                and assignment_type_id = :assignment_type_id
                and assignment_status_id in (4222,4224,4225,4226) limit 1" -default 0]
            
            if {$existing_assignee_id eq 0} {
                set action "<ul><li><a href='$accept_url'>[_ sencha-freelance-translation.Accept]</a></li>
                    <li><a href='$deny_url'>[_ sencha-freelance-translation.Deny]</a></li></ul>"
            } else {
		        set assignment_status "<div style='font-weight:bold;color:#ff0000;'>[_ sencha-freelance-translation.assigned_somone_else]</div>"
                set action ""
                
                # We should not even be here, status if 4228 for someone else assigned
                db_dml update_ass_status "update im_freelance_assignments set assignment_status_id = 4228 where assignment_id = :assignment_id"
            }
        }
        4223 {
            # Denied
            set action ""
        }
        4222 - 4224 - 4229 {
            # Accepted, Work Started, Work Ready
            set out_file_types [im_sub_categories 600]
            set file_present_p 0
            
            # Check if we already have a file for this package
            db_foreach freelance_package_file "select freelance_package_file_id, freelance_package_file_name from im_freelance_package_files where freelance_package_id = :freelance_package_id and package_file_type_id in ([template::util::tcl_to_sql_list $out_file_types])" {
                set file_present_p 1
            }

            if {$file_present_p} {
                set download_url [export_vars -base "/sencha-freelance-translation/package-download" -url {freelance_package_file_id}]
                set action "<ul><li>Download <br /><a href='$download_url'>$freelance_package_file_name</a></li>"
            } else {
        		# Provide an upload URL if you are a PM
        		if {$project_lead_id eq $user_id || [acs_user::site_wide_admin_p -user_id $user_id]} {
                    set upload_url [export_vars -base "/sencha-freelance-translation/package-upload" -url {freelance_package_id return_url {return_file_p 0}}]
        		    append action "<li>[lang::message::lookup "" sencha-freelance-translation.Upload_package_for_freelancer "Upload <a href='%upload_url%'>Package for Freelancer</a>"]</li></ul>"
        		} else {
        		    # Check if the PM Is already reminded
        		    set reminded_p [db_string viewed "select 1 from im_freelance_notifications where object_id = :assignment_id and user_id = :project_lead_id limit 1" -default 0]
        		    if {$reminded_p} {
            			set action "<ul><li><b><font color=red>[_ sencha-freelance-translation.pm_reminded_action]</b></red></li>"
        		    } else {
            			set poke_url [export_vars -base "/sencha-freelance-translation/package-request" -url {freelance_package_id assignee_id assignment_id}]
            			set action "<ul><li><b><font color=red>[_ sencha-freelance-translation.pm_remind_to_upload]</b></font></li>"
        		    }
        		}
            }
            set show_upload_p 1

            if {$trados_p && $assignment_status_id ne 4224} {
                set show_upload_p 0
            }
            if {$show_upload_p} {
                set upload_url [export_vars -base "/sencha-freelance-translation/package-upload" -url {freelance_package_id return_url {return_file_p 1}}]
                append action "<li>[lang::message::lookup "" sencha-freelance-translation.Upload_Return_package "Upload <a href='%upload_url%'>Return Package</a>"]</li></ul>"
            }
        }
        4225 {
            # get the latest file delivered by the assignee. this is the return package
            set freelance_package_file_id [db_string package "select max(freelance_package_file_id)
                from im_freelance_package_files fpf, acs_objects o
                where fpf.freelance_package_file_id = o.object_id
                and fpf.freelance_package_id = :freelance_package_id
                and o.creation_user = :assignee_id" -default ""]
            if {$freelance_package_file_id ne ""} {
                db_1row package_file_info "select freelance_package_file_name
                from im_freelance_package_files
                where freelance_package_file_id = :freelance_package_file_id"
                
                set download_url [export_vars -base "/sencha-freelance-translation/package-download" -url {freelance_package_file_id}]
        if {$assignee_id eq $user_id} {
                set upload_url [export_vars -base "/sencha-freelance-translation/package-upload" -url {freelance_package_id freelance_package_file_id return_url {return_file_p 1}}]
                set action "<ul><li>[_ sencha-freelance-translation.view_your_deliv]<br /><a href='$download_url'>$freelance_package_file_name</a></li>"
            append action "<li>[lang::message::lookup "" sencha-freelance-translation.Replace_Return_package "Replace <a href='%upload_url%'>Return Package</a>"]</li></ul>"
        } else {
                set action "<ul><li>[_ sencha-freelance-translation.review_delivery]<br /><a href='$download_url'>$freelance_package_file_name</a></li>"
        }
            }
        }
	4228 {
	    set assignment_status "<div style='font-weight:bold;color:#ff0000;'>[_ sencha-freelance-translation.assigned_somone_else]</div>"
	}
        default {
            set action ""
        }
    }
    
    if {$purchase_order_id ne ""} {
        if {$project_lead_id eq $user_id || [acs_user::site_wide_admin_p -user_id $user_id]} {
            set po_nr [db_string invoice_nr "select invoice_nr from im_invoices where invoice_id = :purchase_order_id"]
            set po_url [export_vars -base "/intranet-invoices/view" -url {{invoice_id $purchase_order_id}}]
            append po_amount "<br /><a href='$po_url'>$po_nr</a>"
        } elseif {$user_id eq $assignee_id} {
            set token [ns_sha1 someSeCrEt-${assignee_id}-${purchase_order_id}]
            set po_pdf_url [export_vars -base "/sencha-freelance-translation/download-invoice" -url {token assignee_id {invoice_id $purchase_order_id}}]
            append po_amount "<br /><a href='$po_pdf_url'>[_ sencha-freelance-translation.Download_PO]</a>"
        }
    } else {
        # Append the delete action if the user is a pm or admin
        if {$project_lead_id eq $user_id || [acs_user::site_wide_admin_p -user_id $user_id]} {
            set delete_assignment_url [export_vars -base "assignment-delete" -url {assignment_id project_id return_url}]
            append action "<br /><a href='$delete_assignment_url'>[_ sencha-freelance-translation.delete_assignment]</a>"
        }
    }

    if {$project_lead_id eq $user_id || [acs_user::site_wide_admin_p -user_id $user_id]} {
        set view_pm_p 1
    } else {
        set view_pm_p 0
    }
    
    # Mark the entry bold if we have not seen it yet
    set viewed_p 1
    if {![views::viewed_p -object_id $assignment_id -user_id $user_id]} {
        set viewed_p 0
    }
    
    if {!$viewed_p} {
        set count "<b>$count</b>"
        set freelance_package_name "<b>$freelance_package_name</b>"
        set action "<b>$action</b>"
    }

    # Record the view of the assignment
    im_freelance_record_view -object_id $assignment_id -viewer_id $user_id
}

if {$counter eq 0} {
    # Assignments were delete
    ad_return_complaint 1 "[lang::message::lookup "" sencha-freelance-translation.Assignment_deleted "Assignment was probably deleted"]"
}

set end_date [db_string deadline "select max(end_date)
    from im_freelance_assignments fa, im_freelance_packages fp
    where project_id = :project_id
    and fp.freelance_package_id = fa.freelance_package_id
    and assignee_id = :assignee_id
    and assignment_status_id <> 4230"]

set project_deadline [lc_time_fmt $end_date "%q %X"]

set download_pdfs_url [export_vars -base "[parameter::get_from_package_key -package_key "sencha-freelance-translation" -parameter "SourcePDFURL"]" -url {project_id assignee_id}]
set mail_component_html [im_freelance_mail_project_component -assignee_id $assignee_id -project_id $project_id]

set create_missing_purchase_orders_url [export_vars -base "create-missing-purchase-order" -url {project_id assignee_id}]

if {[exists_and_not_null source_language_id]} {
    set source_language [im_name_from_id $source_language_id]
    if {[exists_and_not_null target_language_id]} {
	set target_language [im_name_from_id $target_language_id]
    } else {
	set target_language ""
    }
} else {
    set source_language ""
    set target_language ""
}


if {$assignee_id ne $user_id} {
    # Setup the subnavbar
    set bind_vars [ns_set create]
    ns_set put $bind_vars project_id $project_id
    set parent_menu_id [db_string parent_menu "select menu_id from im_menus where label='project'" -default 0]
    
    set menu_label "project_assignments"
    set sub_navbar [im_sub_navbar \
        -components \
        -base_url "/intranet/projects/view?project_id=$project_id" \
        $parent_menu_id \
        $bind_vars "" "pagedesriptionbar" $menu_label]
} else {
    set sub_navbar ""
}


