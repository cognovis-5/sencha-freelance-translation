<master>
<property name="main_navbar_label">#sencha-freelance-translation.projects#</property>
<property name="sub_navbar">@sub_navbar;noquote@</property>

<table>
  <tr valign="top">
	<td>
	  <table class="list-table" cellpadding="3" cellspacing="1" >

<thead>
    <tr class="list-header">
    
      <th class="list-table" id="assignments_count" colspan="2">
    	
    	  #sencha-freelance-translation.Project_Info#
    	
      </th>
      </tr>
    </thead>
<tbody>
<script type="text/javascript">
   var rateUrl = "@rate_url;noquote@"
   function rateFreelancer(projectId, assigneeId, qualityId) {
          window.document.location.href=rateUrl+'project_id='+projectId+'&assignee_id='+assigneeId+'&quality_id='+qualityId
   }
</script>
<!-- <tr class=odd><td class="list-table">#sencha-freelance-translation.Language#</td><td class="list-table">@skill_2000;noquote@ => @skill_2002;noquote@</td></tr>-->
<tr class=odd><td class="list-table">#sencha-freelance-translation.Language#</td><td class="list-table">@source_language;noquote@ => @target_language;noquote@</td></tr>
<tr class=even><td class="list-table">#sencha-freelance-translation.ProjectNr#</td><td class="list-table">@project_nr;noquote@</td></tr>
<tr class=odd><td class="list-table">#sencha-freelance-translation.Project_Name#</td><td class="list-table">@project_name;noquote@</td></tr>
<tr class=even><td class="list-table">#sencha-freelance-translation.Project_Manager#</td><td class="list-table">@project_lead;noquote@</td></tr>
<if @view_pm_p@ eq 1>
<tr class=odd><td class="list-table">#sencha-freelance-translation.Assignee#</td><td class="list-table">@assignee_name;noquote@</td></tr>
</if>
<tr class=odd><td class="list-table">#sencha-freelance-translation.CAT#</td><td class="list-table">@skill_2006;noquote@</td></tr>
<tr class=even><td class="list-table">#sencha-freelance-translation.Assignments#</td><td class="list-table">@num_assignments;noquote@</td></tr>
<if @employee_p@ eq 1>
    <tr class=even><td class="list-table">Rating:</td><td class="list-table">
       @select_quality_html;noquote@
    </td></tr>
</if>
<tr class=odd><td class="list-table">#sencha-freelance-translation.Honorar_Gesamt#</td><td class="list-table">@total_amount_pretty;noquote@</td></tr>
<tr class=even><td class="list-table">#sencha-freelance-translation.Subject_Area#</td><td class="list-table">@skill_2014;noquote@</td></tr>
<tr class=odd><td class="list-table">#sencha-freelance-translation.Business_Area#</td><td class="list-table">@skill_2024;noquote@</td></tr>
<tr class=even><td class="list-table">#sencha-freelance-translation.Project_Deadline#</td><td class="list-table">@project_deadline;noquote@</td></tr>
<tr>				<td colspan=2>@mail_component_html;noquote@</td></tr>
	</tbody>
	</table>
	</td>
	<td>
		<listtemplate name="assignments"></listtemplate>
		<p align="right"><a href="@download_pdfs_url;noquote@"><button><b>#sencha-freelance-translation.Download_Source_PDFs#</b></button></a></p>
    <if @show_create_missing_po_button_p@ eq 1>
        <p><a href="@create_missing_purchase_orders_url;noquote@">#sencha-freelance-translation.create_missing_po_button#</a></p>
    </if>
		</td>
  </tr>
</table>

