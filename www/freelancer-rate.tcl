ad_page_contract {
	@author ND

	Show the user the details of a single assignment.
} {
	{project_id:integer}
	{assignee_id:integer}
  {quality_id:integer}
	{return_url ""}
} 

   set user_id [ad_maybe_redirect_for_registration]
   set permission [im_user_is_employee_p $user_id]
   set result_message "none"

   if {$permission} {
        set count [db_string sql "select count(mapping_id) as count from im_freelancer_quality where project_id =:project_id and user_id = :assignee_id"]
        if {$count > 0} {
            db_dml update_freelancer_rating "update im_freelancer_quality set quality_id =:quality_id where project_id =:project_id and user_id = :assignee_id"
            set result_message "freelancer rating updated"
        } else {
    	    set result_message "freelancer inserted"
            db_dml insert_freelancer_rating "insert into im_freelancer_quality (project_id, user_id, quality_id) values (:project_id, :assignee_id, :quality_id)"
        }
   } else {
       ad_return_error "not allowed" "You don't have sufficient rights to perform this operation"
   }

   ad_returnredirect [ad_url][apm_package_url_from_key "sencha-freelance-translation"]project-assignments?assignee_id=$assignee_id&project_id=$project_id


