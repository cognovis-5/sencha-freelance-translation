ad_page_contract {
    @author malte.sussdorff@cognovis.de
} {
    project_id:integer
    assignee_id:integer
    {return_url ""}
}


set status_accepted_id 4222
set assignee_accepted_assignments [db_list project_accepted_assignments "select assignment_id from im_freelance_assignments fa, im_freelance_packages fp
    where project_id = :project_id and fp.freelance_package_id = fa.freelance_package_id and assignee_id = :assignee_id and assignment_status_id =:status_accepted_id and purchase_order_id is null"]

foreach assignment_id $assignee_accepted_assignments {
	set invoice_id [im_freelance_create_purchase_orders -assignment_id $assignment_id]
	# Update notification
    db_dml update_notifications "update im_freelance_notifications set viewed_date = now() where object_id = :assignment_id and viewed_date is null"
}


if {$return_url ne ""} {
    ad_returnredirect $return_url
} else {
    ad_returnredirect [export_vars -base "project-assignments" -url {project_id assignee_id}]
}