ad_page_contract {
    @author malte.sussdorff@cognovis.de
} {
    project_id:integer
    task_type_ids:integer,multiple
    action
    {return_url "/sencha-freelance-translation/assignments"}
}

set form [ns_conn form]

set html ""

switch $action {
	back {
		# Return to project
		ad_returnredirect "/intranet/projects/view?project_id=$project_id"
	}
	finalize {
		# ---------------------------------------------------------------
		# Create tasks for finalization if not present
		# Change assignee if present (and time to work on it)
		# ---------------------------------------------------------------
		db_1row project_info "select project_nr, project_name, end_date - interval '1 hour' as default_deadline from im_projects where project_id = :project_id"
		set target_language_ids [im_target_language_ids $project_id]
		set uom_id [im_uom_hour]
		set task_status_id [im_timesheet_task_status_active]
		set task_type_id 9500
		set material_id [im_material_default_material_id]
		set return_url "/sencha-freelance-translation/assignments?project_id=$project_id"
		
		foreach target_language_id $target_language_ids {
			set finalizer_id [ns_querygetall ${target_language_id}_finalizer_id]
			set finalizing_hours [ns_querygetall ${target_language_id}_finalizing_hours]
			set deadline [ns_querygetall ${target_language_id}_deadline]
			if {$deadline eq ""} {
				set deadline $default_deadline
			}
			set task_nr "finalize_${project_nr}_$target_language_id"
			set task_name "[_ sencha-freelance-translation.Finalize] [im_category_from_id $target_language_id] for $project_name"

			
			# Check if we have a task for this language
			set task_id [db_string task_exists "select project_id from im_projects where project_nr = :task_nr and parent_id = :project_id limit 1" -default ""]
			
			if {$task_id eq ""} {
				# Create the finalization task if not present
				set task_id [db_string create_task "SELECT im_timesheet_task__new (
					null,               -- p_task_id
					'im_timesheet_task',    -- object_type
					now(),                  -- creation_date
					null,                   -- creation_user
					null,                   -- creation_ip
					null,                   -- context_id
					:task_nr,
					:task_name,
					:project_id,
					:material_id,
					null,
					:uom_id,
					:task_type_id,
					:task_status_id,
					null
				);" -default ""]
			}
			
			# Update units
			db_dml update_task "update im_timesheet_tasks
				set planned_units = :finalizing_hours,
				task_assignee_id = :finalizer_id,
				deadline_date = :deadline
				where task_id = :task_id"
			
			set freelance_package_ids [db_list tasks "select distinct freelance_package_id
				from im_freelance_packages_trans_tasks fptt, im_trans_tasks tt
				where fptt.trans_task_id = tt.task_id
				and tt.project_id = :project_id
				and tt.target_language_id = :target_language_id"]
				
			set start_date [db_string deadline "select max(end_date)
				from im_freelance_assignments fa
				where freelance_package_id in ([template::util::tcl_to_sql_list $freelance_package_ids])" -default ""]

			db_dml update_project "update im_projects set start_date = :start_date, end_date = :deadline where project_id = :task_id"
			
			# Remove existing members
			foreach member_id [im_biz_object_member_ids $task_id] {
				set object_type [db_string acs_object_type "select object_type from acs_objects where object_id=:task_id"]
				im_exec_dml delete_user "user_group_member_del ($task_id, $member_id)"
				callback im_biz_object_member_after_delete -object_type $object_type -object_id $task_id -user_id $member_id
			}
				
			# Update the assignee as member
			im_biz_object_add_role $finalizer_id $task_id [im_biz_object_role_full_member]
			if {![im_biz_object_member_p $finalizer_id $project_id]} {
				im_biz_object_add_role $finalizer_id $project_id [im_biz_object_role_full_member]
			}
			
			set task_notify_url [export_vars -base "/sencha-freelance-translation/finalize-task-view" -url {task_id return_url}]
			
			# Notify the assignee
			im_freelance_notify \
				-object_id $task_id \
				-recipient_ids [list $finalizer_id] \
				-message "<a href='$task_notify_url'>Finalizing</a> requested until [lc_time_fmt $deadline "%q %X"] for $project_nr" \
				-severity 1
		}
		
		ad_returnredirect $return_url

	}
	default {
		set target_language_ids [im_target_language_ids $project_id]
		
		foreach target_language_id $target_language_ids {
			foreach task_type_id $task_type_ids {
		
				# ---------------------------------------------------------------
				# Work with task_ids
				# We have those in case we did not assign all tasks in packages
				# yet. Typically if we do split assignments.
				# ---------------------------------------------------------------
				
				set task_ids [ns_querygetall ${target_language_id}_${task_type_id}_bulk_file_ids]
				set package_name [ns_querygetall ${target_language_id}_${task_type_id}_package_name]
				
		        if {$task_ids ne ""} {
					# Create a package for it.
					im_freelance_package_create -trans_task_ids $task_ids \
						-package_type_id $task_type_id \
						-freelance_package_name $package_name
				}
			}
		}
		ad_returnredirect "/sencha-freelance-translation/assignments?project_id=$project_id"
	}
}
