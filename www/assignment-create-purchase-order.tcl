
ad_page_contract {
    @author malte.sussdorff@cognovis.de
} {
    assignment_id:integer
    {return_url ""}
}

set invoice_id [im_freelance_create_purchase_orders -assignment_id $assignment_id]

if {$return_url eq ""} {
    set return_url [export_vars -base "/intranet-invoices/view" -url {invoice_id}]
}

ad_returnredirect $return_url
