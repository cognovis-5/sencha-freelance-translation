ad_page_contract {

    Poke the project manager with a notifictaion that the package is waiting to be uploaded.
	@author malte.sussdorff@cognovis.de
    @creation-date 2017-07-08
} {
    freelance_package_id
    assignee_id
    assignment_id
}

set current_user_id [ad_maybe_redirect_for_registration]

db_1row package_file_info "select project_lead_id, fp.project_id, project_nr from im_freelance_packages fp, im_projects p where fp.project_id = p.project_id and fp.freelance_package_id = :freelance_package_id"

set assignee [person::name -person_id $assignee_id]
set assignments_url [export_vars -base "/sencha-freelance-translation/assignments" -url {project_id}]
set upload_url [export_vars -base "/sencha-freelance-translation/package-upload" -url {freelance_package_id {return_file_p 0} {return_url $assignments_url}}] 
im_freelance_notify -object_id $assignment_id -recipient_ids $project_lead_id -severity 0 -message "Package requested from ${assignee}. Please <a href='$upload_url'>upload the package</a> for $project_nr so they can start working"

set return_url [export_vars -base "/sencha-freelance-translation/project-assignments" -url {project_id assignee_id}]

ad_returnredirect $return_url

