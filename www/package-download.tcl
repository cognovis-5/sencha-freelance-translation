
ad_page_contract {
    See if this person is authorized to read the task file,
    guess the MIME type from the original client filename and
    write the binary file to the connection

	@author malte.sussdorff@cognovis.de
    @creation-date 2017-07-08
} {
	freelance_package_file_id
}

set current_user_id [ad_maybe_redirect_for_registration]

db_1row package_file_info "select freelance_package_file_name, freelance_package_id, file_path from im_freelance_package_files where freelance_package_file_id = :freelance_package_file_id"

# Record that the package has been downloaded and the assignment has been started
# (if the assignee is the person doing the downloading)

db_0or1row assignment_info "select assignee_id, assignment_status_id, assignment_id from im_freelance_assignments where freelance_package_id = :freelance_package_id and assignment_status_id in (4222, 4224,4225,4226,4227,4229)"

set read_p 0

if {[im_user_is_employee_p $current_user_id] || [acs_user::site_wide_admin_p -user_id $current_user_id]} {
	set read_p 1
}

if {!$read_p} {
	if {[info exists assignee_id] && $current_user_id eq $assignee_id} {
		# Check if the assignee tries to download
		set read_p 1
	}
}

if {$read_p && [file readable $file_path]} {
	switch $assignment_status_id {
		4222 - 4229 {
			# Update to work started
			db_dml update_ass_status "update im_freelance_assignments set assignment_status_id = 4224 where assignment_id = :assignment_id"
		}
	}

	# Return the file
	set outputheaders [ns_conn outputheaders]
	ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"$freelance_package_file_name\""
	ns_returnfile 200 application/zip $file_path
} else {
	set file $file_path
    ad_return_complaint 1 "<li>[_ intranet-translation.lt_The_specified_file_fi]"
}
