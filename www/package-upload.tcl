# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Template for email inclusion
# @author Malte Sussdorff (sussdorff@sussdorff.de)
# @creation-date 2005-06-14

ad_page_contract {
    See if this person is authorized to read the task file,
    guess the MIME type from the original client filename and
    write the binary file to the connection

    @author malte.sussdorff@cognovis.de
    @creation-date 2017-07-08
} {
    freelance_package_id:integer
    return_url
    {new_status_p 1}
    {return_file_p 1}
}
set current_user_id [ad_maybe_redirect_for_registration]

set page_title "Upload return package"

db_1row package_info "select p.project_id, p.project_nr, p.project_lead_id, freelance_package_name, package_type_id from im_freelance_packages fp, im_projects p
 where freelance_package_id = :freelance_package_id
 and fp.project_id = p.project_id"

# Use the freelancer company name in case needed for communication
set internal_company_name [im_name_from_id [im_company_freelance]]

set assignee_id ""

db_0or1row assignee_info "select assignee_id, assignment_id from im_freelance_assignments where freelance_package_id = :freelance_package_id and assignment_status_id in (4222,4224,4225,4227)"

# Assume the user has viewed the assignment, when uploading a file
# Used for reminders
if {$assignee_id ne ""} {
    im_freelance_record_view -viewer_id $current_user_id -object_id $assignment_id
}

set allowed_p 0
if {$assignee_id eq $current_user_id} {
	set allowed_p 1
} else {
	# Project lead is allowed to upload as well
	if {$project_lead_id eq $current_user_id} {
		set allowed_p 1
	}
}

if {[acs_user::site_wide_admin_p -user_id $current_user_id ]} {
    set allowed_p 1
}

if {!$allowed_p} {
	ad_return_complaint 1 "You are not allowed to upload a file.. How did you get this link?"
	return
}

db_1row task_info "select im_name_from_id(source_language_id) as source_language,
 im_name_from_id(target_language_id) as target_language
  from im_trans_tasks where task_id = (
  	select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id limit 1)"

set associated_task_ids [list]
set associated_tasks [list]
db_foreach tasks "select coalesce(task_filename,task_name) as task, task_id
	from im_trans_tasks tt, im_freelance_packages_trans_tasks fptt
	where tt.task_id = fptt.trans_task_id
	and fptt.freelance_package_id = :freelance_package_id" {
	lappend associated_task_ids $task_id
	lappend associated_tasks $task
}

set associated_tasks_html ""
if {[llength $associated_tasks]>0} {
	append associated_tasks_html "[join $associated_tasks " - "]"
}

set task_type [im_category_from_id $package_type_id]
set form_elements {
    {freelance_package_name:text(inform),optional {label "[_ sencha-freelance-translation.Package_name]"} {value $freelance_package_name}}
    {freelance_package_tasks:text(inform),optional {label "[_ sencha-freelance-translation.Associated_tasks]"} {value $associated_tasks_html}}
    {language:text(inform) {label "[_ intranet-translation.Task_Type]"} {value "$task_type ($source_language => $target_language)"}}
    {upload_file:file(file)
        {label "[_ acs-mail-lite.Upload_file]"}
    }
}

if {$current_user_id eq $assignee_id} {
	lappend form_elements {comment:text(textarea),optional {label "Comment"} {html {cols 40} {rows 10} }}
} else {
	lappend form_elements {comment:text(hidden),optional {label "Comment"} {html {cols 40} {rows 10} }}
}

# In cas  freelacner is uploading file, we are redirecting him to new Extjs page


if {$return_file_p} {
	set rating_upload_screen_url [export_vars -base "/sencha-assignment/freelancer-rating" -url {assignment_id}]
	ad_returnredirect $rating_upload_screen_url
	ad_script_abort
}

set edit_buttons [list [list [_ acs-mail-lite.Upload_file] Upload]]
ad_form -action [ad_conn url] \
    -html {enctype multipart/form-data} \
    -name package_upload \
    -export [list project_id freelance_package_id return_url return_file_p] \
    -cancel_label "[_ acs-kernel.common_Cancel]" \
    -cancel_url $return_url \
    -edit_buttons $edit_buttons \
    -form $form_elements \
    -on_submit {
	    # Check that the uploaded filename matches
	    set upload_filepath [lindex $upload_file 1]
	    set upload_filename [lindex $upload_file 0]
	    set type [im_category_from_id -translate_p 0 $package_type_id]
	    set extension [file extension $upload_filename]
	    switch $extension {
		    .sdlrpx {
			    # Return package
			    set files_in_package_list [split [exec -- /usr/bin/unzip -Z -1 "$upload_filepath"] "\n"]
	    		set matched_p 0
	    		foreach file $files_in_package_list {
	    			if {[string match "${target_language}*" $file]} {
	    				set matched_p 1
	    				break
	    			}
	    		}
	    		if {!$matched_p} {
	    			ad_return_error "Language Missing" "Your package file $upload_filename does not seem to contain a folder for the target language ${target_language}. It is therefore not a valid return package for $package_filename"
	    		}
	    		set upload_folder [im_trans_task_folder \
	    		   -project_id $project_id \
	    		   -target_language $target_language \
	    		   -folder_type $type \
	    		   -return_package]
	    		   
			    set package_file_type_id 607
		    }
		    .sdlppx {
			    # Out Package
			    set upload_folder [im_trans_task_folder \
			       -project_id $project_id \
			       -target_language $target_language \
			       -folder_type $type]
			       
			    set package_file_type_id 606
		    }
			.zip {
		    	if {$return_file_p} {
		    		set upload_folder [im_trans_task_folder \
		    			-project_id $project_id \
		    			-target_language $target_language \
		    			-folder_type $type \
		    			-return_package]
					set package_file_type_id 609
		    	} else {
		    		set upload_folder [im_trans_task_folder \
		    			-project_id $project_id \
		    			-target_language $target_language \
		    			-folder_type $type]
					set package_file_type_id 608
		    	}
		    }
		    default {
			    if {$return_file_p} {
					set upload_folder [im_trans_task_folder \
						-project_id $project_id \
						-target_language $target_language \
						-folder_type $type \
						-return_package]
					set package_file_type_id 601

			    } else {
					set upload_folder [im_trans_task_folder \
						-project_id $project_id \
						-target_language $target_language \
						-folder_type $type]
					set package_file_type_id 600
			    }
		    }
	    }

    


		# Copy the file to the folder
		# Ignore the original name!
		set freelance_package_file_name "${freelance_package_name}$extension"
		set file_path "[im_filestorage_project_path $project_id]/${upload_folder}/$freelance_package_file_name"
		file rename -force $upload_filepath $file_path
		set package_exists_p [db_string exists_p "select 1 from im_freelance_package_files where freelance_package_id = :freelance_package_id and package_file_type_id = :package_file_type_id limit 1" -default 0]

		if {!$package_exists_p} {
			# Mark the file as created (620)
			set ip_address [ad_conn peeraddr]
			set freelance_package_file_id [db_string create_file "select im_freelance_package_file__new(:current_user_id,:ip_address,:freelance_package_id,:freelance_package_file_name,:package_file_type_id,620,:file_path) from dual"]
		} else {
			# Need to overwrite
			db_foreach package_file "select freelance_package_file_id, file_path as old_file_path
				from im_freelance_package_files
				where freelance_package_id = :freelance_package_id and package_file_type_id = :package_file_type_id" {					if {$old_file_path ne $file_path} {
					# delete the file
					file delete -force $old_file_path
					db_dml update_file "update im_freelance_package_files set file_path = :file_path,
						freelance_package_file_name = :freelance_package_file_name
						where freelance_package_file_id = :freelance_package_file_id"
				}
				
				db_dml update_object "update acs_objects set modifying_user = :current_user_id, last_modified = now() where object_id = :freelance_package_file_id"
			}
		}

		# ---------------------------------------------------------------
		# Set variables for mailings etc.
		# ---------------------------------------------------------------
	    set location [util_current_location]
	    if {$assignee_id ne ""} {
	     	set salutation_pretty [im_invoice_salutation -person_id $assignee_id]
			set locale [lang::user::locale -user_id $assignee_id]
		}

		# Need to figure this out later
		if {$return_file_p} {
             
			 # ---------------------------------------------------------------
			 # File uploaded by freelancer
			 # ---------------------------------------------------------------



			set assignments_link [export_vars -base "${location}/sencha-freelance-translation/project-assignments" -url {project_id assignee_id}]
		    set package_link [export_vars -base "${location}/sencha-freelance-translation/package-download" -url {freelance_package_file_id}]
			# Notify the PM about the upload
			set assignee_name [im_name_from_id $assignee_id]
			im_freelance_notify \
				-object_id $assignment_id \
				-recipient_ids $project_lead_id \
			    -message "[_ sencha-freelance-translation.notify_return_package]"
			
			# Set the assignment status to work delivered (4225)
			db_dml update_ass_status "update im_freelance_assignments set assignment_status_id = 4225 where assignment_id = :assignment_id"
			
			if {$comment ne ""} {
				# Create a message for the PM
				set log_id [im_freelance_message_send \
					-sender_id $assignee_id \
					-recipient_ids $project_lead_id \
					-subject "$freelance_package_name Comment" \
					-body "$comment" \
					-context_id $assignment_id \
					-project_id $project_id]
			}
			
			# Notify the freelancer that the package was successfully uploaded
			if {$assignee_id ne ""} {
				set subject "[lang::message::lookup $locale sencha-freelance-translation.lt_package_uploaded_message_subject]"
				set body "[lang::message::lookup $locale sencha-freelance-translation.lt_package_uploaded_message_body]"
			
				im_freelance_notify \
					-object_id $assignment_id \
					-recipient_ids $assignee_id \
					-message "$body"

				set signature [db_string signature "select signature from parties where party_id = :project_lead_id" -default ""]
				if {$signature ne ""} {
					append body [template::util::richtext::get_property html_value $signature]
				}
			
				intranet_chilkat::send_mail \
					-to_party_ids $assignee_id \
					-from_party_id $project_lead_id \
					-subject $subject \
					-body $body \
					-no_callback
								
			}

		} else {

			
			# ---------------------------------------------------------------
			#  Inform the assignee he can start the work
			# ---------------------------------------------------------------

			if {$assignee_id ne ""} {
				# Set the assignment status to work ready (4229)
				db_dml update_ass_status "update im_freelance_assignments set assignment_status_id = 4229 where assignment_id = :assignment_id"
				
	      	   set token [ns_sha1 someSeCrEt-${assignee_id}-${project_id}]
	           set assignments_link [export_vars -base "${location}/sencha-freelance-translation/project-assignments" -url {token project_id assignee_id}]
	           set download_url [export_vars -base "${location}/sencha-freelance-translation/package-download" -url {freelance_package_file_id}]
	           set package_url $download_url

	           # Getting and setting deadline
	           set assignment_deadline [db_string assignment_deadline "select end_date from im_freelance_assignments where assignment_id =:assignment_id" -default ""]
	           set assignment_deadline_formatted [lc_time_fmt $assignment_deadline "%q %X"]
	           
	           set subject "[lang::message::lookup $locale sencha-freelance-translation.lt_package_ready_message_subject]"
	           set body "[lang::message::lookup $locale sencha-freelance-translation.lt_package_ready_message_body]"

	            set signature [db_string signature "select signature from parties where party_id = :project_lead_id" -default ""]
	            if {$signature ne ""} {
	            		append body [template::util::richtext::get_property html_value $signature]
	            }


				acs_mail_lite::send \
					-send_immediately \
					-to_addr [party::email -party_id $assignee_id] \
					-from_addr [party::email -party_id $project_lead_id] \
					-subject $subject \
					-body $body \
					-mime_type "text/html" \
					-no_callback \
				        -use_sender
					
				im_freelance_notify \
					-object_id $assignment_id \
					-recipient_ids $assignee_id
			}
		}
    } -after_submit {
        ad_returnredirect $return_url
    }

