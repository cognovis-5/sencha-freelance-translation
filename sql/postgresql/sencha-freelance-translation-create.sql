CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'im_trans_main_freelancer';
    IF 0 != v_count THEN return 0; END IF;

    create table im_trans_main_freelancer (
        mapping_id          serial,
        customer_id         integer not null
                            constraint im_trans_main_fl_customer_fk
                            references im_companies,
        freelancer_id       integer not null
                            constraint im_trans_main_fl_user_fk
                            references users,
        source_language_id  integer
                            constraint im_trans_main_fl_source_fk
                            references im_categories,
        target_language_id  integer not null
                            constraint im_trans_main_fl_target_fk
                            references im_categories,
        unique (customer_id, freelancer_id, source_language_id, target_language_id)
    );


    return 1;
end;
$$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

update im_categories set aux_string2 = 'source_language_id' where category_id = 2000;
update im_categories set aux_string2 = 'target_language_id' where category_id = 2002;
update im_categories set aux_string2 = 'sworn_language_id' where category_id = 2004;
update im_categories set aux_string2 = 'business_sector_id' where category_id = 2024;
update im_categories set aux_string2 = 'expected_quality_id' where category_id = 2016;

-- Components for main translators
CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
begin


    perform im_component_plugin__delete(plugin_id)
        from im_component_plugins
    where plugin_name in ('Main Translators');

    perform im_component_plugin__new (
        null,                               -- plugin_id
        'im_component_plugin',              -- object_type
        now(),                              -- creation_date
        null,                               -- creation_user
        null,                               -- creation_ip
        null,                               -- context_id
        'Main Translators',                 -- plugin_name
        'intranet-freelance-translation',              -- package_name
        'left',                             -- location
        '/intranet/users/view',             -- page_url
        null,                               -- view_name
        5,                                 -- sort_order
        E'freelance_main_translators_component -user_id $user_id_from_search'     -- component_tcl
    );

    perform im_component_plugin__new (
        null,                               -- plugin_id
        'im_component_plugin',              -- object_type
        now(),                              -- creation_date
        null,                               -- creation_user
        null,                               -- creation_ip
        null,                               -- context_id
        'Main Translators for Company',                 -- plugin_name
        'intranet-freelance-translation',              -- package_name
        'left',                             -- location
        '/intranet/companies/view',             -- page_url
        null,                               -- view_name
        5,                                 -- sort_order
        E'freelance_main_translators_component -company_id $company_id'     -- component_tcl
    );

    perform acs_permission__grant_permission(
        plugin_id,
        (select group_id from groups where group_name = 'Employees'),
        'read')
    from im_component_plugins
where plugin_name in ('Main Translators', 'Main Translators for Company')
    and package_name = 'intranet-freelance-translation';

    return 1;

end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'im_freelancer_quality';
    IF 0 != v_count THEN return 0; END IF;

    create table im_freelancer_quality (
        mapping_id           serial,
        project_id          integer not null
                            constraint im_trans_fl_quality_project_fk
                            references im_projects,
        user_id             integer not null
                            constraint im_trans_fl_quality_user_fk
                            references users,
        quality_id  integer
                            constraint im_trans_fl_quality_id_fk
                            references im_categories,
        rating_date         date
    );

    update im_categories set aux_int1 = 5 where category_id = 110;
    update im_categories set aux_int1 = 4 where category_id = 111;
    update im_categories set aux_int1 = 2 where category_id = 112;
    update im_categories set aux_int1 = 1 where category_id = 113;
    return 0;
end;
$$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'im_freelance_packages';
    IF 0 != v_count THEN return 0; END IF;

    -- Create a new Object Type
    perform acs_object_type__create_type (
        'im_freelance_package',	-- object_type
        'Freelance package',	-- pretty_name
        'Freelance packages',	-- pretty_plural
        'acs_object',		-- supertype
        'im_freelance_packages',	-- table_name
        'freelance_package_id',		-- id_column
        'im_freelance_package',	-- package_name
        'f',			-- abstract_p
        null,			-- type_extension_table
        'im_freelance_package__name'	-- name_method
    );

    insert into acs_object_type_tables (object_type,table_name,id_column)
    values ('im_freelance_package', 'im_freelance_packages', 'freelance_package_id');

    update acs_object_types set
        status_type_table = 'im_freelance_packages',
        status_column = 'package_status_id',
        type_column = 'package_type_id'
    where object_type = 'im_freelance_package';

    create table im_freelance_packages (
        freelance_package_id  integer
                    constraint im_freelance_packages_pk
                    primary key
                    constraint im_freelance_packages_id_fk
                    references acs_objects,
        project_id      integer
                    constraint im_freelance_package_project_fk
                    references im_projects,
        freelance_package_name varchar,
        package_type_id integer
                    constraint im_freelance_packages_type_fk
                    references im_categories,
        package_status_id	integer
                    constraint im_freelance_packages_status_fk
                    references im_categories
    );


    return 1;
end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();

create or replace function im_freelance_package__new (
    integer, varchar, integer, integer, integer,varchar
) returns integer as '
DECLARE
   p_creation_user        alias for $1;
   p_creation_ip        alias for $2;
   p_package_type_id		alias for $3;
   p_package_status_id	alias for $4;
   p_project_id         alias for $5;
   p_freelance_package_name       alias for $6;
   

    v_freelance_package_id	integer;
BEGIN
    v_freelance_package_id := acs_object__new (
        null,
        ''im_freelance_package'',
    now(),
        p_creation_user,
        p_creation_ip,
        p_project_id
    );

    insert into im_freelance_packages (
        freelance_package_id, project_id,
        package_type_id, package_status_id, freelance_package_name
    ) values (
        v_freelance_package_id, p_project_id,
        p_package_type_id, p_package_status_id,p_freelance_package_name
    );

    return v_freelance_package_id;
end;' language 'plpgsql';


create or replace function im_freelance_package__delete (integer) returns integer as '
DECLARE
    v_trans_package_id	 alias for $1;
BEGIN

    -- Erase the im_trans_packages item associated with the id
    delete from     im_trans_packages
    where	   trans_package_id = v_trans_package_id;

    -- Erase all the priviledges
    delete from     acs_permissions
    where	   object_id = v_trans_package_id;

    PERFORM acs_object__delete(v_trans_package_id);

    return 0;
end;' language 'plpgsql';


create or replace function im_freelance_package__name (integer) returns varchar as '
DECLARE
    v_trans_package_id    alias for $1;
    v_name	  varchar;
BEGIN
    select  trans_package_id
    into    v_name
    from    im_trans_packages
    where   trans_package_id = v_trans_package_id;

    return v_name;
end;' language 'plpgsql';


CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'im_freelance_package_files';
    IF 0 != v_count THEN return 0; END IF;

    -- Create a new Object Type
    perform acs_object_type__create_type (
        'im_freelance_package_file',	-- object_type
        'Freelance package file',	-- pretty_name
        'Freelance package files',	-- pretty_plural
        'acs_object',		-- supertype
        'im_freelance_package_files',	-- table_name
        'freelance_package_file_id',		-- id_column
        'im_freelance_package_file',	-- package_name
        'f',			-- abstract_p
        null,			-- type_extension_table
        'im_freelance_package_file__name'	-- name_method
    );

    insert into acs_object_type_tables (object_type,table_name,id_column)
    values ('im_freelance_package_file', 'im_freelance_package_files', 'freelance_package_file_id');

    -- Provide the status column (to let us know if the file was accepted, partially uploaded etc.)
    -- Provide the type column to differentiate between in and out packages (for translation)
    
    update acs_object_types set
        status_type_table = 'im_freelance_package_files',
        status_column = 'package_file_status_id',
        type_column = 'package_file_type_id'
    where object_type = 'im_freelance_package_file';

    create table im_freelance_package_files (
        freelance_package_file_id  integer
                    constraint im_freelance_package_files_pk
                    primary key
                    constraint im_freelance_package_files_id_fk
                    references acs_objects,
        freelance_package_file_name varchar not null,
        freelance_package_id integer not null
                    constraint im_freelance_package_files_package_fk references im_freelance_packages,
        package_file_type_id integer not null
                    constraint im_freelance_package_files_type_fk
                    references im_categories,
        package_file_status_id		integer not null
                    constraint im_freelance_package_files_status_fk
                    references im_categories,
        file_path   text
    );

    -- Speedup lookups by project
    create index im_freelance_package_files_package_id_idx on im_freelance_package_files(freelance_package_id);

    return 1;
end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();

create or replace function im_freelance_package_file__new (
    integer, varchar, integer, varchar,
    integer, integer, text
) returns integer as '
DECLARE
    p_creation_user        alias for $1;
    p_creation_ip        alias for $2;
    p_freelance_package_id		alias for $3;
    p_freelance_package_file_name alias for $4;
    p_package_file_type_id		alias for $5;
    p_package_file_status_id	alias for $6;
    p_file_path 	alias for $7;

    v_freelance_package_file_id	integer;
BEGIN
    v_freelance_package_file_id := acs_object__new (
        null,
        ''im_freelance_package_file'',
         now(),
         p_creation_user,
         p_creation_ip,
         p_freelance_package_id
     );

    insert into im_freelance_package_files (
        freelance_package_file_id, freelance_package_id,
        package_file_type_id, package_file_status_id,
        file_path, freelance_package_file_name
    ) values (
        v_trans_package_file_id, p_freelance_package_id,
        p_package_file_type_id, p_package_file_status_id,
        p_file_path, p_freelance_package_file_name
    );

    return v_freelance_package_file_id;
end;' language 'plpgsql';


create or replace function im_freelance_package_file__delete (integer) returns integer as '
DECLARE
    v_trans_package_file_id	 alias for $1;
BEGIN

    -- Erase the im_freelance_packages item associated with the id
    delete from     im_freelance_package_files
    where	   freelance_package_file_id = v_freelance_package_file_id;

    -- Erase all the priviledges
    delete from     acs_permissions
    where	   object_id = v_freelance_package_file_id;

PERFORM acs_object__delete(v_freelance_package_file_id);

    return 0;
end;' language 'plpgsql';

create or replace function im_freelance_package_file__name (integer) returns varchar as '
DECLARE
    p_freelance_package_file_id    alias for $1;
    v_name	  varchar;
BEGIN
    select  freelance_package_file_name
    into    v_name
    from    im_freelance_package_files
    where   freelance_package_file_id = p_freelance_package_file_id;

    return v_name;
end;' language 'plpgsql';

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'im_freelance_assignments';
    IF 0 != v_count THEN return 0; END IF;

    -- Create a new Object Type
    perform acs_object_type__create_type (
        'im_freelance_assignment',	-- object_type
        'Freelance assignment',	-- pretty_name
        'Freelance assignments',	-- pretty_plural
        'acs_object',		-- supertype
        'im_freelance_assignments',	-- table_name
        'assignment_id',		-- id_column
        'im_freelance_assignment',	-- package_name
        'f',			-- abstract_p
        null,			-- type_extension_table
        'im_freelance_assignment__name'	-- name_method
    );

    insert into acs_object_type_tables (object_type,table_name,id_column)
    values ('im_freelance_assignment', 'im_freelance_assignments', 'assignment_id');
    
    
    update acs_object_types set
        status_type_table = 'im_freelance_assignments',
        status_column = 'assignment_status_id',
        type_column = 'assignment_type_id'
    where object_type = 'im_freelance_assignment';

    create table im_freelance_assignments (
        assignment_id integer
                CONSTRAINT im_freelance_assignment_pk
                PRIMARY KEY
                CONSTRAINT im_freelance_assignment_id_fk
                references acs_objects,
        assignment_name varchar,
        freelance_package_id integer NOT NULL
                constraint im_freelance_assignment_package_fk
                references im_freelance_packages,
        assignee_id integer
                constraint im_freelance_assignment_assignee_fk
                references users,
        assignment_type_id integer NOT NULL
                constraint im_freelance_task_assignment_type_fk
                references im_categories,
        assignment_status_id integer NOT NULL
                constraint im_freelance_task_assignment_status_fk
                references im_categories,
        material_id integer
                constraint im_freelance_assignments_material_fk
                references im_materials ,
        purchase_order_id integer
                constraint im_freelance_assignments_po_fk
                references im_invoices,
        assignment_units numeric,
        uom_id integer NOT NULL
                constraint im_freelance_assignments_uom_fk
                references im_categories,
        rate numeric(12,4),
        start_date timestamptz,
        end_date timestamptz
    );

    return 1;
end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();

create or replace function im_freelance_assignment__new (integer, varchar, varchar, integer, integer, integer, integer, integer,numeric, integer, numeric, timestamptz, timestamptz
) returns integer as '
DECLARE
    p_creation_user        alias for $1;
    p_creation_ip        alias for $2;
    p_assignment_name alias for $3;
    p_freelance_package_id    alias for $4;
    p_assignee_id   alias for $5;
    p_assignment_type_id    alias for $6;
    p_assignment_status_id  alias for $7;
    p_material_id           alias for $8;
    p_assignment_units      alias for $9;
    p_uom_id                alias for $10;
    p_rate                  alias for $11;
    p_start_date            alias for $12;
    p_end_date              alias for $13;
    
    v_assignment_id    integer;

BEGIN
     v_assignment_id := acs_object__new (
        null,
        ''im_freelance_assignment'',
        now(),
        p_creation_user,
        p_creation_ip,
        p_freelance_package_id
    );

    insert into im_freelance_assignments (
        assignment_id, assignment_name, freelance_package_id, assignee_id, assignment_type_id, assignment_status_id,
        material_id, assignment_units, uom_id, rate, start_date, end_date
    ) values (
        v_assignment_id, p_assignment_name, p_freelance_package_id, p_assignee_id, p_assignment_type_id, p_assignment_status_id,
        p_material_id, p_assignment_units, p_uom_id, p_rate, p_start_date, p_end_date
    );
    
    return v_assignment_id;
end;' language 'plpgsql';


create or replace function im_freelance_assignment__delete (integer) returns integer as '
DECLARE
    v_assignment_id	 alias for $1;
BEGIN

    
    delete from     im_freelance_assignments
    where	   assignment_id = v_assignment_id;

    -- Erase all the priviledges
    delete from     acs_permissions
    where	   object_id = v_assignment_id;

    PERFORM acs_object__delete(v_assignment_id);

    return 0;
end;' language 'plpgsql';


SELECT im_category_new ('4220', 'Created', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4221', 'Requested', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4222', 'Accepted', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4223', 'Denied', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4224', 'Work Started', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4225', 'Work Delivered', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4226', 'Delivery Accepted', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4227', 'Delivery Rejected', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4228', 'Other Assignment', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4229', 'Work Ready', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4230', 'Assignment Deleted', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4231', 'Assignment Closed', 'Intranet Freelance Assignment Status');


 -- Mapping table between packages and trans_task_ids
create table im_freelance_packages_trans_tasks (
    freelance_package_id   integer
                    constraint im_freelance_packages_tasks_assignment_fk
                    references im_freelance_packages on delete cascade,
    trans_task_id   integer
                    constraint im_freelance_packages_trans_tasks_fk
                    references im_trans_tasks on delete cascade
);

