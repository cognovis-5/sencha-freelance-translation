SELECT acs_log__debug('/packages/sencha-freelance-translation/sql/postgresql/upgrade/upgrade-4.1.0.1.7-4.1.0.1.8.sql','');

-- Components for assignments

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
begin


    perform im_component_plugin__delete(plugin_id)
        from im_component_plugins
    where plugin_name in ('Project Packages') and package_name = 'sencha-freelance-translation';

    perform im_component_plugin__new (
        null,                               -- plugin_id
        'im_component_plugin',              -- object_type
        now(),                              -- creation_date
        null,                               -- creation_user
        null,                               -- creation_ip
        null,                               -- context_id
        'Project Packages',                 -- plugin_name
        'sencha-freelance-translation',              -- package_name
        'top',                             -- location
        '/sencha-freelance-translation/assignments',             -- page_url
        null,                               -- view_name
        5,                                 -- sort_order
        E'im_freelance_packages_component -project_id $project_id -display_only_p $display_only_p'     -- component_tcl
    );

    perform acs_permission__grant_permission(
        plugin_id,
        (select group_id from groups where group_name = 'Employees'),
        'read')
    from im_component_plugins
where plugin_name in ('Project Packages')
    and package_name = 'sencha-freelance-translation';

    return 1;

end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'im_freelance_notifications';
    IF 0 != v_count THEN return 0; END IF;

    create table im_freelance_notifications (
        notification_id          serial primary key,
        user_id             integer not null
                            constraint im_freelance_notif_user_fk
                            references users,
        object_id           integer not null
                            constraint im_freelance_notif_object_fk
                            references acs_objects,
        viewed_date         timestamptz,
        creation_date       timestamptz,
        severity            integer,
        message             text
    );


    return 1;
end;
$$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

SELECT im_category_new ('4228', 'Other Assignment', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4229', 'Work Ready', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4230', 'Assignment Deleted', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4231', 'Assignment Closed', 'Intranet Freelance Assignment Status');
