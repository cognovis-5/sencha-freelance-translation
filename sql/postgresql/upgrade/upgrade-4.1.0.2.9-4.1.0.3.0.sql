SELECT acs_log__debug('/packages/sencha-freelance-translation/sql/postgresql/upgrade/upgrade-4.1.0.2.9-4.1.0.3.0.sql','');

ALTER TABLE im_assignment_quality_reports ADD COLUMN comment text DEFAULT '';

DROP FUNCTION IF EXISTS im_assignment_quality_report__new(
   p_report_id integer,           -- default null
   p_object_type varchar,       -- default acs_object
   p_creation_date timestamptz, -- default now()
   p_creation_user integer,     -- default null
   p_creation_ip varchar,       -- default null
   p_context_id integer,        -- default null
   p_quality_type_id integer,	
   p_quality_level_id integer,
   p_subject_area_id integer
);

select define_function_args('im_assignment_quality_report__new','report_id;null,object_type;acs_object,creation_date;now(),creation_user;null,creation_ip;null,context_id;null,quality_type_id,quality_level_id');

--
-- procedure im_menu__new/13
--
CREATE OR REPLACE FUNCTION im_assignment_quality_report__new(
   p_report_id integer,           -- default null
   p_object_type varchar,       -- default acs_object
   p_creation_date timestamptz, -- default now()
   p_creation_user integer,     -- default null
   p_creation_ip varchar,       -- default null
   p_context_id integer,        -- default null
   p_quality_type_id integer,	
   p_quality_level_id integer,
   p_subject_area_id integer,
   p_comment text
) RETURNS integer AS $$
DECLARE


        v_report_id               im_assignment_quality_reports.report_id%TYPE;
BEGIN

	-- Check if we have the same reporter rating an assignment for a quality type twice
        select  report_id into v_report_id
        from    im_assignment_quality_reports r, acs_objects o where r.report_id  = o.object_id
        and r.quality_type_id = p_quality_type_id 
        and o.creation_user = p_creation_user
        and o.context_id = p_context_id;

        IF v_report_id is not null THEN return v_report_id; END IF;

        v_report_id := acs_object__new (
                p_report_id,              -- object_id
                p_object_type,          -- object_type
                p_creation_date,        -- creation_date
                p_creation_user,        -- creation_user
                p_creation_ip,          -- creation_ip
                p_context_id            -- context_id
        );

        insert into im_assignment_quality_reports (
                report_id,quality_level_id, quality_type_id, subject_area_id, comment
        ) values (
        		v_report_id,p_quality_level_id, p_quality_type_id, p_subject_area_id, p_comment
        );
        return v_report_id;
END;
$$ LANGUAGE plpgsql;