SELECT acs_log__debug('/packages/sencha-freelance-translation/sql/postgresql/upgrade/upgrade-4.1.0.2.8-4.1.0.2.9.sql','');



select acs_object_type__create_type (
        'im_assignment_quality_report',           -- object_type
        'Assignment Quality Report',              -- pretty_name
        'Assignment Quality Reports',             -- pretty_plural
        'acs_object',        -- supertype
        'im_assignment_quality_reports',          -- table_name
        'report_id',           -- id_column
        'im_assignment_quality',           -- package_name
        'f',                    -- abstract_p
        null,                   -- type_extension_table
        'im_project__name'      -- name_method
);

insert into acs_object_type_tables (object_type,table_name,id_column)
values ('im_assignment_quality_report', 'im_assignment_quality_reports', 'report_id');

update acs_object_types set
        status_type_table = 'im_assignment_quality_reports',
        type_column = 'quality_type_id',
        status_column = NULL
where object_type = 'im_assignment_quality_report';

-- assignment_id is the context_id of the object
-- reporter_id is the creation_user of the object
-- reporting_date is the creation date

create table im_assignment_quality_reports (
	report_id		integer
				constraint im_assignq_report_pk
				primary key
				constraint im_assignq_report_id_fk
				references acs_objects,
	quality_type_id	integer not null
				constraint im_assignq_q_category_fk
				references im_categories,
    quality_level_id integer not null
				constraint im_assignq_q_level_fk
				references im_categories,
 	subject_area_id integer
 				constraint im_assignq_subject_area_fk
 				references im_categories
);

select define_function_args('im_assignment_quality_report__new','report_id;null,object_type;acs_object,creation_date;now(),creation_user;null,creation_ip;null,context_id;null,quality_type_id,quality_level_id');

--
-- procedure im_menu__new/13
--
CREATE OR REPLACE FUNCTION im_assignment_quality_report__new(
   p_report_id integer,           -- default null
   p_object_type varchar,       -- default acs_object
   p_creation_date timestamptz, -- default now()
   p_creation_user integer,     -- default null
   p_creation_ip varchar,       -- default null
   p_context_id integer,        -- default null
   p_quality_type_id integer,	
   p_quality_level_id integer,
   p_subject_area_id integer
) RETURNS integer AS $$
DECLARE


        v_report_id               im_assignment_quality_reports.report_id%TYPE;
BEGIN

	-- Check if we have the same reporter rating an assignment for a quality type twice
        select  report_id into v_report_id
        from    im_assignment_quality_reports r, acs_objects o where r.report_id  = o.object_id
        and r.quality_type_id = p_quality_type_id 
        and o.creation_user = p_creation_user
        and o.context_id = p_context_id;

        IF v_report_id is not null THEN return v_report_id; END IF;

        v_report_id := acs_object__new (
                p_report_id,              -- object_id
                p_object_type,          -- object_type
                p_creation_date,        -- creation_date
                p_creation_user,        -- creation_user
                p_creation_ip,          -- creation_ip
                p_context_id            -- context_id
        );

        insert into im_assignment_quality_reports (
                report_id,quality_level_id, quality_type_id, subject_area_id
        ) values (
        		v_report_id,p_quality_level_id, p_quality_type_id, p_subject_area_id
        );
        return v_report_id;
END;
$$ LANGUAGE plpgsql;


-- added
select define_function_args('im_assignment_quality_report__delete','report_id');

--
-- procedure im_menu__delete/1
--
CREATE OR REPLACE FUNCTION im_assignment_quality_report__delete(
   p_report_id integer
) RETURNS integer AS $$
DECLARE
BEGIN
        -- Erase the im_menus item associated with the id
        delete from     im_assignment_quality_reports
        where           report_id = p_report_id;

        PERFORM acs_object__delete(p_report_id);
        return 0;
END;
$$ LANGUAGE plpgsql;