SELECT acs_log__debug('/packages/sencha-freelance-translation/sql/postgresql/upgrade/upgrade-4.1.0.1.3-4.1.0.1.4.sql','');

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'im_freelance_packages';
    IF 0 != v_count THEN return 0; END IF;

    -- Create a new Object Type
    perform acs_object_type__create_type (
        'im_freelance_package',	-- object_type
        'Freelance package',	-- pretty_name
        'Freelance packages',	-- pretty_plural
        'acs_object',		-- supertype
        'im_freelance_packages',	-- table_name
        'freelance_package_id',		-- id_column
        'im_freelance_package',	-- package_name
        'f',			-- abstract_p
        null,			-- type_extension_table
        'im_freelance_package__name'	-- name_method
    );

    insert into acs_object_type_tables (object_type,table_name,id_column)
    values ('im_freelance_package', 'im_freelance_packages', 'freelance_package_id');

    update acs_object_types set
        status_type_table = 'im_freelance_packages',
        status_column = 'package_status_id',
        type_column = 'package_type_id'
    where object_type = 'im_freelance_package';

    create table im_freelance_packages (
        freelance_package_id  integer
                    constraint im_freelance_packages_pk
                    primary key
                    constraint im_freelance_packages_id_fk
                    references acs_objects,
        freelance_package_name varchar,
        package_type_id integer
                    constraint im_freelance_packages_type_fk
                    references im_categories,
        package_status_id	integer
                    constraint im_freelance_packages_status_fk
                    references im_categories
    );


    return 1;
end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();

create or replace function im_freelance_package__new (
    integer, varchar, integer, integer, integer,varchar
) returns integer as '
DECLARE
   p_creation_user        alias for $1;
   p_creation_ip        alias for $2;
   p_package_type_id		alias for $3;
   p_package_status_id	alias for $4;
   p_project_id         alias for $5;
   p_freelance_package_name       alias for $6;
   

    v_freelance_package_id	integer;
BEGIN
    v_freelance_package_id := acs_object__new (
        null,
        ''im_freelance_package'',
    now(),
        p_creation_user,
        p_creation_ip,
        p_project_id
    );

    insert into im_freelance_packages (
        freelance_package_id, project_id,
        package_type_id, package_status_id, freelance_package_name
    ) values (
        v_freelance_package_id, p_project_id,
        p_package_type_id, p_package_status_id,p_freelance_package_name
    );

    return v_freelance_package_id;
end;' language 'plpgsql';


create or replace function im_freelance_package__delete (integer) returns integer as '
DECLARE
    v_trans_package_id	 alias for $1;
BEGIN

    -- Erase the im_trans_packages item associated with the id
    delete from     im_trans_packages
    where	   trans_package_id = v_trans_package_id;

    -- Erase all the priviledges
    delete from     acs_permissions
    where	   object_id = v_trans_package_id;

    PERFORM acs_object__delete(v_trans_package_id);

    return 0;
end;' language 'plpgsql';


create or replace function im_freelance_package__name (integer) returns varchar as '
DECLARE
    v_trans_package_id    alias for $1;
    v_name	  varchar;
BEGIN
    select  trans_package_id
    into    v_name
    from    im_trans_packages
    where   trans_package_id = v_trans_package_id;

    return v_name;
end;' language 'plpgsql';


CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'im_freelance_package_files';
    IF 0 != v_count THEN return 0; END IF;

    -- Create a new Object Type
    perform acs_object_type__create_type (
        'im_freelance_package_file',	-- object_type
        'Freelance package file',	-- pretty_name
        'Freelance package files',	-- pretty_plural
        'acs_object',		-- supertype
        'im_freelance_package_files',	-- table_name
        'freelance_package_file_id',		-- id_column
        'im_freelance_package_file',	-- package_name
        'f',			-- abstract_p
        null,			-- type_extension_table
        'im_freelance_package_file__name'	-- name_method
    );

    insert into acs_object_type_tables (object_type,table_name,id_column)
    values ('im_freelance_package_file', 'im_freelance_package_files', 'freelance_package_file_id');

    -- Provide the status column (to let us know if the file was accepted, partially uploaded etc.)
    -- Provide the type column to differentiate between in and out packages (for translation)
    
    update acs_object_types set
        status_type_table = 'im_freelance_package_files',
        status_column = 'package_file_status_id',
        type_column = 'package_file_type_id'
    where object_type = 'im_freelance_package_file';

    create table im_freelance_package_files (
        freelance_package_file_id  integer
                    constraint im_freelance_package_files_pk
                    primary key
                    constraint im_freelance_package_files_id_fk
                    references acs_objects,
        freelance_package_file_name varchar not null,
        freelance_package_id integer not null
                    constraint im_freelance_package_files_package_fk references im_freelance_packages,
        package_file_type_id integer not null
                    constraint im_freelance_package_files_type_fk
                    references im_categories,
        package_file_status_id		integer not null
                    constraint im_freelance_package_files_status_fk
                    references im_categories,
        file_path   text
    );

    -- Speedup lookups by project
    create index im_freelance_package_files_package_id_idx on im_freelance_package_files(freelance_package_id);

    return 1;
end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();

create or replace function im_freelance_package_file__new (
    integer, varchar, integer, varchar,
    integer, integer, text
) returns integer as '
DECLARE
    p_creation_user        alias for $1;
    p_creation_ip        alias for $2;
    p_freelance_package_id		alias for $3;
    p_freelance_package_file_name alias for $4;
    p_package_file_type_id		alias for $5;
    p_package_file_status_id	alias for $6;
    p_file_path 	alias for $7;

    v_freelance_package_file_id	integer;
BEGIN
    v_freelance_package_file_id := acs_object__new (
        null,
        ''im_freelance_package_file'',
         now(),
         p_creation_user,
         p_creation_ip,
         p_freelance_package_id
     );

    insert into im_freelance_package_files (
        freelance_package_file_id, freelance_package_id,
        package_file_type_id, package_file_status_id,
        file_path, freelance_package_file_name
    ) values (
        v_freelance_package_file_id, p_freelance_package_id,
        p_package_file_type_id, p_package_file_status_id,
        p_file_path, p_freelance_package_file_name
    );

    return v_freelance_package_file_id;
end;' language 'plpgsql';


create or replace function im_freelance_package_file__delete (integer) returns integer as '
DECLARE
    v_trans_package_file_id	 alias for $1;
BEGIN

    -- Erase the im_freelance_packages item associated with the id
    delete from     im_freelance_package_files
    where	   freelance_package_file_id = v_freelance_package_file_id;

    -- Erase all the priviledges
    delete from     acs_permissions
    where	   object_id = v_freelance_package_file_id;

PERFORM acs_object__delete(v_freelance_package_file_id);

    return 0;
end;' language 'plpgsql';

create or replace function im_freelance_package_file__name (integer) returns varchar as '
DECLARE
    p_freelance_package_file_id    alias for $1;
    v_name	  varchar;
BEGIN
    select  freelance_package_file_name
    into    v_name
    from    im_freelance_package_files
    where   freelance_package_file_id = p_freelance_package_file_id;

    return v_name;
end;' language 'plpgsql';

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'im_freelance_assignments';
    IF 0 != v_count THEN return 0; END IF;

    -- Create a new Object Type
    perform acs_object_type__create_type (
        'im_freelance_assignment',	-- object_type
        'Freelance assignment',	-- pretty_name
        'Freelance assignments',	-- pretty_plural
        'acs_object',		-- supertype
        'im_freelance_assignments',	-- table_name
        'assignment_id',		-- id_column
        'im_freelance_assignment',	-- package_name
        'f',			-- abstract_p
        null,			-- type_extension_table
        'im_freelance_assignment__name'	-- name_method
    );

    insert into acs_object_type_tables (object_type,table_name,id_column)
    values ('im_freelance_assignment', 'im_freelance_assignments', 'assignment_id');
    
    
    update acs_object_types set
        status_type_table = 'im_freelance_assignments',
        status_column = 'assignment_status_id',
        type_column = 'assignment_type_id'
    where object_type = 'im_freelance_assignment';

    create table im_freelance_assignments (
        assignment_id integer
                CONSTRAINT im_freelance_assignment_pk
                PRIMARY KEY
                CONSTRAINT im_freelance_assignment_id_fk
                references acs_objects,
        assignment_name varchar,
        freelance_package_id integer NOT NULL
                constraint im_freelance_assignment_package_fk
                references im_freelance_packages,
        assignee_id integer
                constraint im_freelance_assignment_assignee_fk
                references users,
        assignment_type_id integer NOT NULL
                constraint im_freelance_task_assignment_type_fk
                references im_categories,
        assignment_status_id integer NOT NULL
                constraint im_freelance_task_assignment_status_fk
                references im_categories,
        material_id integer
                constraint im_freelance_assignments_material_fk
                references im_materials ,
        purchase_order_id integer
                constraint im_freelance_assignments_po_fk
                references im_invoices,
        assignment_units numeric,
        uom_id integer NOT NULL
                constraint im_freelance_assignments_uom_fk
                references im_categories,
        rate numeric(12,4),
        start_date timestamptz,
        end_date timestamptz
    );

    return 1;
end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();

create or replace function im_freelance_assignment__new (integer, varchar, varchar, integer, integer, integer, integer, integer,numeric, integer, numeric, timestamptz, timestamptz
) returns integer as '
DECLARE
    p_creation_user        alias for $1;
    p_creation_ip        alias for $2;
    p_assignment_name alias for $3;
    p_freelance_package_id    alias for $4;
    p_assignee_id   alias for $5;
    p_assignment_type_id    alias for $6;
    p_assignment_status_id  alias for $7;
    p_material_id           alias for $8;
    p_assignment_units      alias for $9;
    p_uom_id                alias for $10;
    p_rate                  alias for $11;
    p_start_date            alias for $12;
    p_end_date              alias for $13;
    
    v_assignment_id    integer;

BEGIN
     v_assignment_id := acs_object__new (
        null,
        ''im_freelance_assignment'',
        now(),
        p_creation_user,
        p_creation_ip,
        p_freelance_package_id
    );

    insert into im_freelance_assignments (
        assignment_id, assignment_name, freelance_package_id, assignee_id, assignment_type_id, assignment_status_id,
        material_id, assignment_units, uom_id, rate, start_date, end_date
    ) values (
        v_assignment_id, p_assignment_name, p_freelance_package_id, p_assignee_id, p_assignment_type_id, p_assignment_status_id,
        p_material_id, p_assignment_units, p_uom_id, p_rate, p_start_date, p_end_date
    );
    
    return v_assignment_id;
end;' language 'plpgsql';


create or replace function im_freelance_assignment__delete (integer) returns integer as '
DECLARE
    v_assignment_id	 alias for $1;
BEGIN

    
    delete from     im_freelance_assignments
    where	   assignment_id = v_assignment_id;

    -- Erase all the priviledges
    delete from     acs_permissions
    where	   object_id = v_assignment_id;

    PERFORM acs_object__delete(v_assignment_id);

    return 0;
end;' language 'plpgsql';


SELECT im_category_new ('4220', 'Requested', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4221', 'Requested', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4222', 'Accepted', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4223', 'Denied', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4224', 'Work Started', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4225', 'Work Delivered', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4226', 'Delivery Accepted', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4227', 'Delivery Rejected', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4228', 'Other Assignment', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4229', 'Work Ready', 'Intranet Freelance Assignment Status');
SELECT im_category_new ('4230', 'Assignment Deleted', 'Intranet Freelance Assignment Status');


CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'im_freelance_assignments';
    IF 0 != v_count THEN return 0; END IF;

 -- Mapping table between packages and trans_task_ids
create table im_freelance_packages_trans_tasks (
    freelance_package_id   integer
                    constraint im_freelance_packages_tasks_assignment_fk
                    references im_freelance_packages on delete cascade,
    trans_task_id   integer
                    constraint im_freelance_packages_trans_tasks_fk
                    references im_trans_tasks on delete cascade
);
    return 1;
end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();


