
SELECT acs_log__debug('/packages/sencha-freelance-translation/sql/postgresql/upgrade/upgrade-4.1.0.0.2-4.1.0.0.3.sql','');

-- Components for main translators
CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
begin


    perform im_component_plugin__delete(plugin_id)
        from im_component_plugins
    where plugin_name in ('Main Translators');
    
    perform im_component_plugin__new (
        null,                               -- plugin_id
        'im_component_plugin',              -- object_type
        now(),                              -- creation_date
        null,                               -- creation_user
        null,                               -- creation_ip
        null,                               -- context_id
        'Main Translators',                 -- plugin_name
        'intranet-freelance-translation',              -- package_name
        'left',                             -- location
        '/intranet/users/view',             -- page_url
        null,                               -- view_name
        5,                                 -- sort_order
        E'freelance_main_translators_component -user_id $user_id_from_search'     -- component_tcl
    );
    
    perform im_component_plugin__new (
        null,                               -- plugin_id
        'im_component_plugin',              -- object_type
        now(),                              -- creation_date
        null,                               -- creation_user
        null,                               -- creation_ip
        null,                               -- context_id
        'Main Translators for Company',                 -- plugin_name
        'intranet-freelance-translation',              -- package_name
        'left',                             -- location
        '/intranet/companies/view',             -- page_url
        null,                               -- view_name
        5,                                 -- sort_order
        E'freelance_main_translators_component -company_id $company_id'     -- component_tcl
    );

    perform acs_permission__grant_permission(
        plugin_id,
        (select group_id from groups where group_name = 'Employees'),
        'read')
    from im_component_plugins
where plugin_name in ('Main Translators', 'Main Translators for Company')
    and package_name = 'intranet-freelance-translation';

    return 1;

end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();