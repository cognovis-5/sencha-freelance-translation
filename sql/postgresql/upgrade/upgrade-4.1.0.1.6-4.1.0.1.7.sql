SELECT acs_log__debug('/packages/sencha-freelance-translation/sql/postgresql/upgrade/upgrade-4.1.0.1.6-4.1.0.1.7.sql','');

-- Components for assignments

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
begin


    perform im_component_plugin__delete(plugin_id)
        from im_component_plugins
    where plugin_name in ('Assigned Projects') and package_name = 'sencha-freelance-translation';

    perform im_component_plugin__new (
        null,                               -- plugin_id
        'im_component_plugin',              -- object_type
        now(),                              -- creation_date
        null,                               -- creation_user
        null,                               -- creation_ip
        null,                               -- context_id
        'Assigned Projects',                 -- plugin_name
        'sencha-freelance-translation',              -- package_name
        'left',                             -- location
        '/intranet/index',             -- page_url
        null,                               -- view_name
        1,                                 -- sort_order
        E'im_freelance_assignee_projects_component -assignee_id $user_id'     -- component_tcl
    );

    perform acs_permission__grant_permission(
        plugin_id,
        (select group_id from groups where group_name = 'Employees'),
        'read')
    from im_component_plugins
where plugin_name in ('Assigned Projects')
    and package_name = 'sencha-freelance-translation';
    perform acs_permission__grant_permission(
            plugin_id,
        (select group_id from groups where group_name = 'Freelancers'),
            'read')
        from im_component_plugins
    where plugin_name in ('Assigned Projects')
        and package_name = 'sencha-freelance-translation';

    return 1;

end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
begin


    perform im_component_plugin__delete(plugin_id)
        from im_component_plugins
    where plugin_name in ('Requested Projects') and package_name = 'sencha-freelance-translation';

    perform im_component_plugin__new (
        null,                               -- plugin_id
        'im_component_plugin',              -- object_type
        now(),                              -- creation_date
        null,                               -- creation_user
        null,                               -- creation_ip
        null,                               -- context_id
        'Requested Projects',                 -- plugin_name
        'sencha-freelance-translation',              -- package_name
        'right',                             -- location
        '/intranet/index',             -- page_url
        null,                               -- view_name
        1,                                 -- sort_order
        E'im_freelance_assignee_projects_component -assignee_id $user_id -assignment_status_ids 4221'     -- component_tcl
    );

    perform acs_permission__grant_permission(
        plugin_id,
        (select group_id from groups where group_name = 'Employees'),
        'read')
    from im_component_plugins
where plugin_name in ('Requested Projects')
    and package_name = 'sencha-freelance-translation';
    perform acs_permission__grant_permission(
            plugin_id,
        (select group_id from groups where group_name = 'Freelancers'),
            'read')
        from im_component_plugins
    where plugin_name in ('Requested Projects')
        and package_name = 'sencha-freelance-translation';

    return 1;

end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();