SELECT acs_log__debug('/packages/sencha-freelance-translation/sql/postgresql/upgrade/upgrade-4.1.0.1.4-4.1.0.1.5.sql','');

create or replace function im_freelance_package_file__new (
    integer, varchar, integer, varchar,
    integer, integer, text
) returns integer as '
DECLARE
    p_creation_user        alias for $1;
    p_creation_ip        alias for $2;
    p_freelance_package_id		alias for $3;
    p_freelance_package_file_name alias for $4;
    p_package_file_type_id		alias for $5;
    p_package_file_status_id	alias for $6;
    p_file_path 	alias for $7;

    v_freelance_package_file_id	integer;
BEGIN
    v_freelance_package_file_id := acs_object__new (
        null,
        ''im_freelance_package_file'',
         now(),
         p_creation_user,
         p_creation_ip,
         p_freelance_package_id
     );

    insert into im_freelance_package_files (
        freelance_package_file_id, freelance_package_id,
        package_file_type_id, package_file_status_id,
        file_path, freelance_package_file_name
    ) values (
        v_freelance_package_file_id, p_freelance_package_id,
        p_package_file_type_id, p_package_file_status_id,
        p_file_path, p_freelance_package_file_name
    );

    return v_freelance_package_file_id;
end;' language 'plpgsql';


SELECT im_category_new ('620', 'Created', 'Intranet Translation File Status');
SELECT im_category_new ('621', 'Changed', 'Intranet Translation File Status');
SELECT im_category_new ('622', 'Deleted', 'Intranet Translation File Status');
