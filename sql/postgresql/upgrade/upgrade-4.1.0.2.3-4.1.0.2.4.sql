SELECT acs_log__debug('/packages/sencha-freelance-translation/sql/postgresql/upgrade/upgrade-4.1.0.2.3-4.1.0.2.4.sql','');

ALTER TABLE im_freelance_assignments ADD COLUMN assignment_comment text DEFAULT '';


create or replace function im_freelance_assignment__new (integer, varchar, varchar, integer, integer, integer, integer, integer,numeric, integer, numeric, timestamptz, timestamptz, text
) returns integer as '
DECLARE
    p_creation_user        alias for $1;
    p_creation_ip        alias for $2;
    p_assignment_name alias for $3;
    p_freelance_package_id    alias for $4;
    p_assignee_id   alias for $5;
    p_assignment_type_id    alias for $6;
    p_assignment_status_id  alias for $7;
    p_material_id           alias for $8;
    p_assignment_units      alias for $9;
    p_uom_id                alias for $10;
    p_rate                  alias for $11;
    p_start_date            alias for $12;
    p_end_date              alias for $13;
    p_assignment_comment alias for $14;
    
    v_assignment_id    integer;

BEGIN
     v_assignment_id := acs_object__new (
        null,
        ''im_freelance_assignment'',
        now(),
        p_creation_user,
        p_creation_ip,
        p_freelance_package_id
    );

    insert into im_freelance_assignments (
        assignment_id, assignment_name, freelance_package_id, assignee_id, assignment_type_id, assignment_status_id,
        material_id, assignment_units, uom_id, rate, start_date, end_date, assignment_comment
    ) values (
        v_assignment_id, p_assignment_name, p_freelance_package_id, p_assignee_id, p_assignment_type_id, p_assignment_status_id,
        p_material_id, p_assignment_units, p_uom_id, p_rate, p_start_date, p_end_date, p_assignment_comment
    );
    
    return v_assignment_id;
end;' language 'plpgsql';
