SELECT acs_log__debug('/packages/sencha-freelance-translation/sql/postgresql/upgrade/upgrade-4.1.0.2.0-4.1.0.2.1.sql','');

alter table im_freelance_notifications drop constraint im_freelance_notif_object_fk;
alter table im_freelance_notifications add constraint im_freelance_notif_object_fk foreign key (object_id) references acs_objects(object_id) on delete cascade;
