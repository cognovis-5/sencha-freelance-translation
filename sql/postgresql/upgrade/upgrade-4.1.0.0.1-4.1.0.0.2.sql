
SELECT acs_log__debug('/packages/sencha-freelance-translation/sql/postgresql/upgrade/upgrade-4.1.0.0.1-4.1.0.0.2.sql','');

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'im_trans_main_freelancer';
    IF 0 != v_count THEN return 0; END IF;

    create table im_trans_main_freelancer (
    	mapping_id          serial,
        customer_id         integer not null
                            constraint im_trans_main_fl_customer_fk
                            references im_companies,
        freelancer_id       integer not null
                            constraint im_trans_main_fl_user_fk
                            references users,
        source_language_id  integer
                            constraint im_trans_main_fl_source_fk
                            references im_categories,
        target_language_id  integer not null
                            constraint im_trans_main_fl_target_fk
                            references im_categories,
        unique (customer_id, freelancer_id, source_language_id, target_language_id)
    );


    return 1;
end;
$$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

update im_categories set aux_string2 = 'source_language_id' where category_id = 2000;
update im_categories set aux_string2 = 'target_language_id' where category_id = 2002;
update im_categories set aux_string2 = 'sworn_language_id' where category_id = 2004;
update im_categories set aux_string2 = 'business_sector_id' where category_id = 2024;
update im_categories set aux_string2 = 'expected_quality_id' where category_id = 2016;


