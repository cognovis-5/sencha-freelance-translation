SELECT acs_log__debug('/packages/sencha-freelance-translation/sql/postgresql/upgrade/upgrade-4.1.0.3.2-4.1.0.3.3.sql','');

create or replace function im_freelance_assignment__name (integer)
returns varchar as '
declare
	p_assignment_id alias for $1;	-- invoice_id
	v_name	varchar;
begin
	select	assignment_name
	into	v_name
	from	im_freelance_assignments
	where	assignment_id = p_assignment_id;

	return v_name;
end;' language 'plpgsql';
