SELECT acs_log__debug('/packages/sencha-freelance-translation/sql/postgresql/upgrade/upgrade-4.1.0.3.0-4.1.0.3.1.sql','');

-- Create the table to store the report ratings
create table im_assignment_quality_report_ratings (
	report_id		integer
				constraint im_assignq_report_id_fk
				references im_assignment_quality_reports,
	quality_type_id	integer not null
				constraint im_assignq_q_category_fk
				references im_categories,
        quality_level_id integer not null
				constraint im_assignq_q_level_fk
				references im_categories,
	primary key (report_id, quality_type_id)
);

-- Update the report table

alter table im_assignment_quality_reports add column assignment_id integer constraint im_assignq_q_free_ass_fq references im_freelance_assignments;
alter table im_assignment_quality_reports add column comment text;
alter table im_assignment_quality_reports drop column quality_level_id;
alter table im_assignment_quality_reports drop column quality_type_id;

update im_assignment_quality_reports set assignment_id = context_id from acs_objects where report_id = object_id;

create or replace function inline_0 ()
returns integer as $body$
declare
        row                     RECORD;
	record		RECORD;
begin

	for row in 
	select min(report_id) as report_id, assignment_id from im_assignment_quality_reports group by assignment_id
	loop
	   for record in
	   select quality_type_id, quality_level_id from im_assignment_quality_reports
           loop
               insert into im_assignment_quality_report_ratings (report_id, quality_type_id, quality_level_id) 
	       values (row.report_id, record.quality_type_id, record.quality_level_id);
	   end loop;
        end loop;
	return 0;
end;$body$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

create or replace function inline_0 ()
returns integer as $body$
declare
        row                     RECORD;
begin

	for row in 
	select report_id from im_assignment_quality_reports where report_id not in (select distinct report_id from im_assignment_quality_report_ratings)
	loop
		perform im_assignment_quality_report__delete(row.report_id);
        end loop;
	return 0;
end;$body$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();


-- Update the delete function
CREATE OR REPLACE FUNCTION im_assignment_quality_report__delete(
   p_report_id integer
) RETURNS integer AS $$
DECLARE
BEGIN
	-- Delete from the ratings table
	delete from    im_assignment_quality_report_ratings
	where	       report_id = p_report_id;

        -- Erase the im_menus item associated with the id
        delete from     im_assignment_quality_reports
        where           report_id = p_report_id;

        PERFORM acs_object__delete(p_report_id);
        return 0;
END;
$$ LANGUAGE plpgsql;


--
-- procedure im_menu__new/13
--

DROP FUNCTION IF EXISTS im_assignment_quality_report__new(
   p_report_id integer,           -- default null
   p_object_type varchar,       -- default acs_object
   p_creation_date timestamptz, -- default now()
   p_creation_user integer,     -- default null
   p_creation_ip varchar,       -- default null
   p_context_id integer,        -- default null
   p_quality_type_id integer,
   p_quality_level_id integer,
   p_subject_area_id integer,
   p_comment text
);

CREATE OR REPLACE FUNCTION im_assignment_quality_report__new(
   p_report_id integer,           -- default null
   p_object_type varchar,       -- default acs_object
   p_creation_date timestamptz, -- default now()
   p_creation_user integer,     -- default null
   p_creation_ip varchar,       -- default null
   p_assignment_id integer,        -- default null
   p_quality_type_id integer,	
   p_quality_level_id integer,
   p_subject_area_id integer,
   p_comment text
) RETURNS integer AS $$
DECLARE
        v_report_id               im_assignment_quality_reports.report_id%TYPE;
BEGIN

	-- Check if we have the same reporter rating an assignment for a quality type twice
        select  r.report_id into v_report_id
        from    im_assignment_quality_reports r, acs_objects o, im_assignment_quality_report_ratings ra
	where r.report_id  = o.object_id
	and ra.report_id = r.report_id
        and ra.quality_type_id = p_quality_type_id 
        and o.creation_user = p_creation_user
        and r.assignment_id = p_assignment_id;

        IF v_report_id is not null THEN return v_report_id; END IF;

  	-- Check if we have the same reporter rating an assignment for a quality type twice
        select  r.report_id into v_report_id
        from    im_assignment_quality_reports r, acs_objects o
	where r.report_id  = o.object_id
        and o.creation_user = p_creation_user
        and r.assignment_id = p_assignment_id;

        IF v_report_id is not null THEN 

        insert into im_assignment_quality_report_ratings (
                report_id, quality_type_id, quality_level_id
        ) values (
       		v_report_id,p_quality_type_id, p_quality_level_id
        );
     return v_report_id;
     	END IF;

        v_report_id := acs_object__new (
                p_report_id,              -- object_id
                p_object_type,          -- object_type
                p_creation_date,        -- creation_date
                p_creation_user,        -- creation_user
                p_creation_ip,          -- creation_ip
                NULL			-- context_id
        );

        insert into im_assignment_quality_reports (
                report_id, assignment_id, comment, subject_area_id
        ) values (
        		v_report_id,p_assignment_id, p_comment, p_subject_area_id
        );
        insert into im_assignment_quality_report_ratings (
                report_id, quality_type_id, quality_level_id
        ) values (
        		v_report_id,p_quality_type_id, p_quality_level_id
        );
        return v_report_id;

END;
$$ LANGUAGE plpgsql;
